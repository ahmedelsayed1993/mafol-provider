package com.aait.mahfulprovider.di

import com.aait.mahfulprovider.business.data.network.repo.AuthRepositoryImpl
import com.aait.mahfulprovider.business.data.network.repo.CommonRepositoryImpl
import com.aait.mahfulprovider.business.data.network.repo.MainRepositoryImpl
import com.aait.mahfulprovider.business.domain.api.AuthApi
import com.aait.mahfulprovider.business.domain.api.CommonApi
import com.aait.mahfulprovider.business.domain.api.MainApi
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import com.aait.mahfulprovider.business.domain.repo.CommonRepository
import com.aait.mahfulprovider.business.domain.repo.MainRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.CoroutineDispatcher

@Module
@InstallIn(ViewModelComponent::class)
class RepositoryModule {

    @ViewModelScoped
    @Provides
    fun providesAuthRepository(authApi: AuthApi, dispatcher: CoroutineDispatcher): AuthRepository {
        return AuthRepositoryImpl(
            authApi,
            dispatcher
        )
    }

    @ViewModelScoped
    @Provides
    fun providesMainRepository(mainApi: MainApi, dispatcher: CoroutineDispatcher): MainRepository {
        return MainRepositoryImpl(
            mainApi,
            dispatcher
        )
    }

    @ViewModelScoped
    @Provides
    fun providesCommonRepository(commonApi: CommonApi, dispatcher: CoroutineDispatcher): CommonRepository {
        return CommonRepositoryImpl(
            commonApi,
            dispatcher
        )
    }
}