package com.aait.mahfulprovider.di

import android.content.Context
import android.content.SharedPreferences
import com.aait.mahfulprovider.business.domain.model.auth.UserData
import com.aait.mahfulprovider.business.domain.model.auth.UserResponse
import com.aait.mahfulprovider.di.PreferenceModule.PreferenceConstants.ADDRESS
import com.aait.mahfulprovider.di.PreferenceModule.PreferenceConstants.FIREBASE_TOKEN
import com.aait.mahfulprovider.di.PreferenceModule.PreferenceConstants.ID
import com.aait.mahfulprovider.di.PreferenceModule.PreferenceConstants.IS_ACTIVATED
import com.aait.mahfulprovider.di.PreferenceModule.PreferenceConstants.IS_ARABIC
import com.aait.mahfulprovider.di.PreferenceModule.PreferenceConstants.IS_FIRST_TIME
import com.aait.mahfulprovider.di.PreferenceModule.PreferenceConstants.LANGUAGE
import com.aait.mahfulprovider.di.PreferenceModule.PreferenceConstants.LATITUDE
import com.aait.mahfulprovider.di.PreferenceModule.PreferenceConstants.LONGITUDE
import com.aait.mahfulprovider.di.PreferenceModule.PreferenceConstants.PREFERENCE_NAME
import com.aait.mahfulprovider.di.PreferenceModule.PreferenceConstants.TOKEN
import com.aait.mahfulprovider.di.PreferenceModule.PreferenceConstants.USER_DATA
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class PreferenceModule @Inject
constructor(@ApplicationContext context: Context) {
    private val preferences: SharedPreferences =
        context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    //handle string
    private fun putString(key: String, value: String?) {
        val editor = preferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    private fun getString(key: String, defaultValue: String?): String? {
        return preferences.getString(key, defaultValue)
    }

    //handle boolean
    private fun putBoolean(key: String, value: Boolean?) {
        val editor = preferences.edit()
        editor.putBoolean(key, value!!)
        editor.apply()
    }

    private fun getBoolean(key: String, defaultValue: Boolean?): Boolean {
        return preferences.getBoolean(key, defaultValue!!)
    }

    //language
    var isArabicLanguage: Boolean
        get() = this.getBoolean(IS_ARABIC, language == "ar")
        set(isArabicLanguage) = putBoolean(IS_ARABIC, isArabicLanguage)

    var language: String
        get() = this.getString(LANGUAGE, "ar")!!
        set(language) = putString(LANGUAGE, language)

    //token
    var token: String?
        get() = this.getString(TOKEN, "")
        set(token) = putString(TOKEN, token)

    //userData
    var userData: UserResponse?
        get() = this.getString(USER_DATA, null).let { Gson().fromJson(it, UserResponse::class.java) }
        set(userData) = putString(USER_DATA, Gson().toJson(userData))

    //firebaseToken
    var firebaseToken: String?
        get() = this.getString(FIREBASE_TOKEN, "123")
        set(firebaseToken) = putString(FIREBASE_TOKEN, firebaseToken)
    //ID
    var id: String?
        get() = this.getString(ID, "123")
        set(id) = putString(ID, id)

    //latitude
    var latitude: String?
        get() = this.getString(LATITUDE, "")
        set(latitude) = putString(LATITUDE, latitude)

    //longitude
    var longitude: String?
        get() = this.getString(LONGITUDE, "")
        set(longitude) = putString(LONGITUDE, longitude)

    //longitude
    var address: String?
        get() = this.getString(ADDRESS, "")
        set(address) = putString(ADDRESS, address)

    var isUserActivated: Boolean
        get() = this.getBoolean(IS_ACTIVATED, false)
        set(isUserActivated) = putBoolean(IS_ACTIVATED, isUserActivated)

    var isFirstTime: Boolean
        get() = this.getBoolean(IS_FIRST_TIME, true)
        set(isFirstTime) = putBoolean(IS_FIRST_TIME, isFirstTime)

    object PreferenceConstants {
        const val PREFERENCE_NAME = "mahfulprovider_preference"
        const val IS_ARABIC = "isArabic"
        const val LANGUAGE = "language"
        const val TOKEN = "token"
        const val USER_DATA = "userData"
        const val FIREBASE_TOKEN = "firebaseToken"
        const val ID = "ID"
        const val ADDRESS = "address"
        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"
        const val IS_ACTIVATED = "is_activated"
        const val IS_FIRST_TIME = "is_first_time"
    }
}
