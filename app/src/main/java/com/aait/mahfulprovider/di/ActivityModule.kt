package com.aait.mahfulprovider.di

import android.app.Activity
import com.aait.mahfulprovider.business.data.soket.SocketIoManger
import com.aait.mahfulprovider.business.data.soket.SocketIoMangerImpl
//import com.aait.mahfulprovider.business.data.socket.SocketIoManger
//import com.aait.mahfulprovider.business.data.socket.SocketIoMangerImpl
import com.aait.mahfulprovider.util.Constants
import com.aait.mahfulprovider.util.ProgressUtil
import com.aait.mahfulprovider.util.gpsNetworkHelper.GpsAndNetworkChecker
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped
import io.socket.client.IO
import io.socket.client.Socket

@Module
@InstallIn(ActivityComponent::class)
object ActivityModule {

    @Provides
    @ActivityScoped
    fun provideProgressUtil(activity: Activity): ProgressUtil {
        return ProgressUtil(activity)
    }

    @Provides
    @ActivityScoped
    fun provideSocketManager(socket: Socket): SocketIoManger {
        return SocketIoMangerImpl(socket)
    }

    @Provides
    @ActivityScoped
    fun provideSocket(): Socket {
        return IO.socket(Constants.URL.SocketURl)
    }

    @Provides
    @ActivityScoped
    fun gpsAndNetworkChecker(activity: Activity): GpsAndNetworkChecker {
        return GpsAndNetworkChecker(activity)
    }


}