package com.aait.mahfulprovider.util.date

data class Time(
    var hour: String?,
    var minutes: String?,
    var sucand: String?,
    var timeTxt24: String?,
    var timeTxt: String?,
    var timeTxtStat: Boolean?,
    var timeTxtStatus: String?,
    var timeState: String?
)