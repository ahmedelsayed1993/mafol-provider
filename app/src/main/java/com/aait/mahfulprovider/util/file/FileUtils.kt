package com.aait.mahfulprovider.util.file

import android.content.Context
import android.net.Uri
import android.os.Environment
import android.provider.OpenableColumns
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream
import javax.inject.Inject

class FileUtils @Inject constructor(val context: Context) {

    suspend fun saveFileInStorage(uri: Uri): String {

        var path = ""

        val launch = CoroutineScope(Dispatchers.Main).launch {

            try {
                val file: File?
                val mimeType: String? = context.contentResolver.getType(uri)
                if (mimeType != null) {
                    val inputStream: InputStream? = context.contentResolver.openInputStream(uri)
                    val fileName = getFileName(uri)

                    if (fileName != "") {
                        file = File(
                            context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)?.absolutePath.toString() + "/" + fileName
                        )
                        val output: OutputStream = FileOutputStream(file)
                        try {
                            val buffer =
                                ByteArray(inputStream?.available()!!) // or other buffer size
                            var read = 0
                            while (inputStream.read(buffer).also({ read = it }) != -1) {
                                output.write(buffer, 0, read)
                            }
                            output.flush()
                            path = file.getAbsolutePath() //use this path

                        } finally {
                            output.close()
                        }
                    }
                }

            } catch (e: Exception) {
            }

        }

        launch.join()

        return path
    }

    fun getFileName(uri: Uri): String {
        // The query, since it only applies to a single document, will only return
        // one row. There's no need to filter, sort, or select fields, since we want
        // all fields for one document.
        var displayName = ""
        var cursor = context.contentResolver
            .query(uri, null, null, null, null, null)
        try {
            // moveToFirst() returns false if the cursor has 0 rows.  Very handy for
            // "if there's anything to look at, look at it" conditionals.
            if (cursor != null && cursor.moveToFirst()) {

                // Note it's called "Display Name".  This is
                // provider-specific, and might not necessarily be the file name.
                displayName = cursor.getString(
                    cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                )
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            cursor?.close()
        }
        return displayName
    }


}
