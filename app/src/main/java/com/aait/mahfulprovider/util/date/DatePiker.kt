package com.aait.mahfulprovider.util.date

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import com.aait.mahfulprovider.R
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

fun createTimeObj(): Time {
    return Time("00", "00", "00", "", "", true, "", "")
}

fun createDateObj(): Date {
    val cal = Calendar.getInstance(Locale.ENGLISH)
    val month = cal[Calendar.MONTH] + 1
    val day = cal[Calendar.DAY_OF_MONTH]

    var dayC = day.toString()
    if (dayC.length == 1) {
        dayC = "0$dayC"
    }
    var monthC = month.toString()
    if (monthC.length == 1) {
        monthC = "0$monthC"
    }
    val yearC = cal[Calendar.YEAR]

    return Date(
        dayC,
        monthC,
        "$yearC",
        "$dayC/$monthC/$yearC",
        "$dayC-$monthC-$yearC",
        monthC.toInt()
    )
}

fun Context.fullDateTime(
    date: Date, time: Time,
    resultFullDateTime: (Date?, Time) -> Unit
) {
    this.showCalender(date) {
        this.showTimer(date, time) { date, time ->
            resultFullDateTime(date, time)
        }
    }
}

fun Context.showCalender(
    date1: Date,
    next: (Date) -> Unit
) {
    val mDatePicker = DatePickerDialog(
        this, AlertDialog.BUTTON_POSITIVE,
        { _, selectedYear, selectedMonth, selectedDay ->
            val symbols = DecimalFormatSymbols(Locale.US)
            val mFormat = DecimalFormat("00", symbols)
            date1.date_txt =
                selectedYear.toString() + "/" + mFormat.format(((selectedMonth + 1).toDouble())) + "/" + mFormat.format(
                    (selectedDay.toDouble())
                )

            date1.date =
                selectedYear.toString() + "-" + mFormat.format(((selectedMonth + 1).toDouble())) + "-" + mFormat.format(
                    (selectedDay.toDouble())
                )

            date1.month =
                (mFormat.format(((selectedMonth + 1).toDouble())))

            date1.day = mFormat.format((selectedDay.toDouble()))
            date1.monthD = (selectedMonth)
            date1.year = (selectedYear.toString())

            next(date1)
        },
        date1.year?.toInt()!!,
        date1.monthD!!,
        date1.day?.toInt()!!
    )
    mDatePicker.setTitle("")
    mDatePicker.show()
}

fun Context.showTimer(
    date: Date?, time: Time,
    result: (Date?, Time) -> Unit
) {
    val mTimePicker = TimePickerDialog(
        this, AlertDialog.BUTTON_POSITIVE,
        { _, selectedHour, selectedMinute ->
            val symbols = DecimalFormatSymbols(Locale.US)
            val mFormat = DecimalFormat("00", symbols)

            if (selectedHour > 12) {
                time.timeState = this.getString(R.string.pm)

            } else {
                time.timeState = this.getString(R.string.am)
            }

            time.timeTxt =
                mFormat.format((selectedHour.toDouble() - 12)) + ":" + mFormat.format((selectedMinute.toDouble())) + ":00"
            time.timeTxtStatus =
                mFormat.format((selectedHour.toDouble() - 12)) + ":" + mFormat.format((selectedMinute.toDouble())) + " " + time.timeState

            time.timeTxt24 =
                mFormat.format((selectedHour.toDouble())) + ":" + mFormat.format((selectedMinute.toDouble())) + ":00"

            time.hour = mFormat.format((selectedHour.toDouble()))
            time.minutes = mFormat.format((selectedMinute.toDouble()))
            time.sucand = "00"

            result(date, time)
        }, time.hour!!.toInt(), time.minutes!!.toInt(), true
    ) //Yes 24 hour time
    mTimePicker.setTitle(this.getString(R.string.select_time))
    mTimePicker.show()
}
