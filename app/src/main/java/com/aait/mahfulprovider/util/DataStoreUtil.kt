package com.aait.mahfulprovider.util

enum class DataStoreKey(dataStoreItem: DataStoreItem) {
    TOKEN(DataStoreConstants.tokenItem)
}

data class DataStoreItem(
    val key: String,
    val default: Any?
)

object DataStoreConstants {
    val tokenItem = DataStoreItem("user_token", null)
}
