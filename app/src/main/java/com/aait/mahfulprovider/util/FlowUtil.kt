package com.aait.mahfulprovider.util

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart

object FlowUtil {
    fun <T> Flow<DataState<T>>.launchWhenStarted(lifecycleOwner: LifecycleOwner) {
        lifecycleOwner.lifecycleScope.launchWhenStarted {
            this@launchWhenStarted.collect()
            this@launchWhenStarted.onStart {
                emit(DataState.Idle)
            }
        }
    }
}