package com.aait.mahfulprovider.util.map

interface GpsTrakerListener {
    fun onTrackerSuccess(lat: Double?, log: Double?)

    fun onStartTracker()
}
