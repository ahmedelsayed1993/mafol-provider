package com.aait.mahfulprovider.util.date

data class Date(
    var day: String?,
    var month: String?,
    var year: String?,
    var date_txt: String?,
    var date: String?,
    var monthD: Int?
)