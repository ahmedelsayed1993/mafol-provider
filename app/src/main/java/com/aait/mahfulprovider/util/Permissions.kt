package com.aait.mahfulprovider.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

val CONTACTS_PERMISSIONS_REQUIRED =
    arrayOf(
        Manifest.permission.READ_CONTACTS
    )

fun Context.hasContactPermissions() = CONTACTS_PERMISSIONS_REQUIRED.all {
    ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
}