package com.aait.mahfulprovider.util

import android.net.MacAddress
import java.util.*

object Constants {

    const val TAG = "AppDebug" // Tag for logs
    const val DEBUG = true // enable logging
    const val PICK_DOCUMENT_REQUEST = 1000
    const val NETWORK_TIMEOUT = 60000L
    const val PHONE = "phone"
    const val PASSWORD = "password"
    const val CURRENT_PASSWORD = "current_password"
    const val EMAIL = "email"
    const val NAME = "name"
    const val CONFIRM_PASSWORD = "password_confirmation"
    const val IDENTITY_NUMBER = "identity_number"
    const val GENDER = "gender"
    const val NATION_ID = "nationality_id"
    const val CITY_ID = "city_id"
    const val ADDRESS = "address"
    const val LAT = "lat"
    const val LNG = "lng"
    const val PAGE = "page"
    const val BIRTH_DATE = "birth_date"
    const val COUNTRY_ISO = "country_iso"
    const val DEVICE_ID = "device_id"
    const val DEVICE_TYPE = "device_type"
    const val TYPE_ANDROID = "android"
    const val TYPE = "type"
    const val TYPE_DOCTOR = "doctor"
    const val ACTION = "action"
    const val RESERVATION_ID = "reservation_id"
    const val REASON = "reason"
    const val MALE = "male"
    const val FEMALE = "female"
    const val SUBJECT = "subject"
    const val MESSAGE = "message"
    const val PENDING = "pending"
    const val ACTIVE = "active"
    const val BLOCK = "block"
    const val SUCCESS = "success"
    const val CODE = "code"
    const val AUTH = "auth"
    const val BEARER = "Bearer "
    const val AUTHORIZATION = "Authorization"
    var TOKEN = ""
    const val LANGUAGE = "lang"
    const val CONVERSATION_ID = "conversation_id"
    const val TEXT = "text"
    const val MacAddress = "mac_address"
    val workingDaysIds = listOf(
        "saturday", "sunday", "monday", "tuesday", "wednesday", "thursday", "friday"
    )

    object URL {
        private const val Base_URL = "https://mahfol.4hoste.com"
        const val Remote_Base_URL = "$Base_URL/api/"
        const val SocketURl = "$Base_URL:4601"
    }

    val language: String
        get() {
            return Locale.getDefault().language
        }

    object Pages {
        const val about = "about"
        const val terms = "terms"
    }

    object Home {
        const val RESERVATIONS = "reservations"
        const val VIDEO = "video"
    }

    object Reservations {
        const val UPCOMING = "upcoming_reservations"
        const val FINISHED = "finished_reservations"
    }

    object Videos {
        const val UPCOMING = "upcoming_video"
        const val FINISHED = "finished_video"
    }

    object ReservationActions {
        const val ACCEPT = "accept"
        const val REFUSE = "refuse"
    }

    object MessageSender{
        const val SECOND_USER = "seconduser"
        const val YOU = "you"
    }

    object RequestStatus{
        const val PENDING = "pending"
        const val ACTIVE = "active"
        const val BLOCK = "block"
        const val SUCCESS = "success"

        var FAIL = "fail"
        const val NOT_AUTH = "not_authed"
        const val NOT_COMPLETE = "not_complete"
    }
}