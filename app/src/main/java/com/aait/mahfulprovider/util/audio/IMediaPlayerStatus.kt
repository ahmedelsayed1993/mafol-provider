package com.aait.mahfulprovider.util.audio

interface IMediaPlayerStatus {

    fun onAudioStart()

    fun onAudioComplete()

    fun onAudioError()
}