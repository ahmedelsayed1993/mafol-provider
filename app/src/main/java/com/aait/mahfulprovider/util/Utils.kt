package com.aait.mahfulprovider.util

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.text.Html
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.util.common.onClick
import com.aait.mahfulprovider.util.Constants
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.fxn.pix.Options
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.File
import java.util.*

object Utils {
    fun ImageView.loadProfileImg(url: String?) {
        Glide.with(this).load(url).apply(
            RequestOptions.overrideOf(500, 500).circleCrop()
        ).into(this)
    }

    fun TextView.setHtml(text: String?){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            this.text = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
        } else {
            this.text = Html.fromHtml(text)
        }
    }
    fun shareApp(mContext: Context) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(
            Intent.EXTRA_TEXT,
            "https://play.google.com/store/apps/details?id=" + mContext.packageName
        )
        sendIntent.type = "text/plain"
        sendIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        mContext.startActivity(sendIntent)
    }
    fun getFacebookPageURL(context: Context, facebook: String?): String? {
        val packageManager = context.packageManager
        return try {
            val versionCode = packageManager.getPackageInfo("com.facebook.orca", 0).versionCode
            if (versionCode >= 3002850) { //newer versions of fb app
                "fb://facewebmodal/f?href=$facebook"
            } else { //older versions of fb app
                "fb://page/$facebook"
            }
        } catch (e: PackageManager.NameNotFoundException) {
            facebook //normal web url
        }
    }
    fun openBrowser(context: Context, url: String?) {
        if(url.isNullOrEmpty())
            Toast.makeText(context, R.string.soon, Toast.LENGTH_SHORT).show()
        else
        {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            try {
                context.startActivity(browserIntent)
            }
            catch (e: Exception){
                Toast.makeText(context, R.string.soon, Toast.LENGTH_SHORT).show()
            }
        }
    }
    fun openPhone(context: Context, phone: String?){
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phone")
        try {
            context.startActivity(intent)
        }
        catch (e: Exception){
            Toast.makeText(context, R.string.soon, Toast.LENGTH_SHORT).show()
        }
    }
    fun openWhats(context: Context, phone: String?){
        val url = "https://api.whatsapp.com/send?phone=$phone"
        try {
            val pm: PackageManager = context.packageManager
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES)
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            context.startActivity(i)
        } catch (e: PackageManager.NameNotFoundException) {
//            e.printStackTrace()
            openBrowser(context, url)
        }
    }
    fun openEmail(context: Context, email: String?){
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto: $email")
        try {
            context.startActivity(intent)
        }
        catch (e: Exception){
            Toast.makeText(context, R.string.soon, Toast.LENGTH_SHORT).show()
        }
    }
//    fun onPrepareImagePart(partName: String, fileUri: String): MultipartBody.Part {
//        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
//        // use the FileUtils to get the actual file by uri
//        val file = File(fileUri)
//        val requestFile = file
//            .asRequestBody("image/*".toMediaTypeOrNull())
//        return MultipartBody.Part.createFormData(partName, file.name, requestFile)
//    }
    fun createDrawableFromView(context: Context, view: View): Bitmap? {
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        view.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.buildDrawingCache()
        val bitmap = Bitmap.createBitmap(
            view.measuredWidth,
            view.measuredHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }
    fun onConvertObjToJson(o: Any): JSONObject? {
        val obj: JSONObject
        return try {
            obj = JSONObject(Gson().toJson(o))
            obj
        } catch (t: Throwable) {
            null
        }
    }
    fun handleBack(activity: Activity, backBtn:ImageView){
        if(Constants.language =="en")
            backBtn.rotation = 180f
        backBtn.onClick {
            activity.onBackPressed()
        }
    }

    //Validation
    fun checkEditError(

        editText: EditText,
        message: String
    ): Boolean {
        if (editText.text.toString().isEmpty()) {
            editText.error = message
            editText.requestFocus()
            return true
        }
        else {
            editText.error = null
            return false
        }
    }

    fun checkTextError(textView: TextView, message: String): Boolean {
        if (textView.text.toString().isEmpty()) {
            textView.error = message
            return true
        }else {
            textView.error = null
            return false
        }
    }

    fun checkLength(editText: EditText, message: String, i: Int): Boolean {
        if (editText.text.toString().length < i) {
            editText.error = message
            return true
        }else{
            editText.error = null
            return false
        }

    }

    fun checkLength1(editText: EditText, message: String, i: Int): Boolean {
        if (editText.text.toString().length != i) {
            editText.error = message
            return true
        }else {
            editText.error = null
            return false
        }
    }
    fun showAlertDialog(
        context: Context, message: String,
        negativeClickListener: DialogInterface.OnClickListener?
    ): AlertDialog {
        // create the dialog builder & set message
        val dialogBuilder = AlertDialog.Builder(context)
        dialogBuilder.setTitle(message)
        // check negative click listener
        if (negativeClickListener != null) {
            // not null
            // add negative click listener
            dialogBuilder.setNegativeButton(context.getString(R.string.text_yes), negativeClickListener)
        } else {
            // null
            // add new click listener to dismiss the dialog
            dialogBuilder.setNegativeButton(context.getString(R.string.text_no)) { dialog, which -> dialog.dismiss() }
        }
        // create and show the dialog

        val dialog = dialogBuilder.create()
        dialog.show()
        return dialog
    }

    fun convertToMultiPart(path: String, name: String): MultipartBody.Part {
        var filePart: MultipartBody.Part? = null
        val imageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), imageFile)
        filePart = MultipartBody.Part.createFormData(name, imageFile.name, fileBody)
        return filePart
    }


    val pixOptions: Options = Options.init()
        .setCount(1) //Number of images to restict selection count
        .setFrontfacing(false) //Front Facing camera on start
        .setSpanCount(4) //Span count for gallery min 1 & max 5
        .setMode(Options.Mode.Picture) //Option to select only pictures or videos or both
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion
        .setPath("/pix/images")

}