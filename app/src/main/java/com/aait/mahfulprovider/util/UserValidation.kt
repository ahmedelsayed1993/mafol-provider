package com.aait.mahfulprovider.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.aait.mahfulprovider.R
import es.dmoral.toasty.Toasty

import java.util.*
import java.util.regex.Pattern


fun String.validatePassword(): Boolean {
    val regex = Regex("^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,30}$")
    return matches(regex)
}

fun String.isValidPhone(): Boolean {
    return this.isNotEmpty()
}

fun String.isValidName(): Boolean {
    return this.isNotEmpty() && this.length >= 3
}

fun String.isValidPassword(): Boolean {
    return this.isNotEmpty() && this.length >= 6
}

fun String.isValidEmailAddress(): Boolean {
    val ePattern =
        "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
    val p = Pattern.compile(ePattern)
    val m = p.matcher(this)
    return m.matches()
}


fun Activity.applyLanguage(lang: String) {
    val locale = Locale(lang)
    Locale.setDefault(locale)
    val resources = resources
    val config = resources.configuration
    config.setLocale(locale)
    if (lang == "ar") {
        window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
    } else {
        window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
    }
    resources.updateConfiguration(config, resources.displayMetrics)
}

fun openUrl(context: Context, url: String?) {
    val uri = Uri.parse(url)
    val intent = Intent(Intent.ACTION_VIEW, uri)
    context.startActivity(intent)
}

fun openEmail(context: Context, email: String) {
    val intent = Intent(Intent.ACTION_SEND)
    intent.type = "plain/text"
    intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
    context.startActivity(Intent.createChooser(intent, "Send mail..."))
}

fun openWhatsAppNumber(context: Context, number: String) {
    val url = "https://api.whatsapp.com/send?phone=$number"
    val intent = Intent(Intent.ACTION_VIEW)
    intent.data = Uri.parse(url)
    context.startActivity(intent)
}

fun Context.showToast(message: String?, toastType: ToastType, withIcon: Boolean = true) {
    if (message == null) return
    when (toastType) {
        ToastType.SUCCESS -> {
            Toasty.success(this, message, Toast.LENGTH_SHORT, withIcon).show()
        }
        ToastType.ERROR -> {
            Toasty.error(this, message, Toast.LENGTH_SHORT, withIcon).show()
        }
        ToastType.INFO -> {
            Toasty.info(this, message, Toast.LENGTH_SHORT, withIcon).show()
        }
        ToastType.WARNING -> {
            Toasty.warning(this, message, Toast.LENGTH_SHORT, withIcon).show()
        }
    }
}

fun toastNoInternetConnection(context: Context) {
    Toasty.warning(
        context,
        context.getString(R.string.no_internet_connection),
        Toast.LENGTH_SHORT,
        true
    ).show()
}

fun validateName(name: String): Boolean {
    return name.length >= 3
}

fun openMap(context: Context, latitude: Double?, longitude: Double?) {
    val uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude)
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
    context.startActivity(intent)
}

fun openDirections(context: Context, latitude: Double, longitude: Double) {
    val intent = Intent(
        Intent.ACTION_VIEW,
        Uri.parse("http://maps.google.com/maps?daddr=$latitude,$longitude")
    )
    context.startActivity(intent)
}

fun hideKeyboard(activity: Activity) {
    val v = activity.window.currentFocus
    if (v != null) {
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(v.windowToken, 0)
    }
}

enum class ToastType {
    SUCCESS, ERROR, WARNING, INFO
}
