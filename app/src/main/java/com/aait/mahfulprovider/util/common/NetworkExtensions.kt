package com.aait.mahfulprovider.util.common

import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.data.network.utils.NetworkExceptions
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.util.Constants

interface NetworkExtensionsActions {
    fun onLoad(showLoading: Boolean) {

    }

    fun onError(exceptionMsgId: Int?) {

    }

    fun onFail(message: String) {

    }

    fun notAuth() {

    }

    fun authStatus(userStatus: String) {

    }
}

fun <T> DataState<T>.applyCommonSideEffects(
    networkExtensionsActions: NetworkExtensionsActions,
    showLoading: Boolean = true,
    showError: Boolean = true,
    onSuccess: (T) -> Unit
) {
    when (this) {
        is DataState.Loading -> {
            networkExtensionsActions.onLoad(showLoading)
        }

        is DataState.Success -> {
            networkExtensionsActions.onLoad(false)
            if (this.data is BaseResponse<*>) {
                if (data.key == Constants.RequestStatus.SUCCESS) {
                    when (data.userStatus) {
                        Constants.RequestStatus.ACTIVE -> {
                            onSuccess(data)
                        }
                        Constants.RequestStatus.BLOCK, Constants.RequestStatus.NOT_AUTH -> {
                            networkExtensionsActions.notAuth()
                        }
                        else -> {
                            networkExtensionsActions.authStatus(data.userStatus)
                        }
                    }
                } else {
                    networkExtensionsActions.onFail(if (showError) this.data.msg else "")
                }
            } else {
                networkExtensionsActions.onError(if (showLoading) NetworkExceptions.UnknownException.getErrorMessage() else null)
            }
        }

        is DataState.Error -> {
            networkExtensionsActions.onLoad(false)
            networkExtensionsActions.onError(if (showLoading) this.exception.getErrorMessage() else null)
        }

        is DataState.Idle -> {

        }
    }
}


fun Exception.getErrorMessage(): Int {
    return when (this) {
        NetworkExceptions.TimeoutException -> {
            R.string.time_out
        }

        NetworkExceptions.NetworkConnectionException -> {
            (R.string.no_internet_connection)
        }

        NetworkExceptions.UnknownException -> {
            (R.string.something_went_wrong)
        }

        NetworkExceptions.ServerFailException -> {
            (R.string.something_went_wrong)
        }

        else -> {
            (R.string.something_went_wrong)
        }
    }
}