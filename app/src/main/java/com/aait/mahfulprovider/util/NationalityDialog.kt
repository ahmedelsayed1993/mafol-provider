package com.aait.mahfulprovider.util

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.mahfulprovider.R

import com.aait.mahfulprovider.business.domain.model.auth.NationalityResponseItem
import com.aait.mahfulprovider.ui.adapter.ListAdapter
import com.aait.mahfulprovider.ui.adapter.NationalityAdapter

class NationalityDialog (
    internal var mContext: Context,
    internal var onItemClickListener: OnItemClickListener,
    internal var mCarsList: ArrayList<NationalityResponseItem>,
    internal var title: String
) : Dialog(mContext) {


    internal var rvRecycle: RecyclerView? = null
    internal  var iv_close: ImageView?=null


    internal var lay_no_data: LinearLayout? = null


    var tv_title: TextView? = null

    lateinit var mLinearLayoutManager: LinearLayoutManager


    internal lateinit var mListAdapter: NationalityAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.search_dialog)
        window!!.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        window!!.setGravity(Gravity.CENTER)
        setCancelable(false)

        initializeComponents()
    }


    private fun initializeComponents() {
        tv_title = findViewById(R.id.title)
        rvRecycle = findViewById(R.id.rv_recycle)
        lay_no_data = findViewById(R.id.lay_no_data)
        iv_close = findViewById(R.id.close)
        tv_title!!.text = title
        mLinearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        rvRecycle!!.layoutManager = mLinearLayoutManager
        mListAdapter = NationalityAdapter(mContext, mCarsList, R.layout.recycle_list)
        mListAdapter.setOnItemClickListener(onItemClickListener)
        rvRecycle!!.adapter = mListAdapter

        if (mCarsList.size == 0) {
            lay_no_data!!.visibility = View.VISIBLE
        }
        iv_close?.setOnClickListener { dismiss() }

    }


}

