package com.aait.mahfulprovider.util.common

import android.app.AlertDialog
import android.content.Context
import com.aait.mahfulprovider.R


fun Context.showAlertDialog(
    title: String? = null,
    msg: String? = null,
    drawable: Int? = null,
    listener: (Boolean) -> Unit
) {
    AlertDialog.Builder(this)
        .setTitle(title ?: "")
        .setMessage(msg ?: "")
        .setPositiveButton(
            R.string.text_ok
        ) { _, _ ->
            listener(true)
        }
        .setNegativeButton(
            R.string.text_cancel
        ) { dialog, _ -> dialog.cancel() }
        .setIcon(drawable ?: android.R.drawable.ic_dialog_alert)
        .show()
}