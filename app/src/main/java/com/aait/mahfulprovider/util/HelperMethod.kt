package com.aait.mahfulprovider.util

import android.app.Activity
import android.content.Context
import android.transition.TransitionManager
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.constraintlayout.widget.ConstraintLayout
import com.aait.mahfulprovider.R
import com.bumptech.glide.Glide
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.gson.Gson
import org.json.JSONObject

class HelperMethod {
    companion object {

        fun hiddenKeyBord(view: View, context: Context) {
            val imm =
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun editHeight(view: View, SubView: RelativeLayout?, open: Boolean, context: Context) {
            if (open) {
                TransitionManager.beginDelayedTransition(SubView)
                val appear = view.layoutParams
                appear.width = ViewGroup.LayoutParams.MATCH_PARENT
                appear.height = ViewGroup.LayoutParams.MATCH_PARENT
                view.layoutParams = appear
            } else {
                val appear = view.layoutParams
                appear.height = context.resources.getDimension(R.dimen._530sdp).toInt()
                appear.width = context.resources.getDimension(R.dimen._250sdp).toInt()
                view.layoutParams = appear
            }
        }

        fun editHeight(
            view: View,
            SubView: ConstraintLayout?,
            open: Boolean,
            height: Int,
            width: Int
        ) {
            if (open) {
                TransitionManager.beginDelayedTransition(SubView)
                val appear = view.layoutParams
                appear.width = ViewGroup.LayoutParams.MATCH_PARENT
                appear.height = ViewGroup.LayoutParams.MATCH_PARENT
                view.layoutParams = appear
            } else {
                val appear = view.layoutParams
                appear.height = height
                appear.width = width
                view.layoutParams = appear
            }
        }

        fun editHeight(
            view: View,
            SubView: ConstraintLayout?,
            open: Boolean
        ) {
            if (open) {
                TransitionManager.beginDelayedTransition(SubView)
                val appear = view.layoutParams
                appear.width = ViewGroup.LayoutParams.MATCH_PARENT
                appear.height = ViewGroup.LayoutParams.WRAP_CONTENT
                view.layoutParams = appear
            } else {
                val appear = view.layoutParams
                appear.height = 0
                appear.width = ViewGroup.LayoutParams.MATCH_PARENT
                view.layoutParams = appear
            }
        }

        fun onLoadImageFromUrl(imageView: ImageView?, URl: String?, context: Context?) {
            Glide.with(context!!)
                .load(URl)
                .into(imageView!!)
        }

        fun onLoadImageFromUrl(
            imageView: ImageView?,
            URl: String?,
            context: Context?,
            placeholder: Int
        ) {
            Glide.with(context!!)
                .load(URl)
                .placeholder(placeholder)
                .into(imageView!!)
        }

        fun convertModelToString(slides: Any): String? {
            val gson = Gson()
            return gson.toJson(slides)
        }

        fun onConvertObjToJson(o: Any): JSONObject? {
            val obj: JSONObject
            return try {
                obj = JSONObject(Gson().toJson(o))
                obj
            } catch (t: Throwable) {
                null
            }
        }

        fun isGooglePlayServicesAvailable(activity: Activity?): Boolean {
            val googleApiAvailability = GoogleApiAvailability.getInstance()
            val status = googleApiAvailability.isGooglePlayServicesAvailable(activity!!)
            if (status != ConnectionResult.SUCCESS) {
                if (googleApiAvailability.isUserResolvableError(status)) {
                    googleApiAvailability.getErrorDialog(activity, status, 2404).show()
                }
                return false
            }
            return true
        }

    }
}