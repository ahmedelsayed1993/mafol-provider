package com.aait.mahfulprovider.util

import android.net.Uri
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

object MultiPartUtil {

    fun convertStringToPart(stringToConvert: String): RequestBody {
        return RequestBody.create(
            MediaType.parse("text/plain"),
            stringToConvert
        )
    }


    fun prepareFilePart(
        partName: String,
        fileUri: Uri
    ): MultipartBody.Part {
        val file = File(fileUri.path)
        val requestFile = RequestBody.create(
            MediaType.parse("image/*"),
            file
        )
        return MultipartBody.Part.createFormData(partName, "image", requestFile)
    }


    fun prepareFilePart(
        partName: String,
        path: String
    ): MultipartBody.Part {
        val file = File(path)
        val requestFile = RequestBody.create(
            MediaType.parse("image/*"),
            file
        )
        return MultipartBody.Part.createFormData(partName, "image", requestFile)
    }

    fun preparePDFPart(
        partName: String,
        path: String
    ): MultipartBody.Part {
        val file = File(path)
        val requestFile = RequestBody.create(
            MediaType.parse("pdf/*"),
            file
        )
        return MultipartBody.Part.createFormData(partName, "image", requestFile)
    }

    fun prepareAudioPart(
        partName: String,
        path: String
    ): MultipartBody.Part {
        val file = File(path)
        val requestFile = RequestBody.create(
            MediaType.parse("audio/*"),
            file
        )
        return MultipartBody.Part.createFormData(partName, "audio", requestFile)
    }


    fun prepareVideoPart(
        partName: String,
        path: String
    ): MultipartBody.Part {
        val file = File(path)
        val requestFile = RequestBody.create(
            MediaType.parse("video/*"),
            file
        )
        return MultipartBody.Part.createFormData(partName, "video", requestFile)
    }


}