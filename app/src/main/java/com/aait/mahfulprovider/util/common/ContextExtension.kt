package com.aait.mahfulprovider.util.common

import android.content.Context
import android.widget.Toast

fun Context.toasty(message: String?) =
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()

