package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class ConversationResponse(

	@Json(name="ConversationResponse")
	val conversationResponse: List<ConversationResponseItem?>? = null
)

data class ConversationResponseItem(

	@Json(name="messageType")
	val messageType: String? = null,

	@Json(name="conversation_id")
	val conversation_id: Int? = null,

	@Json(name="receiver_id")
	val receiver_id: Int? = null,

	@Json(name="created")
	val created: String? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="avatar")
	val avatar: String? = null,

	@Json(name="message")
	val message: String? = null
)
