package com.aait.mahfulprovider.business.data.network.utils

object NetworkErrors {

    const val ERROR_CHECK_NETWORK_CONNECTION = "Check network connection."
    const val NETWORK_ERROR_UNKNOWN = "Unknown network error"
    const val NETWORK_ERROR_TIMEOUT = "Network timeout"
    const val SERVER_FAIL = "server fail"
}