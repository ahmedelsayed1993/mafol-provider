package com.aait.mahfulprovider.business.domain.repo

import kotlinx.coroutines.flow.Flow

interface DataStoreRepository {
    suspend fun getValue(key: String, default: Any): Flow<Any>
    suspend fun setValue(key: String, value: Any)
}