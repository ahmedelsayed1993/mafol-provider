package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class NationalityResponse(

	@Json(name="NationalityResponse")
	val nationalityResponse: List<NationalityResponseItem?>? = null
)

data class NationalityResponseItem(

	@Json(name="nationality")
	val nationality: String? = null,

	@Json(name="id")
	val id: Int? = null
)
