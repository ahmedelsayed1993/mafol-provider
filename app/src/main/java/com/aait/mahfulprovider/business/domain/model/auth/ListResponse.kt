package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class ListResponse(

	@Json(name="ListResponse")
	val listResponse: List<ListResponseItem?>? = null
)

data class ListResponseItem(

	@Json(name="name")
	val name: String? = null,

	@Json(name="id")
	val id: Int? = null
)
