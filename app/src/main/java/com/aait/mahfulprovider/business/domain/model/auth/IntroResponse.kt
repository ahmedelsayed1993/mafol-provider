package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class IntroResponse(

	@Json(name="IntroResponse")
	val introResponse: List<IntroResponseItem?>? = null
)

data class IntroResponseItem(

	@Json(name="image")
	val image: String? = null,

	@Json(name="description")
	val description: String? = null,

	@Json(name="title")
	val title: String? = null
)
