package com.aait.mahfulprovider.business.domain.model.auth

import com.google.gson.annotations.SerializedName

data class AuthResponse(
   // val token: String = "",
    val user: UserData? = null
)

data class UserData(
    val token: String = "",
    val name: String = "",
//    @SerializedName("country_key")
//    val countryKey: String = "",
    val phone: String = "",
    val email: String = "",
    val invitation: String = "",
    val user_type: String = "",
    val code: String = "",
    val nationality :String = "",
   // val gender: String = "",
//    @SerializedName("identity_number")
//    val identityNumber: String = "",
    val avatar: String = "",
    @SerializedName("device_id")
    val deviceID: String = "",
    @SerializedName("device_type")
    val deviceType: String = "",
    @SerializedName("mac_address")
    val macAddress: String = "",
    val date:String = "",
//    @SerializedName("time_zone")
//    val timeZone: String = "",
//    @SerializedName("complete_work_info")
//    val completeWorkInfo: Boolean = false,
//    @SerializedName("complete_time_info")
//    val completeTimeInfo: Boolean = false,
    val address: String,
    val lat: String,
    val lng: String
  //  ,val rate: String
)