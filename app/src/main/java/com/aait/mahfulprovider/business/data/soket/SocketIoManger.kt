package com.aait.mahfulprovider.business.data.soket

import android.app.Activity
import org.json.JSONObject

interface SocketIoManger {

    fun connectToSocket(helper: SocketIoHelper)
    fun disconnectSocket(helper: SocketIoHelper)
    fun openChannelListening(
        activity: Activity,
        channel: String,
        socketReConnect: Int,
        helper: SocketIoHelper
    )

    fun setEmit(
        emitType: String, jsonObject: JSONObject?, socketReConnect: Int,
        helper: SocketIoHelper
    )

    fun onConnectSocket(helper: SocketIoHelper)
    fun onDisconnectSocket(helper: SocketIoHelper)
    fun onReconnectSocket(helper: SocketIoHelper)
}