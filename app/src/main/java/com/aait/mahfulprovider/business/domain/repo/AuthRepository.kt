package com.aait.mahfulprovider.business.domain.repo

import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.*
import com.aait.mahfulprovider.util.Constants
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import retrofit2.http.Field
import retrofit2.http.Header

interface AuthRepository {

    suspend fun login(
        phone: String,
        password: String,
        device_id: String,
        mac_address: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>>

    suspend fun register(
        name:String,
        phone: String,
        nationality_id:Int,
        invitation_code:String,
        password: String,
        device_id: String,
        mac_address: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>>

    suspend fun nationality(
        lang: String
    ):Flow<DataState<BaseResponse<List<NationalityResponseItem>>>>


    suspend fun activate(
        code:String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>>

    suspend fun reSend(
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>>

    suspend fun forgotPass(
        phone:String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>>

    suspend fun updatePass(
        password: String,
        code:String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>>

    suspend fun about(
        lang:String
    ):Flow<DataState<BaseResponse<String>>>

    suspend fun contact(
        lang:String
    ):Flow<DataState<BaseResponse<ContactUsResponse>>>

    suspend fun complaint(
        phone: String,
        name: String,
        subject:String,
        message:String,
        lang: String
    ):Flow<DataState<BaseResponse<String>>>

    suspend fun wallet(
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<Int>>>

    suspend fun home(
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<HomeResponse>>>

    suspend fun providers(
        lat:String,
        lng:String,
        lang: String
    ):Flow<DataState<BaseResponse<List<ProvidersResponseItem>>>>

    suspend fun addAddress(
        address:String,
        type:String,
        lat:String,
        lng:String,
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<String>>>

    suspend fun editAddress(
        address_id: Int,
        address:String,
        type:String,
        lat:String,
        lng:String,
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<String>>>

    suspend fun addresses(
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<List<AddressesResponseItem>>>>

    suspend fun deleteAddress(
        address_id:Int,
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<String>>>

    suspend fun vehicles(
         Authorization:String,
         lang: String
    ):Flow<DataState<BaseResponse<List<ListResponseItem>>>>

    suspend fun models(
        vehicle_id:Int,
        Authorization:String,
        lang: String
    ):Flow<DataState<BaseResponse<List<ListResponseItem>>>>

    suspend fun addCar(
        name:String,
        vehicle_id:Int,
        model_id:Int,
        car_color:String,
        plate_number:String,
        Authorization:String,
        lang: String
    ) :Flow<DataState<BaseResponse<String>>>

    suspend fun myCars(
        Authorization:String,
        lang: String
    ):Flow<DataState<BaseResponse<List<MyCarsResponseItem>>>>

    suspend fun editCar(
        car_id:Int,
        name:String?,
        vehicle_id:Int?,
        model_id:Int?,
        car_color:String?,
        plate_number:String?,
        Authorization:String,
        lang: String
    ) :Flow<DataState<BaseResponse<EditCarResponse>>>

    suspend fun deleteCar(
        car_id:Int,
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<String>>>

    suspend fun addOrder(
        category_id:Int,
        provider_id:Int,
        address_id:Int,
        car_id:Int,
        notes:String,
        images:ArrayList<MultipartBody.Part>,
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<String>>>

    suspend fun orders(
        status:String,
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<List<OrdersItem>>>>

    suspend fun orderDetails(
        order_id:Int,
        action:String?,
        reason:String?,
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<OrderDetailsResponse>>>

    suspend fun orderPayment(
        order_id:Int,
        payment:String,
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<Int>>>

    suspend fun profile(
        mac_address: String,
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<UserResponse>>>

    suspend fun updateProfile(
         mac_address: String,
         name:String,
         phone:String,
         nationality_id: Int,
         authorization:String,
         lang: String
    ):Flow<DataState<BaseResponse<UserResponse>>>

    suspend fun updateAvatar(
        mac_address: String,
        authorization: String,
        lang: String,
        avatar:MultipartBody.Part
    ):Flow<DataState<BaseResponse<UserResponse>>>

    suspend fun changePassword(
        current_password:String,
        password:String,
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<UserResponse>>>

    suspend fun updateLocation(
         address:String,
         lat:String,
         lng: String,
         authorization:String,
         lang: String
    ):Flow<DataState<BaseResponse<String>>>

    suspend fun logOut(
         device_id:String,
         device_type:String,
         mac_address: String,
         Authorization:String,
         lang: String
    ) : Flow<DataState<BaseResponse<String>>>

    suspend fun terms(
        lang: String
    ):Flow<DataState<BaseResponse<String>>>

    suspend fun myConversations(
        authorization: String,
        lang: String,
    ): Flow<DataState<BaseResponse<List<MyConversationsResponseItem>>>>

    suspend fun conversation(
        conversation_id: String,
        authorization: String,
        lang: String,
        page: String?,
        app_type:String
    ): Flow<DataState<BaseResponse<List<ConversationResponseItem>>>>

    suspend fun sendMessage(
        receiver_id: String,
        conversation_id: String,
        type: String,
        message: String,
        authorization: String,
        lang: String,
    ): Flow<DataState<BaseResponse<ConversationResponseItem>>>

    suspend fun sendImageMessage(
        receiver_id: String,
        conversation_id: String,
        type: String,
        message: MultipartBody.Part?,
        authorization: String,
        lang: String,
    ): Flow<DataState<BaseResponse<ConversationResponseItem>>>

    suspend fun newConversation(
        receiver_id: Int,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<ConversationIdResponse>>>

    suspend fun notification(
        authorization: String,
        lang: String
    ) : Flow<DataState<BaseResponse<List<NotificationResponseItem>>>>

    suspend fun deleteNotification(
        notification_id:Int,
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<String>>>

    suspend fun deleteAllNotification(
        authorization: String,
        lang: String
    ):Flow<DataState<BaseResponse<String>>>

    suspend fun sendFile(
        file: MultipartBody.Part,
        authorization: String,
        lang: String
    ) : Flow<DataState<BaseResponse<SendFileResponse>>>

    suspend fun transferOrder(
        order_id: Int,
        provider_id: Int,
        reason: String,
        authorization: String,
        lang: String
    ) : Flow<DataState<BaseResponse<String>>>


    suspend fun balanceRequest(
        bank_name:String,
        account_name:String,
        account_number:String,
        iban_number:String
    ): Flow<DataState<BaseResponse<String>>>

    suspend fun screens():Flow<DataState<BaseResponse<List<IntroResponseItem>>>>

    suspend fun availabilty(
         mac_address: String,
         available:Int
    ) : Flow<DataState<BaseResponse<UserResponse>>>
}