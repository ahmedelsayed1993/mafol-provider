package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class ContactUsResponse(

	@Json(name="whatsapp")
	val whatsapp: String? = null,

	@Json(name="socials")
	val socials: List<SocialsItem?>? = null
)

data class SocialsItem(

	@Json(name="name")
	val name: String? = null,

	@Json(name="link")
	val link: String? = null,

	@Json(name="id")
	val id: Int? = null
)
