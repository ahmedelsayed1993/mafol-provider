package com.aait.mahfulprovider.business.data.network.repo

import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.data.network.utils.safeApiCall
import com.aait.mahfulprovider.business.domain.api.AuthApi
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.*
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import okhttp3.MultipartBody
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val authApi: AuthApi,
    private val dispatcher: CoroutineDispatcher
) : AuthRepository {

    //sign in
    override suspend fun login(
        phone: String,
        password: String,
        device_id: String,
        mac_address: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher) {
                    authApi.login(phone, password, device_id, mac_address, lang)
                }
            )
        }

    override suspend fun nationality(lang: String): Flow<DataState<BaseResponse<List<NationalityResponseItem>>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.nationality(lang)
                }
            )
        }

    override suspend fun register(
        name: String,
        phone: String,
        nationality_id:Int,
        invitation_code: String,
        password: String,
        device_id: String,
        mac_address: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>> =  flow {
        emitAll(
            safeApiCall(dispatcher = dispatcher) {
                authApi.SignUp(name,phone,nationality_id,invitation_code, password, device_id, mac_address, lang)
            }
        )
    }


    //check_code
    override suspend fun activate(
        code: String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>> =  flow {
        emitAll(
            safeApiCall(dispatcher = dispatcher) {
                authApi.Activate(code,authorization, lang)
            }
        )
    }
    //resend code
    override suspend fun reSend(
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.reSend(authorization, lang)
                }
            )
        }

    override suspend fun forgotPass(
        phone: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.forgotPass(phone,lang)
                }
            )
        }

    override suspend fun updatePass(
        phone: String,
        code: String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.updatePass(phone,code,authorization,lang)
                }
            )
        }

    override suspend fun about(lang: String): Flow<DataState<BaseResponse<String>>>  =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.about(lang)
                }
            )
        }

    override suspend fun contact(lang: String): Flow<DataState<BaseResponse<ContactUsResponse>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.contactUs(lang)
                }
            )
        }

    override suspend fun complaint(
        phone: String,
        name: String,
        subject: String,
        message: String,
        lang: String
    ): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.complaints(phone,name,subject,message,lang)
                }
            )
        }

    override suspend fun wallet(authorization: String, lang: String): Flow<DataState<BaseResponse<Int>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.wallet(authorization,lang)
                }
            )
        }

    override suspend fun home(authorization: String,lang: String): Flow<DataState<BaseResponse<HomeResponse>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.home(authorization,lang)
                }
            )
        }

    override suspend fun providers(
        lat: String,
        lng: String,
        lang: String
    ): Flow<DataState<BaseResponse<List<ProvidersResponseItem>>>>  =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.providers(lat,lng,lang)
                }
            )
        }

    override suspend fun addAddress(
        address: String,
        type: String,
        lat: String,
        lng: String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.addAddress(address,type,lat,lng,authorization,lang)
                }
            )
        }

    override suspend fun editAddress(
        address_id: Int,
        address: String,
        type: String,
        lat: String,
        lng: String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.editAddress(address_id,address,type,lat,lng,authorization,lang)
                }
            )
        }

    override suspend fun addresses(
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<List<AddressesResponseItem>>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.myAddresses(authorization,lang)
                }
            )
        }

    override suspend fun deleteAddress(
        address_id: Int,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<String>>> =
         flow {
             emitAll(
                 safeApiCall(dispatcher = dispatcher){
                     authApi.deleteAddress(address_id,authorization,lang)
                 }
             )
         }

    override suspend fun vehicles(
        Authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<List<ListResponseItem>>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.vehicles(Authorization,lang)
                }
            )
        }

    override suspend fun models(
        vehicle_id: Int,
        Authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<List<ListResponseItem>>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.models(vehicle_id,Authorization,lang)
                }
            )
        }

    override suspend fun addCar(
        name: String,
        vehicle_id: Int,
        model_id: Int,
        car_color: String,
        plate_number: String,
        Authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.addCar(name,vehicle_id, model_id, car_color, plate_number, Authorization, lang)
                }
            )
        }

    override suspend fun myCars(
        Authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<List<MyCarsResponseItem>>>>  =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.myCars(Authorization,lang)
                }
            )
        }

    override suspend fun editCar(
        car_id: Int,
        name: String?,
        vehicle_id: Int?,
        model_id: Int?,
        car_color: String?,
        plate_number: String?,
        Authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<EditCarResponse>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.editCar(car_id, name, vehicle_id, model_id, car_color, plate_number, Authorization, lang)
                }
            )
        }

    override suspend fun deleteCar(
        car_id: Int,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.deleteCar(car_id,authorization,lang)
                }
            )
        }


    override suspend fun addOrder(
        category_id: Int,
        provider_id: Int,
        address_id: Int,
        car_id: Int,
        notes: String,
        images: ArrayList<MultipartBody.Part>,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.addOrder(category_id,provider_id,address_id,car_id,notes,images,authorization,lang)
                }
            )
        }

    override suspend fun orders(
        status: String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<List<OrdersItem>>>>  =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.orders(status,authorization,lang)
                }
            )
        }

    override suspend fun orderDetails(
        order_id: Int,
        action: String?,
        reason: String?,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<OrderDetailsResponse>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.orderDetails(order_id,action,reason,authorization,lang)
                }
            )
        }

    override suspend fun orderPayment(
        order_id: Int,
        payment: String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<Int>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.orderPayment(order_id,payment,authorization,lang)
                }
            )
        }

    override suspend fun profile(
        mac_address: String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>>  =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.profile(mac_address,authorization,lang)
                }
            )
        }

    override suspend fun updateAvatar(
        mac_address: String,
        authorization: String,
        lang: String,
        avatar: MultipartBody.Part
    ): Flow<DataState<BaseResponse<UserResponse>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.updateAvatar(mac_address,authorization,lang,avatar)
                }
            )
        }

    override suspend fun changePassword(
        current_password: String,
        password: String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.changePassword(current_password,password,authorization,lang)
                }
            )
        }

    override suspend fun updateLocation(
        address: String,
        lat: String,
        lng: String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.updateLocation(address,lat, lng,authorization,lang)
                }
            )
        }

    override suspend fun updateProfile(
        mac_address: String,
        name: String,
        phone: String,
        nationality_id: Int,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<UserResponse>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.updateProfile(mac_address,name,phone,nationality_id,authorization,lang)
                }
            )
        }

    override suspend fun logOut(
        device_id: String,
        device_type: String,
        mac_address: String,
        Authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.logOut(device_id, device_type, mac_address, Authorization, lang)
                }
            )
        }

    override suspend fun terms(lang: String): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.terms(lang)
                }
            )
        }

    override suspend fun myConversations(
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<List<MyConversationsResponseItem>>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.myConversations(authorization,lang)
                }
            )
        }

    override suspend fun conversation(
        conversation_id: String,
        authorization: String,
        lang: String,
        page: String?,
        app_type:String
    ): Flow<DataState<BaseResponse<List<ConversationResponseItem>>>> =
        flow {
            emitAll(safeApiCall(dispatcher = dispatcher){
                authApi.conversation(conversation_id,authorization,lang,page,app_type)
            })
        }

    override suspend fun sendMessage(
        receiver_id: String,
        conversation_id: String,
        type: String,
        message: String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<ConversationResponseItem>>> = flow {
        emitAll(
            safeApiCall(dispatcher = dispatcher) {
                authApi.sendMessage(
                    receiver_id,
                    conversation_id,
                    type,
                    message,
                    authorization,
                    lang
                )
            }
        )
    }

    override suspend fun sendImageMessage(
        receiver_id: String,
        conversation_id: String,
        type: String,
        message: MultipartBody.Part?,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<ConversationResponseItem>>> = flow {
        emitAll(
            safeApiCall(dispatcher = dispatcher) {
                authApi.sendImageMessage(
                    receiver_id,
                    conversation_id,
                    type,
                    message,
                    authorization,
                    lang
                )
            }
        )
    }

    override suspend fun newConversation(
        receiver_id: Int,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<ConversationIdResponse>>> = flow {
        emitAll(
            safeApiCall(dispatcher = dispatcher) {
                authApi.newConversation(
                    receiver_id,
                    authorization,
                    lang
                )
            }
        )
    }


    override suspend fun notification(
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<List<NotificationResponseItem>>>> =
        flow {
            emitAll(safeApiCall(
                dispatcher = dispatcher
            ){
                authApi.notification(authorization,lang)
            })
        }

    override suspend fun deleteNotification(
        notification_id: Int,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.deleteNotification(notification_id,authorization,lang)
                }
            )
        }

    override suspend fun deleteAllNotification(
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.deleteAllNotifications(authorization,lang)
                }
            )
        }

    override suspend fun sendFile(
        file: MultipartBody.Part,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<SendFileResponse>>>  =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.sendFile(file,authorization,lang)
                }
            )
        }

    override suspend fun transferOrder(
        order_id: Int,
        provider_id: Int,
        reason: String,
        authorization: String,
        lang: String
    ): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.transferOrder(order_id,provider_id,reason,authorization,lang)
                }
            )
        }


    override suspend fun balanceRequest(
        bank_name: String,
        account_name: String,
        account_number: String,
        iban_number: String
    ): Flow<DataState<BaseResponse<String>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.balanceRequest(bank_name, account_name, account_number, iban_number)
                }
            )
        }

    override suspend fun screens(): Flow<DataState<BaseResponse<List<IntroResponseItem>>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.screens()
                }
            )
        }

    override suspend fun availabilty(
        mac_address: String,
        available: Int
    ): Flow<DataState<BaseResponse<UserResponse>>> =
        flow {
            emitAll(
                safeApiCall(dispatcher = dispatcher){
                    authApi.availabilty(mac_address, available)
                }
            )
        }
}