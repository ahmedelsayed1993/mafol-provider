package com.aait.mahfulprovider.business.data.soket

import android.app.Activity
import android.util.Log
import io.socket.client.Socket
import org.json.JSONException
import org.json.JSONObject
import javax.inject.Inject

class SocketIoMangerImpl @Inject constructor(private val socket: Socket) : SocketIoManger {

    override fun connectToSocket(helper: SocketIoHelper) {
        if (!socket.connected()) {
            socket.connect()
            onConnectSocket(helper)
            onReconnectSocket(helper)
        } else {
            helper.startSocketEvent()
        }
    }

    override fun disconnectSocket(helper: SocketIoHelper) {
        if (socket.connected()) {
            onDisconnectSocket(helper)
            socket.disconnect()
        }
    }

    override fun openChannelListening(
        activity: Activity,
        channel: String,
        socketReConnect: Int,
        helper: SocketIoHelper
    ) {
        socket.off()
        socket.on(channel) {
            try {
                val messageJson = JSONObject(it[0].toString())
                activity.runOnUiThread {
                    helper.onChannelReceivedData(channel, messageJson)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    override fun setEmit(
        emitType: String, jsonObject: JSONObject?, socketReConnect: Int,
        helper: SocketIoHelper
    ) {
        if (socket.connected()) {
            socket.emit(emitType, jsonObject)
        } else {
            connectToSocket(helper)
            if (socketReConnect <= 3) {
                setEmit(emitType, jsonObject, socketReConnect + 1, helper)
            }
        }
    }

    override fun onConnectSocket(
        helper: SocketIoHelper
    ) {
        socket.on(Socket.EVENT_CONNECT) {
            helper.startSocketEvent()
            socket.off(Socket.EVENT_CONNECT)
        }
        socket.on(Socket.EVENT_CONNECT_TIMEOUT) {
            Log.d("TAG", "onConnectSocket: $it")
        }
        socket.on(Socket.EVENT_RECONNECT_FAILED) {
            Log.d("TAG", "onConnectSocket: $it")
        }
        socket.on(Socket.EVENT_ERROR) {
            Log.d("TAG", "onConnectSocket: $it")
        }
    }

    override fun onDisconnectSocket(
        helper: SocketIoHelper
    ) {
        helper.closeSocketEvent()
        socket.disconnect()
    }

    override fun onReconnectSocket(helper: SocketIoHelper) {
        socket.on(Socket.EVENT_RECONNECT) {
            helper.startSocketEvent()
        }
    }
}