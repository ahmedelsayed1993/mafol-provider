package com.aait.mahfulprovider.business.data.network.repo

import com.aait.mahfulprovider.business.domain.api.MainApi
import com.aait.mahfulprovider.business.domain.repo.MainRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(
    private val mainApi: MainApi,
    private val dispatcher: CoroutineDispatcher
) : MainRepository {

}