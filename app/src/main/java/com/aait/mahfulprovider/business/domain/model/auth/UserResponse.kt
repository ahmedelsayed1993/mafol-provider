package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class UserResponse(
    @Json(name = "date") val date: String? = null,
    @Json(name = "code") val code: String? = null,
    @Json(name = "address") val address: String? = null,
    @Json(name = "lng") val lng: String? = null,
    @Json(name = "device_id") val device_id: String? = null,
    @Json(name = "invitation") val invitation: String? = null,
    @Json(name = "device_type") val device_type: String? = null,
    @Json(name = "avatar") val avatar: String? = null,
    @Json(name = "token") val token: String? = null,
    @Json(name = "user_type") val user_type: String? = null,
    @Json(name = "nationality") val nationality: String? = null,
    @Json(name = "nationality_id") val nationality_id: Int? = null,
    @Json(name = "phone") val phone: String? = null,
    @Json(name = "mac_address") val mac_address: String? = null,
    @Json(name = "name") val name: String? = null,
    @Json(name = "lat") val lat: String? = null,
    @Json(name = "available") val available:Int? = null,
    @Json(name = "user") val user:Int? = null
)
