package com.aait.mahfulprovider.business.domain.api

import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.*
import com.aait.mahfulprovider.util.Constants
import okhttp3.MultipartBody
import retrofit2.http.*

interface AuthApi {

    @FormUrlEncoded
    @POST("sign-up-provider")
    suspend fun SignUp(
        @Field(Constants.NAME) name:String,
        @Field(Constants.PHONE) phone: String,
        @Field("nationality_id") nationality_id:Int,
        @Field("invitation_code") invitation_code:String,
        @Field(Constants.PASSWORD) password: String,
        @Field(Constants.DEVICE_ID) device_id:String,
        @Field(Constants.MacAddress) mac_address: String,
        @Header(Constants.LANGUAGE) lang:String,
        @Field(Constants.DEVICE_TYPE) deviceType: String? = Constants.TYPE_ANDROID
    ):BaseResponse<UserResponse>

    @POST("nationalities")
    suspend fun nationality(
        @Header(Constants.LANGUAGE) lang:String
    ):BaseResponse<List<NationalityResponseItem>>

    @FormUrlEncoded
    @POST("sign-in")
    suspend fun login(
        @Field(Constants.PHONE) phone: String,
        @Field(Constants.PASSWORD) password: String,
        @Field(Constants.DEVICE_ID) deviceID: String,
        @Field(Constants.MacAddress) mac_address: String,
        @Header(Constants.LANGUAGE) lang:String,
        @Field(Constants.DEVICE_TYPE) deviceType: String? = Constants.TYPE_ANDROID
    ): BaseResponse<UserResponse>

    @FormUrlEncoded
    @POST("check-code")
    suspend fun Activate(
        @Field("code") code:String,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang:String
        ):BaseResponse<UserResponse>


    @POST("resend-code")
    suspend fun reSend(
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ):BaseResponse<UserResponse>

    @FormUrlEncoded
    @POST("forget-password")
    suspend fun forgotPass(
        @Field("phone") phone: String,
        @Header(Constants.LANGUAGE) lang: String
    ):BaseResponse<UserResponse>

    @FormUrlEncoded
    @POST("update-password")
    suspend fun updatePass(
        @Field("password") password: String,
        @Field("code") code: String,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ):BaseResponse<UserResponse>

    @POST("about")
    suspend fun about(
        @Header(Constants.LANGUAGE) lang: String
    ):BaseResponse<String>

    @POST("contact-us")
    suspend fun contactUs(
        @Header(Constants.LANGUAGE) lang: String
    ):BaseResponse<ContactUsResponse>

    @FormUrlEncoded
    @POST("complaints")
    suspend fun complaints(
        @Field("phone") phone: String,
        @Field("name") name: String,
        @Field("subject") subject:String,
        @Field("message") message:String,
        @Header(Constants.LANGUAGE) lang: String
    ):BaseResponse<String>

    @POST("wallet")
    suspend fun wallet(
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) :BaseResponse<Int>

    @POST("provider-home")
    suspend fun home(
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) :BaseResponse<HomeResponse>

    @FormUrlEncoded
    @POST("providers")
    suspend fun providers(
        @Field("lat") lat:String,
        @Field("lng") lng:String,
        @Header(Constants.LANGUAGE) lang: String
    ) :BaseResponse<List<ProvidersResponseItem>>

    @FormUrlEncoded
    @POST("add-address")
    suspend fun addAddress(
        @Field("address") address:String,
        @Field("type") type:String,
        @Field("lat") lat:String,
        @Field("lng") lng: String,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) :BaseResponse<String>

    @FormUrlEncoded
    @POST("edit-address")
    suspend fun editAddress(
        @Field("address_id") address_id: Int,
        @Field("address") address:String,
        @Field("type") type:String,
        @Field("lat") lat:String,
        @Field("lng") lng: String,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) :BaseResponse<String>

    @POST("my-addresses")
    suspend fun myAddresses(
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) :BaseResponse<List<AddressesResponseItem>>

    @FormUrlEncoded
    @POST("delete-address")
    suspend fun deleteAddress(
        @Field("address_id") address_id:Int,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) :BaseResponse<String>

    @POST("vehicles")
    suspend fun vehicles(
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) : BaseResponse<List<ListResponseItem>>

    @FormUrlEncoded
    @POST("vehicle-models")
    suspend fun models(
        @Field("vehicle_id") vehicle_id:Int,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) : BaseResponse<List<ListResponseItem>>

    @FormUrlEncoded
    @POST("add-car")
    suspend fun addCar(
        @Field("name") name:String,
        @Field("vehicle_id") vehicle_id:Int,
        @Field("model_id") model_id:Int,
        @Field("car_color") car_color:String,
        @Field("plate_number") plate_number:String,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) :BaseResponse<String>

    @POST("my-cars")
    suspend fun myCars(
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ): BaseResponse<List<MyCarsResponseItem>>

    @FormUrlEncoded
    @POST("edit-car")
    suspend fun editCar(
        @Field("car_id") car_id:Int,
        @Field("name") name:String?,
        @Field("vehicle_id") vehicle_id:Int?,
        @Field("model_id") model_id:Int?,
        @Field("car_color") car_color:String?,
        @Field("plate_number") plate_number:String?,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) :BaseResponse<EditCarResponse>

    @FormUrlEncoded
    @POST("delete-car")
    suspend fun deleteCar(
        @Field("car_id") car_id:Int,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) :BaseResponse<String>

    @Multipart
    @POST("add-order")
    suspend fun addOrder(
        @Query("category_id") category_id:Int,
        @Query("provider_id") provider_id:Int,
        @Query("address_id") address_id: Int,
        @Query("car_id") car_id: Int,
        @Query("notes") notes:String,
        @Part images :ArrayList<MultipartBody.Part>,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) :BaseResponse<String>

    @FormUrlEncoded
    @POST("provider-orders")
    suspend fun orders(
        @Field("status") status:String,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) : BaseResponse<List<OrdersItem>>

    @FormUrlEncoded
    @POST("provider-order-details")
    suspend fun orderDetails(
        @Field("order_id") order_id:Int,
        @Field("action") action:String?,
        @Field("reason_refuse") reason_refuse:String?,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) : BaseResponse<OrderDetailsResponse>

    @FormUrlEncoded
    @POST("order-payment")
    suspend fun orderPayment(
        @Field("order_id") order_id: Int,
        @Field("payment") payment:String,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) : BaseResponse<Int>

    @FormUrlEncoded
    @POST("edit-profile")
    suspend fun profile(
        @Field("mac_address") mac_address: String,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) : BaseResponse<UserResponse>

    @FormUrlEncoded
    @POST("edit-profile")
    suspend fun availabilty(
        @Field("mac_address") mac_address: String,
        @Field("available") available:Int
    ) : BaseResponse<UserResponse>
    @FormUrlEncoded
    @POST("edit-profile")
    suspend fun updateProfile(
        @Field("mac_address") mac_address: String,
        @Field("name") name:String,
        @Field("phone") phone:String,
        @Field("nationality_id") nationality_id: Int,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) : BaseResponse<UserResponse>

    @Multipart
    @POST("edit-profile")
    suspend fun updateAvatar(
        @Query("mac_address") mac_address: String,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String,
        @Part avatar:MultipartBody.Part
    ) : BaseResponse<UserResponse>

    @FormUrlEncoded
    @POST("reset-password")
    suspend fun changePassword(
        @Field("current_password") current_password:String,
        @Field("password") password: String,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ):BaseResponse<UserResponse>

    @FormUrlEncoded
    @POST("update-location")
    suspend fun updateLocation(
        @Field("address") address:String,
        @Field("lat") lat:String,
        @Field("lng") lng: String,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ):BaseResponse<String>

    @FormUrlEncoded
    @POST("log-out")
    suspend fun logOut(
        @Field("device_id") device_id:String,
        @Field("device_type") device_type:String,
        @Field("mac_address") mac_address: String,
        @Header("Authorization") Authorization:String,
        @Header(Constants.LANGUAGE) lang: String
    ) : BaseResponse<String>


    @POST("terms")
    suspend fun terms(
        @Header(Constants.LANGUAGE) lang: String
    ) : BaseResponse<String>


    @POST("my-conversations")
    suspend fun myConversations(
        @Header("Authorization") Authorization: String,
        @Header("lang") lang: String,
    ): BaseResponse<List<MyConversationsResponseItem>>

    @FormUrlEncoded
    @POST("conversation")
    suspend fun conversation(
        @Field("conversation_id") conversation_id: String,
        @Header("Authorization") Authorization: String,
        @Header("lang") lang: String,
        @Field("page") page: String? = null,
        @Field("app_type") deviceType: String,
    ): BaseResponse<List<ConversationResponseItem>>

    @FormUrlEncoded
    @POST("send-message")
    suspend fun sendMessage(
        @Field("receiver_id") receiver_id: String,
        @Field("conversation_id") conversation_id: String,
        @Field("type") type: String,
        @Field("message") message: String,
        @Header("Authorization") Authorization: String,
        @Header("lang") lang: String
    ): BaseResponse<ConversationResponseItem>

    @Multipart
    @POST("send-message")
    suspend fun sendImageMessage(
        @Query("receiver_id") receiver_id: String,
        @Query("conversation_id") conversation_id: String,
        @Query("type") type: String,
        @Part message: MultipartBody.Part?,
        @Header("Authorization") Authorization: String,
        @Header("lang") lang: String
    ): BaseResponse<ConversationResponseItem>

    @FormUrlEncoded
    @POST("conversation-id")
    suspend fun newConversation(
        @Field("receiver_id") receiver_id: Int,
        @Header("Authorization") Authorization: String,
        @Header("lang") lang: String
    ): BaseResponse<ConversationIdResponse>

    @POST("notifications")
    suspend fun notification(
        @Header("Authorization") Authorization: String,
        @Header("lang") lang: String
    ) : BaseResponse<List<NotificationResponseItem>>

    @FormUrlEncoded
    @POST("delete-notification")
    suspend fun deleteNotification(
        @Field("notification_id") notification_id:Int,
        @Header("Authorization") Authorization: String,
        @Header("lang") lang: String
    ) : BaseResponse<String>

    @POST("delete-all-notifications")
    suspend fun deleteAllNotifications(
        @Header("Authorization") Authorization: String,
        @Header("lang") lang: String
    ):BaseResponse<String>

    @Multipart
    @POST("send-file")
    suspend fun sendFile(
        @Part file: MultipartBody.Part,
        @Header("Authorization") Authorization: String,
        @Header("lang") lang: String
    ):BaseResponse<SendFileResponse>
    @FormUrlEncoded
    @POST("order-provider-transfer")
    suspend fun transferOrder(
        @Field("order_id") order_id: Int,
        @Field("provider_id") provider_id: Int,
        @Field("reason_refuse") reason_refuse:String,
        @Header("Authorization") Authorization: String,
        @Header("lang") lang: String
    ):BaseResponse<String>

    @FormUrlEncoded
    @POST("balance-request")
    suspend fun balanceRequest(
        @Field("bank_name") bank_name:String,
        @Field("account_name") account_name:String,
        @Field("account_number") account_number:String,
        @Field("iban_number") iban_number:String
    ):BaseResponse<String>
    @POST("screens")
    suspend fun screens():BaseResponse<List<IntroResponseItem>>
}