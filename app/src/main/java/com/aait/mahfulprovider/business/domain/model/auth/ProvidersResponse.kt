package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class ProvidersResponse(

	@Json(name="ProvidersResponse")
	val providersResponse: List<ProvidersResponseItem?>? = null
)

data class ProvidersResponseItem(

	@Json(name="image")
	val image: String? = null,

	@Json(name="address")
	val address: String? = null,

	@Json(name="lng")
	val lng: String? = null,

	@Json(name="distance")
	val distance: Int? = null,

	@Json(name="nationality")
	val nationality: String? = null,

	@Json(name="rate")
	val rate: Int? = null,

	@Json(name="name")
	val name: String? = null,

	@Json(name="active")
	val active: Int? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="lat")
	val lat: String? = null
)
