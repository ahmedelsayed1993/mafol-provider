package com.aait.mahfulprovider.business.data.network.utils

import com.aait.mahfulprovider.util.Constants.NETWORK_TIMEOUT
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withTimeout
import okhttp3.internal.Internal
import retrofit2.HttpException
import java.io.IOException
import java.net.UnknownHostException

suspend fun <T> safeApiCall(
    dispatcher: CoroutineDispatcher,
    apiCall: suspend () -> T,
): Flow<DataState<T>> = flow {
    withTimeout(NETWORK_TIMEOUT) {
        val response = apiCall.invoke()

        if (response != null) {
            emit(DataState.Success(response))
        } else {
            emit(DataState.Error(NetworkExceptions.UnknownException))
        }
    }
}.onStart {
    emit(DataState.Loading)
}.catch {
    emit(handleError(it))
}.flowOn(dispatcher)

fun <T> handleError(it: Throwable): DataState<T> {
    it.printStackTrace()
    return when (it) {
        is TimeoutCancellationException -> {
            DataState.Error(NetworkExceptions.TimeoutException)
        }

        is UnknownHostException -> {
            DataState.Error(NetworkExceptions.NetworkConnectionException)
        }

        is Internal -> {
            DataState.Error(NetworkExceptions.TimeoutException)
        }

        is IOException -> {
            DataState.Error(NetworkExceptions.UnknownException)
        }

        is HttpException -> {
            DataState.Error(getExceptionType(it))
        }

        else -> {
            DataState.Error(NetworkExceptions.UnknownException)
        }
    }
}

private fun getExceptionType(throwable: HttpException): Exception {
    return when (throwable.code()) {
        401 -> NetworkExceptions.UnAuthorizedException
        404, 500 -> NetworkExceptions.ServerFailException
        else -> NetworkExceptions.UnknownException
    }
}