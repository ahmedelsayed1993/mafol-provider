package com.aait.mahfulprovider.business.data.network.repo

import com.aait.mahfulprovider.business.domain.api.CommonApi
import com.aait.mahfulprovider.business.domain.repo.CommonRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class CommonRepositoryImpl @Inject constructor(
    private val commonApi: CommonApi,
    private val dispatcher: CoroutineDispatcher
) : CommonRepository {

}