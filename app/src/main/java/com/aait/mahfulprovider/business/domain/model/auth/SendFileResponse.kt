package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class SendFileResponse(
    @Json(name="file_name")
    val file_name: String? = null,

    @Json(name="file_path")
    val file_path: String? = null,
)
