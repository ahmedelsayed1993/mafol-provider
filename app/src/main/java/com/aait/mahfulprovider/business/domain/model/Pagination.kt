package com.aait.mahfulprovider.business.domain.model

import com.squareup.moshi.Json

data class Pagination(

	@Json(name="total")
	val total: Int? = null,

	@Json(name="perPage")
	val perPage: Int? = null,

	@Json(name="lastPage")
	val lastPage: Int? = null,

	@Json(name="currentPage")
	val currentPage: Int? = null
)
