package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class EditCarResponse(

	@Json(name="name")
	val name: String? = null,

	@Json(name="car_color")
	val car_color: String? = null,

	@Json(name="model")
	val model: String? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="model_id")
	val model_id: Int? = null,

	@Json(name="plate_number")
	val plate_number: String? = null,

	@Json(name="vehicle_id")
	val vehicle_id: Int? = null,

	@Json(name="vehicle")
	val vehicle: String? = null
)
