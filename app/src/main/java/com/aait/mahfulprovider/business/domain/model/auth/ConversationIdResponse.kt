package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class ConversationIdResponse(

	@Json(name="lastPage")
	val lastPage: Int? = null,

	@Json(name="conversation_id")
	val conversation_id: Int? = null,

	@Json(name="sender_id")
	val sender_id: Int? = null,

	@Json(name="receiver_id")
	val receiver_id: Int? = null
)
