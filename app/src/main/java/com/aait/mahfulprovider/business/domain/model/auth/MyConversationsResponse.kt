package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class MyConversationsResponse(

	@Json(name="MyConversationsResponse")
	val myConversationsResponse: List<MyConversationsResponseItem?>? = null
)

data class MyConversationsResponseItem(

	@Json(name="date")
	val date: String? = null,

	@Json(name="messageType")
	val messageType: String? = null,

	@Json(name="lastPage")
	val lastPage: Int? = null,

	@Json(name="user_id")
	val user_id: Int? = null,

	@Json(name="sender_id")
	val sender_id: Int? = null,

	@Json(name="conversation_id")
	val conversation_id: Int? = null,

	@Json(name="id")
	val id: Int? = null,


	@Json(name="avatar")
	val avatar: String? = null,

	@Json(name="message")
	val message: String? = null,

	@Json(name="seen")
	val seen: Int? = null,

	@Json(name="username")
	val username: String? = null
)
