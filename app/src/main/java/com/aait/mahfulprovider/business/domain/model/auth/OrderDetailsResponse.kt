package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class OrderDetailsResponse(

	@Json(name="images")
	val images: List<String?>? = null,

	@Json(name="address")
	val address: String? = null,

	@Json(name="notes")
	val notes: String? = null,

	@Json(name="distance")
	val distance: Int? = null,

	@Json(name="lng")
	val lng: String? = null,

	@Json(name="created")
	val created: String? = null,

	@Json(name="car")
	val car: String? = null,

	@Json(name = "receiver_id")
	val receiver_id: Int?=null,

	@Json(name="price")
	val price: String? = null,

	@Json(name="name")
	val name: String? = null,

	@Json(name="reason")
	val reason: String? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="rate")
	val rate: Int? = null,

	@Json(name="category")
	val category: String? = null,

	@Json(name="lat")
	val lat: String? = null,

	@Json(name="payment")
	val payment: String? = null,

	@Json(name="status")
	val status: String? = null
)
