package com.aait.mahfulprovider.business.data.soket

import org.json.JSONObject

interface SocketIoHelper {
    fun startSocketEvent()
    fun onChannelReceivedData(channel: String, messageJson: JSONObject)
    fun closeSocketEvent()
}