package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class OrdersResponse(

	@Json(name="OrdersResponse")
	val ordersResponse: List<OrdersItem?>? = null
)


