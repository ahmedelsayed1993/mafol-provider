package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class HomeResponse(

	@Json(name="orders")
	val orders: List<OrdersItem?>? = null,

	@Json(name="sliders")
	val sliders: List<SliderItem?>? = null
)

data class OrdersItem(

	@Json(name="distance")
	val distance: Int? = null,

	@Json(name="created")
	val created: String? = null,

	@Json(name="name")
	val name: String? = null,

	@Json(name="payment")
	val payment: String? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="status")
	val status: String? = null
)
data class SliderItem(
	@Json(name="description")
	val description: String? = null,

	@Json(name="image")
	val image: String? = null,

	@Json(name="id")
	val id: Int? = null
)
