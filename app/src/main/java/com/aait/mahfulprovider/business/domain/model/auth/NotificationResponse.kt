package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class NotificationResponse(

	@Json(name="NotificationResponse")
	val notificationResponse: List<NotificationResponseItem?>? = null
)

data class NotificationResponseItem(

	@Json(name="created")
	val created: String? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="type")
	val type: String? = null,

	@Json(name="order_id")
	val order_id: Int? = null,

	@Json(name="content")
	val content: String? = null
)
