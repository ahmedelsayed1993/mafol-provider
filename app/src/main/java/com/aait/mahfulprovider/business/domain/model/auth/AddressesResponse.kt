package com.aait.mahfulprovider.business.domain.model.auth

import com.squareup.moshi.Json

data class AddressesResponse(

	@Json(name="AddressesResponse")
	val addressesResponse: List<AddressesResponseItem?>? = null
)

data class AddressesResponseItem(

	@Json(name="address")
	val address: String? = null,

	@Json(name="lng")
	val lng: String? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="type")
	val type: String? = null,

	@Json(name="lat")
	val lat: String? = null,

	var checked : Boolean?=null
)
