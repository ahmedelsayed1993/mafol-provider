package com.aait.mahfulprovider.business.data.network.utils

sealed class NetworkExceptions: Exception(){
    object NetworkConnectionException: NetworkExceptions()
    object UnknownException: NetworkExceptions()
    object UnAuthorizedException: NetworkExceptions()
    object ServerFailException: NetworkExceptions()
    object TimeoutException: NetworkExceptions()
    data class CustomException(val msg: String): NetworkExceptions()
}
