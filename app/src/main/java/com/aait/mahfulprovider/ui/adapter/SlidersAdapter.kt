package com.aait.mahfulprovider.ui.adapter

import android.content.Context
import android.content.Intent
import android.transition.Slide
import android.view.View
import android.widget.ImageView
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.domain.model.auth.SliderItem


import com.bumptech.glide.Glide
import com.github.islamkhsh.CardSliderAdapter


open class SlidersAdapter(context: Context, list: ArrayList<SliderItem>) :
    CardSliderAdapter<SliderItem>(list) {

    interface SliderViewClickListener {
        fun recyclerViewListClicked(v: View?, position: Int)
    }

    protected lateinit var onItemClickListener: SliderViewClickListener

    var list = list
    var context = context

    lateinit var image: ImageView
    override fun bindView(position: Int, itemContentView: View, item: SliderItem?) {
        image = itemContentView.findViewById(R.id.image)
        Glide.with(context).load(item?.image).into(image)
        itemContentView.setOnClickListener { view ->
            onItemClickListener.recyclerViewListClicked(view, position)
        }
    }

    internal fun setOnItemClickListener(itemClickListener: SliderViewClickListener) {
        this.onItemClickListener = itemClickListener

    }

    override fun getItemContentLayout(position: Int): Int {
        return R.layout.card_image_slider
    }
}