package com.aait.mahfulprovider.ui.fragment.more_cycle

import android.provider.ContactsContract
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.NationalityResponseItem
import com.aait.mahfulprovider.business.domain.model.auth.UserResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import javax.inject.Inject
@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val authRepo:AuthRepository
) : ViewModel() {
    private val _profileResponse =
        MutableStateFlow<DataState<BaseResponse<UserResponse>>>(DataState.Idle)

    val profileResponse: MutableStateFlow<DataState<BaseResponse<UserResponse>>>
        get() = _profileResponse

    private val _availableResponse =
        MutableStateFlow<DataState<BaseResponse<UserResponse>>>(DataState.Idle)

    val availableResponse: MutableStateFlow<DataState<BaseResponse<UserResponse>>>
        get() = _availableResponse

    private val _updateAvatarResponse =
        MutableStateFlow<DataState<BaseResponse<UserResponse>>>(DataState.Idle)

    val updateAvatarResponse: MutableStateFlow<DataState<BaseResponse<UserResponse>>>
        get() = _updateAvatarResponse
    private val _nationalityResponse =
        MutableStateFlow<DataState<BaseResponse<List<NationalityResponseItem>>>>(DataState.Idle)

    val nationalityResponse: MutableStateFlow<DataState<BaseResponse<List<NationalityResponseItem>>>>
        get() = _nationalityResponse
    fun profile(
        mac_address:String,
        auth:String,
        lang:String
    ){
        viewModelScope.launch {
            authRepo.profile(mac_address,auth,lang).onEach {
                _profileResponse.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun updateProfile(
        mac_address:String,
        name:String,
        phone:String,
        nationality_id:Int,
        auth:String,
        lang:String
    ){
        viewModelScope.launch {
            authRepo.updateProfile(mac_address,name,phone,nationality_id,auth,lang).onEach {
                _updateAvatarResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
    fun updateAvatar(
        mac_address:String,
        auth:String,
        lang:String,
        avatar:MultipartBody.Part
    ){
        viewModelScope.launch {
            authRepo.updateAvatar(mac_address,auth,lang,avatar).onEach {
                _updateAvatarResponse.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun nationality(
        lang:String
    ){
        viewModelScope.launch {
            authRepo.nationality(lang).onEach {
                _nationalityResponse.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun availability(mac_address:String,
                     available:Int){
        viewModelScope.launch {
            authRepo.availabilty(mac_address,available).onEach {
                availableResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}