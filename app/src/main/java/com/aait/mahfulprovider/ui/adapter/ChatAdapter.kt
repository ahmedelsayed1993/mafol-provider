package com.aait.mahfulprovider.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.domain.model.auth.ConversationResponseItem
import com.aait.mahfulprovider.util.OnItemClickListener


import com.bumptech.glide.Glide

import de.hdodenhof.circleimageview.CircleImageView

class ChatAdapter(
    internal var context: Context,
    private var messageModels: List<ConversationResponseItem>,
    internal var id: Int,
    internal var listener:OnItemClickListener
) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {
    internal var paramsMsg: LinearLayout.LayoutParams? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v: View
        return if (viewType == 0) {
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_receive_message,
                    parent,
                    false
                )
            )
        } else
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_send_message,
                    parent,
                    false
                )
            )
    }

    override fun getItemViewType(position: Int): Int {

        return if (messageModels[position].receiver_id == id) {

            0
        } else {

            1
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (messageModels[position].messageType.equals("text")) {
            holder.msg.visibility = View.VISIBLE
            holder.image.visibility = View.GONE
            holder.msg.text = messageModels[position].message
            holder.time.text = messageModels[position].created
            Glide.with(context).load(messageModels[position].avatar).into(holder.sender)
        }
        if (messageModels[position].messageType.equals("image")){
            holder.msg.visibility = View.GONE
            holder.image.visibility = View.VISIBLE
            holder.time.text = messageModels[position].created
            Glide.with(context).load(messageModels[position].avatar).into(holder.sender)
            Glide.with(context).load(messageModels[position].message).into(holder.image)
        }
        if (messageModels[position].messageType.equals("sound")){
            holder.msg.visibility = View.VISIBLE
            holder.image.visibility = View.GONE
            holder.time.text = messageModels[position].created
            Glide.with(context).load(messageModels[position].avatar).into(holder.sender)
            holder.msg.text = context.getString(R.string.voice_sent)
        }
        holder.itemView.setOnClickListener(View.OnClickListener { view -> listener.onItemClick(view,position) })



    }

    override fun getItemCount(): Int {
        return messageModels.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var msg: TextView
        internal var sender: CircleImageView

        internal var image : ImageView
        internal var time:TextView


        init {
            msg = itemView.findViewById(R.id.message)
            sender = itemView.findViewById(R.id.avatar)
            image = itemView.findViewById(R.id.iv_message)
            time = itemView.findViewById(R.id.time)


        }
    }
}
