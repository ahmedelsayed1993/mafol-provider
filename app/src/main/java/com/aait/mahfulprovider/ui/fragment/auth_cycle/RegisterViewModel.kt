package com.aait.mahfulprovider.ui.fragment.auth_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.NationalityResponseItem
import com.aait.mahfulprovider.business.domain.model.auth.UserResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val authRepo:AuthRepository
) :ViewModel(){
    private val _registerResponse =
        MutableStateFlow<DataState<BaseResponse<UserResponse>>>(DataState.Idle)

    val registerResponse: MutableStateFlow<DataState<BaseResponse<UserResponse>>>
        get() = _registerResponse

    private val _nationalityResponse =
        MutableStateFlow<DataState<BaseResponse<List<NationalityResponseItem>>>>(DataState.Idle)

    val nationalityResponse: MutableStateFlow<DataState<BaseResponse<List<NationalityResponseItem>>>>
        get() = _nationalityResponse
    fun register(
        name:String,
        phone: String,
        nationality_id:Int,
        invitation_code:String,
        password: String,
        device_id:String,
        mac_address:String,
        lang:String
    ) {
        viewModelScope.launch {
            authRepo.register(
                name,
                phone,
                nationality_id,
                invitation_code,
                password,
                device_id,
                mac_address,
                lang
            ).onEach {
                _registerResponse.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun nationality(
        lang:String
    ){
        viewModelScope.launch {
            authRepo.nationality(lang).onEach {
                _nationalityResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}