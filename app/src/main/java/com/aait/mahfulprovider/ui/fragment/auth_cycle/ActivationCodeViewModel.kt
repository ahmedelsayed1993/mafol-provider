package com.aait.mahfulprovider.ui.fragment.auth_cycle

import android.service.autofill.Dataset
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.UserResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ActivationCodeViewModel @Inject constructor(
    private val authRepo:AuthRepository
):ViewModel (){

    private val _activateResponse =
        MutableStateFlow<DataState<BaseResponse<UserResponse>>>(DataState.Idle)

    val activateResponse: MutableStateFlow<DataState<BaseResponse<UserResponse>>>
        get() = _activateResponse

    private val _resendResponse =
        MutableStateFlow<DataState<BaseResponse<UserResponse>>>(DataState.Idle)

    val resendResponse: MutableStateFlow<DataState<BaseResponse<UserResponse>>>
        get() = _resendResponse


    fun activate(code:String,auth:String,lang:String){
        viewModelScope.launch {
            authRepo.activate(code,auth,lang)
                .onEach {
                    _activateResponse.value = it
                }.launchIn(viewModelScope)
        }
    }

    fun resend(auth:String,lang:String){
        viewModelScope.launch {
            authRepo.reSend(auth,lang)
                .onEach {
                    _resendResponse.value = it
                }.launchIn(viewModelScope)
        }
    }
}