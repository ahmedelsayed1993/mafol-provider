package com.aait.mahfulprovider.ui.fragment.order_cycle

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentCancelOrderBinding
import com.aait.mahfulprovider.ui.activity.MainActivity
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.common.getErrorMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class CancelOrderFragment : BaseFragment<FragmentCancelOrderBinding>() {
    private val viewModel by viewModels<OrderDetailsViewHolder>()
    private var order_id = 0
    override fun afterCreateView() {
        order_id = requireArguments().getInt("order_id")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        deleteOrderObserver()
    }

    override fun getFragmentView(): Int = R.layout.fragment_cancel_order
    private fun navigate(){
        binding.titleLay.back.setOnClickListener {
            onBack()
        }
        binding.titleLay.title.text = getString(R.string.order_refuse_reason)
        binding.send.setOnClickListener {
            if (Utils.checkEditError(binding.reason, getString(R.string.order_refuse_reason))) {

            } else {
                viewModel.deleteOrder(
                    order_id,
                    "refuse",
                    binding.reason.text.toString(),
                    "Bearer" + preferenceModule.userData?.token,
                    preferenceModule.language
                )
            }
        }
    }

    private fun deleteOrderObserver(){
        lifecycleScope.launchWhenStarted {
            viewModel.deleteOrderResponse.onEach {
                when(it){
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {

                            startActivity(Intent(requireActivity(),MainActivity::class.java))
                            requireActivity().finish()
                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)
        }
    }
}