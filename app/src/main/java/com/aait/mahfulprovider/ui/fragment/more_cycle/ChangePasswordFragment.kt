package com.aait.mahfulprovider.ui.fragment.more_cycle

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentChangePasswordBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class ChangePasswordFragment : BaseFragment<FragmentChangePasswordBinding>() {
    private val viewModel by viewModels<ChangePasswordViewModel>()
    override fun afterCreateView() {
    }
    override fun getFragmentView(): Int = R.layout.fragment_change_password

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        changePassObserver()

    }

    private fun navigate(){
        binding.titleLay.title.text = getString(R.string.change_password)
        binding.titleLay.back.setOnClickListener { onBack() }
        binding.save.setOnClickListener {
            if (Utils.checkEditError(binding.currentPassword,getString(R.string.current_password))||
                    Utils.checkEditError(binding.newPassword,getString(R.string.new_password))||
                    Utils.checkLength(binding.newPassword,getString(R.string.password_length),6)||
                    Utils.checkEditError(binding.confirmNewPassword,getString(R.string.confirm_new_password))){

            }else{
                if (binding.newPassword.text.toString().equals(binding.confirmNewPassword.text.toString())){
                    viewModel.changePass(binding.currentPassword.text.toString(),binding.newPassword.text.toString()
                    ,"Bearer"+preferenceModule.userData?.token,preferenceModule.language)
                }else{
                    binding.confirmNewPassword.error = getString(R.string.password_not_match)
                }
            }
        }
    }
    private fun changePassObserver(){
        lifecycleScope.launchWhenStarted { viewModel.changePassResponse.emit(DataState.Idle)
            viewModel.changePassResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                            onBack()

                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }
}