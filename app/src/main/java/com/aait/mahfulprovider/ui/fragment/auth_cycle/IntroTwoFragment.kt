package com.aait.mahfulprovider.ui.fragment.auth_cycle

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmnetIntroTwoBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.ui.fragment.more_cycle.AboutViewModel
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class IntroTwoFragment:BaseFragment<FragmnetIntroTwoBinding>() {
    private val viewModel by viewModels<ScreensViewModel>()
    override fun afterCreateView() {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        aboutObserver()
    }
    fun navigate(){
        viewModel.about()
        binding.start.setOnClickListener {
            findNavController().navigate(R.id.action_introTwoFragment_to_registerFragment)
        }
    }

    override fun getFragmentView(): Int = R.layout.fragmnet_intro_two
    private fun aboutObserver(){
        lifecycleScope.launchWhenStarted { viewModel.aboutResponse.emit(DataState.Idle)
            viewModel.aboutResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {

                            binding.content.text = it.data?.data?.get(1)?.description
                            binding.title.text = it.data?.data?.get(1)?.title
                            Glide.with(requireContext()).asBitmap().load(it.data?.data?.get(1)?.image).into(binding.image)


                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }
}