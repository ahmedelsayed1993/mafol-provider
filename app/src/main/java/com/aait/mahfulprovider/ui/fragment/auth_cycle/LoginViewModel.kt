package com.aait.mahfulprovider.ui.fragment.auth_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.AuthResponse
import com.aait.mahfulprovider.business.domain.model.auth.UserResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val authRepo: AuthRepository
) : ViewModel() {
    private val _loginResponse =
        MutableStateFlow<DataState<BaseResponse<UserResponse>>>(DataState.Idle)

    val loginResponse: MutableStateFlow<DataState<BaseResponse<UserResponse>>>
        get() = _loginResponse

    fun login(
        phone: String,
        password: String,
        device_id:String,
        mac_address:String,
        lang:String
    ) {
        viewModelScope.launch {
            authRepo.login(
                phone,
                password,
                device_id,
                mac_address,
                lang
            ).onEach {
                _loginResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}