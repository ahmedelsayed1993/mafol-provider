package com.aait.mahfulprovider.ui.fragment.more_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.ContactUsResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class ContactUsViewModel @Inject constructor(
    private val authRepo :AuthRepository
) :ViewModel(){
    private val _contactResponse =
        MutableStateFlow<DataState<BaseResponse<ContactUsResponse>>>(DataState.Idle)

    val contactResponse: MutableStateFlow<DataState<BaseResponse<ContactUsResponse>>>
        get() = _contactResponse

    fun contact(lang:String){
        viewModelScope.launch {
            authRepo.contact(lang).onEach {
                _contactResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}