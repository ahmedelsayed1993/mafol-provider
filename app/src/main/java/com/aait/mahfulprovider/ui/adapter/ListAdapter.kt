package com.aait.mahfulprovider.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.domain.model.auth.ListResponseItem
import com.aait.mahfulprovider.ui.base.ParentRecyclerAdapter
import com.aait.mahfulprovider.ui.base.ParentRecyclerViewHolder


class ListAdapter (context: Context, data: MutableList<ListResponseItem>, layoutId: Int) :
    ParentRecyclerAdapter<ListResponseItem>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.name!!.setText(listModel.name)

        viewHolder.name.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var name=itemView.findViewById<TextView>(R.id.name)


    }
}