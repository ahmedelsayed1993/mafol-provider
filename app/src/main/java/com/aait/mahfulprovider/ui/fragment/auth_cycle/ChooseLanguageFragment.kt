package com.aait.mahfulprovider.ui.fragment.auth_cycle

import android.content.Context
import android.content.res.Configuration
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.databinding.FragmentChooseLanguageBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ChooseLanguageFragment :BaseFragment<FragmentChooseLanguageBinding> () {
    override fun afterCreateView() {
        navigate()

    }

    fun navigate(){
        binding.arabic.setOnClickListener {
            preferenceModule.language = "ar"
            setConfig("ar",requireContext())
            findNavController().navigate(R.id.action_chooseLanguageFragment_to_loginFragment)
        }
        binding.english.setOnClickListener {
            preferenceModule.language = "en"
            setConfig("en",requireContext())
            findNavController().navigate(R.id.action_chooseLanguageFragment_to_loginFragment)
        }

    }

    override fun getFragmentView(): Int = R.layout.fragment_choose_language

    private fun setConfig(language: String, context: Context) {
        val locale = Locale(language)
        Locale.setDefault(locale)
        preferenceModule.language = language
        val config = Configuration()
        config.locale = locale
        context.resources.updateConfiguration(
            config,
            context.resources.displayMetrics
        )
    }
}