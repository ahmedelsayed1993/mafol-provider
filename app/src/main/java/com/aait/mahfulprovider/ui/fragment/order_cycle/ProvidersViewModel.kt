package com.aait.mahfulprovider.ui.fragment.order_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.ProvidersResponseItem
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class ProvidersViewModel @Inject constructor(
    private val authRepo:AuthRepository
) :ViewModel() {
    private val _providersResponse =
        MutableStateFlow<DataState<BaseResponse<List<ProvidersResponseItem>>>>(DataState.Idle)

    val providersResponse: MutableStateFlow<DataState<BaseResponse<List<ProvidersResponseItem>>>>
        get() = _providersResponse


    private val _transferOrderResponse =
        MutableStateFlow<DataState<BaseResponse<String>>>(DataState.Idle)

    val transferOrderResponse: MutableStateFlow<DataState<BaseResponse<String>>>
        get() = _transferOrderResponse

    fun providers(lat:String,lng:String,lang:String){
        viewModelScope.launch {
            authRepo.providers(lat,lng,lang).onEach {
                _providersResponse.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun transferOrder(order_id:Int,provider_id:Int,reason:String,auth:String,lang: String){
        viewModelScope.launch {
            authRepo.transferOrder(order_id,provider_id,reason,auth,lang).onEach {
                _transferOrderResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}