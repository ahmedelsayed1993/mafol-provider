package com.aait.mahfulprovider.ui.fragment.order_cycle

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.auth.OrdersItem
import com.aait.mahfulprovider.databinding.FragmentOrdersBinding
import com.aait.mahfulprovider.ui.adapter.OrdersAdapter
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.ui.fragment.main_cycle.HomeContainerFragmentDirections
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.OnItemClickListener
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject
@AndroidEntryPoint
class CurrentFragment @Inject constructor(): BaseFragment<FragmentOrdersBinding>() ,
    OnItemClickListener {
    private val viewModel by viewModels<OrdersViewHolder>()
    private lateinit var ordersAdapter: OrdersAdapter
    private var orders = ArrayList<OrdersItem>()
    override fun afterCreateView() {
        ordersAdapter = OrdersAdapter(requireContext(), orders, R.layout.item_home)
        ordersAdapter.setOnItemClickListener(this)
    }

    override fun getFragmentView(): Int = R.layout.fragment_orders
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        ordersObserver()
    }

    private fun navigate() {
        binding.orders.rvRecycle.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.orders.rvRecycle.adapter = ordersAdapter
        binding.orders.swipeRefresh.setColorSchemeResources(
            R.color.colorAccent,
            R.color.colorPrimaryDark,
            R.color.colorPrimary
        )
        binding.orders.swipeRefresh.setOnRefreshListener {
            viewModel.orders(
                "processing",
                "Bearer" + preferenceModule.userData?.token,
                preferenceModule.language
            )
        }
        viewModel.orders(
            "processing",
            "Bearer" + preferenceModule.userData?.token,
            preferenceModule.language
        )

    }

    private fun ordersObserver() {
        lifecycleScope.launchWhenStarted {
            viewModel.ordersResponse.onEach {
                when (it) {
                    is DataState.Loading -> {
                        binding.orders.layNoInternet!!.visibility = View.GONE
                        binding.orders.layNoItem!!.visibility = View.GONE
                        binding.orders.swipeRefresh.isRefreshing = true
                    }
                    is DataState.Success -> {
                        binding.orders.swipeRefresh.isRefreshing = false
                        if (it.data.value == "1") {
                            if (it.data.data!!.size==0){
                                binding.orders.layNoItem!!.visibility = View.VISIBLE
                                binding.orders.layNoInternet!!.visibility = View.GONE
                            }else{
                                Log.e("orders",Gson().toJson(it.data.data))
                                ordersAdapter.updateAll(it.data.data)
                            }



                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {

                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        binding.orders.layNoInternet!!.visibility = View.VISIBLE
                        binding.orders.layNoItem!!.visibility = View.GONE
                        binding.orders.swipeRefresh!!.isRefreshing = false
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)
        }
    }



    override fun onItemClick(view: View, position: Int) {
        findNavController().navigate(HomeContainerFragmentDirections.actionHomeContainerFragmentToOrderDetailsFragment(orders[position].id!!))
    }
}