package com.aait.mahfulprovider.ui.fragment.order_cycle

import android.content.Intent
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.databinding.FragmentAddDoneBinding
import com.aait.mahfulprovider.ui.activity.MainActivity
import com.aait.mahfulprovider.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddDoneFragment :BaseFragment<FragmentAddDoneBinding>() {
    override fun afterCreateView() {

        binding.back.setOnClickListener {
            startActivity(Intent(activity,MainActivity::class.java))
        }

    }

    override fun getFragmentView(): Int = R.layout.fragment_add_done

}