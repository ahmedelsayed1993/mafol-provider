package com.aait.mahfulprovider.ui.fragment.more_cycle

import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.databinding.FragmentChargeWalletBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.Utils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChargeWalletFragment : BaseFragment<FragmentChargeWalletBinding>() {

    private var type = "mada"
    override fun afterCreateView() {
         navigate()
    }

    private fun navigate(){
        binding.titleLay.back.setOnClickListener { onBack() }
        binding.titleLay.title.text = getString(R.string.recharge_the_balance)
        binding.mada.setOnClickListener {
            type = "mada"
        }
        binding.visa.setOnClickListener {
            type = "visa"
        }
        binding.confirm.setOnClickListener {
            if (Utils.checkEditError(binding.amount,getString(R.string.the_amount))){

            }else{
                findNavController().navigate(ChargeWalletFragmentDirections.actionToChargeWalletOnlineFragment(type,binding.amount.text.toString()))
            }
        }

    }

    override fun getFragmentView(): Int = R.layout.fragment_charge_wallet
}