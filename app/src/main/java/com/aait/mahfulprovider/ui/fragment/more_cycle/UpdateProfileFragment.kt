package com.aait.mahfulprovider.ui.fragment.more_cycle

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.auth.NationalityResponseItem
import com.aait.mahfulprovider.databinding.FragmentUpdateProfileBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.Constants
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.NationalityDialog
import com.aait.mahfulprovider.util.OnItemClickListener
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class UpdateProfileFragment :BaseFragment<FragmentUpdateProfileBinding>() ,OnItemClickListener{
    private val viewModel by viewModels<ProfileViewModel>()
    private lateinit var nationalityDialog: NationalityDialog
    private var nationalities = ArrayList<NationalityResponseItem>()
    private lateinit var nationalityResponseItem: NationalityResponseItem
    override fun afterCreateView() {

    }

    override fun getFragmentView(): Int = R.layout.fragment_update_profile
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        profileObserver()
        updateProfileObserver()
        nationalityObserver()
    }

    private fun navigate(){
        viewModel.profile(preferenceModule.userData?.mac_address!!,"Bearer"+preferenceModule.userData?.token,preferenceModule.language)
        binding.location.setOnClickListener {
            findNavController().navigate(UpdateProfileFragmentDirections.actionUpdateProfileFragmentToLocationFragment(preferenceModule.userData?.token!!,"profile"))
        }
        binding.titleLay.back.setOnClickListener { onBack() }
        binding.titleLay.title.text = getString(R.string.profile)
        binding.nationality.setOnClickListener {
            viewModel.nationality(preferenceModule.language)
        }
        binding.save.setOnClickListener {
            if (Utils.checkEditError(binding.name,getString(R.string.name))||
                    Utils.checkEditError(binding.phone,getString(R.string.phone_number))||
                    Utils.checkLength(binding.phone,getString(R.string.phone_length),9)){

            }else {
                viewModel.updateProfile(
                    preferenceModule.userData?.mac_address!!,
                    binding.name.text.toString(),
                    binding.phone.text.toString(),
                    nationalityResponseItem.id!!,
                    "Bearer" + preferenceModule.userData?.token,
                    preferenceModule.language
                )
            }
        }
    }

    private fun profileObserver(){
        lifecycleScope.launchWhenStarted { viewModel.profileResponse.emit(DataState.Idle)
            viewModel.profileResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            preferenceModule.userData = it.data.data
                            Constants.TOKEN = it.data.data?.token!!
                            binding.name.setText(it.data.data?.name)
                            binding.phone.setText(it.data.data?.phone)
                            binding.location.text = it.data.data?.address
                            nationalityResponseItem = NationalityResponseItem(it.data.data.nationality,it.data.data.nationality_id)
                            binding.nationality.text = it.data.data.nationality

                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }

    private fun updateProfileObserver(){
        lifecycleScope.launchWhenStarted { viewModel.updateAvatarResponse.emit(DataState.Idle)
            viewModel.updateAvatarResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            Toast.makeText(requireContext(), getString(R.string.data_updated), Toast.LENGTH_SHORT)
                                .show()
                            preferenceModule.userData = it.data.data
                            Constants.TOKEN = it.data.data?.token!!
                            binding.name.setText(it.data.data?.name)
                            binding.phone.setText(it.data.data?.phone)
                            binding.location.text = it.data.data?.address
                            onBack()
                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }

    private fun nationalityObserver(){
        lifecycleScope.launchWhenStarted {
            viewModel.nationalityResponse.emit(DataState.Idle)
        }
        viewModel.nationalityResponse.onEach {
            when (it) {
                is DataState.Loading -> {
                    progressUtil.showProgress()
                }
                is DataState.Success -> {
                    progressUtil.hideProgress()
                    if (it.data.value == "1") {
                        nationalities = it.data.data as ArrayList<NationalityResponseItem>
                        nationalityDialog = NationalityDialog(requireContext(),this,nationalities,getString(R.string.nationality))
                        nationalityDialog.show()
                    } else {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
                    }
                }
                is DataState.Error -> {
                    progressUtil.hideProgress()
                    Toast.makeText(
                        requireContext(),
                        it.exception.getErrorMessage(),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                is DataState.Idle -> {
                }
            }
        }.launchWhenStarted(viewLifecycleOwner)
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            nationalityDialog.dismiss()
            nationalityResponseItem = nationalities.get(position)
            binding.nationality.text = nationalityResponseItem.nationality
        }
    }
}