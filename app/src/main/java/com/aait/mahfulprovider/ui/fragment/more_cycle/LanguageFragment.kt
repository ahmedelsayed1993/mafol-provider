package com.aait.mahfulprovider.ui.fragment.more_cycle

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.databinding.FragmentLangBinding
import com.aait.mahfulprovider.ui.activity.SplashActivity
import com.aait.mahfulprovider.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class LanguageFragment :BaseFragment<FragmentLangBinding>() {
    private var lang = ""
    override fun afterCreateView() {
        lang = preferenceModule.language
       navigate()
    }

    override fun getFragmentView(): Int = R.layout.fragment_lang

    private fun navigate(){
        binding.titleLay.title.text = getString(R.string.language)
        binding.titleLay.back.setOnClickListener { onBack() }
        if (preferenceModule.language.equals("ar")){
            binding.arabic.isChecked = true
            binding.english.isChecked = false
        }else{
            binding.arabic.isChecked = false
            binding.english.isChecked = true
        }

        binding.english.setOnClickListener {
            binding.english.isChecked = true
            binding.arabic.isChecked = false
            lang = "en"
        }
        binding.englishLay.setOnClickListener {
            binding.english.isChecked = true
            binding.arabic.isChecked = false
            lang = "en"
        }
        binding.arabic.setOnClickListener {
            binding.english.isChecked = false
            binding.arabic.isChecked = true
            lang = "ar"
        }
        binding.arabicLay.setOnClickListener {
            binding.english.isChecked = false
            binding.arabic.isChecked = true
            lang = "ar"
        }

        binding.confirm.setOnClickListener {
            preferenceModule.language = lang
            setConfig(lang,requireContext())
            activity?.finishAffinity()
            startActivity(Intent(activity,SplashActivity::class.java))
        }
    }
    private  fun setConfig(language: String, context: Context) {
        val locale = Locale(language)
        Locale.setDefault(locale)
        preferenceModule.language = language
        val config = Configuration()
        config.locale = locale
        context.resources.updateConfiguration(
            config,
            context.resources.displayMetrics
        )
    }
}