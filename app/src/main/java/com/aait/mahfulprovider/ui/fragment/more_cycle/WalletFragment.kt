package com.aait.mahfulprovider.ui.fragment.more_cycle

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentWalletBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.common.getErrorMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class WalletFragment :BaseFragment<FragmentWalletBinding>() {
    private val viewModel by viewModels<WalletViewModel>()
    override fun afterCreateView() {

    }

    override fun getFragmentView(): Int = R.layout.fragment_wallet
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        walletObserver()
    }

    private fun navigate(){
        binding.titleLay.title.text = getString(R.string.wallet)
        binding.titleLay.back.setOnClickListener { onBack() }
        binding.pull.setOnClickListener {
            findNavController().navigate(WalletFragmentDirections.actionWalletFragmentToPullBalanceFragment())
        }
        viewModel.wallet("Bearer"+preferenceModule.userData?.token,preferenceModule.language)
        binding.recharge.setOnClickListener { findNavController().navigate(WalletFragmentDirections.actionToChargeWalletFragment()) }

    }
    private fun walletObserver(){
        lifecycleScope.launchWhenStarted { viewModel.walletResponse.emit(DataState.Idle)
            viewModel.walletResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {

                            binding.balance.text = it.data?.data.toString()+getString(R.string.rs)

                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }
}