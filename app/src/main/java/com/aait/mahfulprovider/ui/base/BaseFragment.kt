package com.aait.mahfulprovider.ui.base

import android.content.IntentFilter
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.di.NetworkHelper
import com.aait.mahfulprovider.di.PreferenceModule
import com.aait.mahfulprovider.util.*
import com.aait.mahfulprovider.util.common.NetworkExtensionsActions
import com.aait.mahfulprovider.util.gpsNetworkHelper.GpsAndNetworkChecker
import com.aait.mahfulprovider.util.gpsNetworkHelper.GpsNetworkHelper
import com.tapadoo.alerter.Alerter
import javax.inject.Inject


abstract class BaseFragment<T : ViewDataBinding> : Fragment(), NetworkExtensionsActions,
    GpsNetworkHelper {

    private var oldNetWork: Boolean = true

    @Inject
    lateinit var progressUtil: ProgressUtil

    @Inject
    lateinit var gpsAndNetworkChecker: GpsAndNetworkChecker

    @Inject
    lateinit var networkHelper: NetworkHelper

    @Inject
    lateinit var preferenceModule: PreferenceModule

    lateinit var binding: T
    var isInitialized = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        gpsAndNetworkChecker.gpsNetworkHelper = this
        if (!::binding.isInitialized) {
            binding = DataBindingUtil.inflate(
                layoutInflater,
                getFragmentView(),
                container,
                false
            )

            isInitialized = false
        } else {
            isInitialized = true
        }
        afterCreateView()

        return binding.root
    }

    abstract fun afterCreateView()

    abstract fun getFragmentView(): Int

    override fun onStart() {
        super.onStart()

        backDefaultKey()
    }

    private fun backDefaultKey() {
        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()

        requireView().setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        onBack()
                        return true
                    }
                }
                return false
            }
        })
    }

    open fun onBack() {
        requireView().hideKeyboard()
        requireActivity().onBackPressed()
    }

    override fun notAuth() {
        preferenceModule.token = null
    }

    override fun onLoad(showLoading: Boolean) {
        if (showLoading) {
            progressUtil.showProgress()
        } else {
            progressUtil.hideProgress()
        }
    }

    override fun onError(exceptionMsgId: Int?) {
        exceptionMsgId?.let {
            requireContext().showToast(getString(it), ToastType.ERROR)
        }
    }

    override fun onFail(message: String) {
        if (message.isNotEmpty()) {
            requireContext().showToast(message, ToastType.ERROR)

        }
    }

    fun startCalls(calls: (Boolean) -> Unit) {

        if (networkHelper.isNetworkConnected()) {
            calls(true)
        } else {
            networkConnectionError()
        }
    }

    open fun networkConnectionError() {
        toastNoInternetConnection(requireContext())
    }

    override fun isGpsEnabled(status: Boolean) {

    }

    override fun isNetworkEnabled(status: Boolean) {
        if (status) {
            if (!oldNetWork) {
                oldNetWork = true
                createTopToasty(
                    R.drawable.ic_wifi, getString(R.string.you_are_online_now),
                    "", "", null,
                    "", null, 0
                )
            }
        } else {
            oldNetWork = false
            createTopToasty(
                R.drawable.ic_no_wifi,
                getString(R.string.you_are_no_longer_connected_to_the_Internet),
                "", "", null,
                "", null, 0
            )
        }
    }

    private fun createTopToasty(
        icon: Int,
        text: String,
        title: String,
        titleOk: String,
        ok: View.OnClickListener?,
        titleCancel: String,
        cancel: View.OnClickListener?, duration: Long
    ) {
        Alerter.hide()
        val show = Alerter.create(requireActivity())
            .setTitle(title)
            .setText(text)
            .setContentGravity(Gravity.START)
            .setBackgroundColorRes(R.color.colorPrimary)
            .setContentGravity(Gravity.CENTER)
            .enableVibration(true)

        if (icon != 0) {
            show.setIcon(icon)
        }

        if (duration != 0.toLong()) {
            show.setDuration(duration)
        } else {
            show.setDismissable(false)
        }

        if (ok != null) {
            show.addButton(titleOk, R.style.btn_s7, ok)
        }
        if (cancel != null) {
            show.addButton(titleCancel, R.style.btn_s7, cancel)
        }

        show.show()
    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        filter.addAction("android.location.PROVIDERS_CHANGED")
        requireActivity().registerReceiver(gpsAndNetworkChecker, filter)
    }

    override fun onStop() {
        super.onStop()
//        requireActivity().unregisterReceiver(gpsAndNetworkChecker)
    }

}