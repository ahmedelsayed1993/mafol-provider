package com.aait.mahfulprovider.ui.fragment.order_cycle

import android.Manifest
import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.auth.ProvidersResponseItem
import com.aait.mahfulprovider.databinding.FragmentProvidersBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.PermissionUtils
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.aait.mahfulprovider.util.map.GPSTracker
import com.aait.mahfulprovider.util.map.GpsTrakerListener
import com.bumptech.glide.Glide
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.coroutines.flow.onEach
import java.io.IOException
import java.lang.Float
import java.util.*

@AndroidEntryPoint
class ProvidersFragment :BaseFragment<FragmentProvidersBinding>() , OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener,
    GpsTrakerListener {
    private val viewModel by viewModels<ProvidersViewModel>()
    private lateinit var googleMap: GoogleMap
    private lateinit var myMarker: Marker
    private lateinit var geocoder: Geocoder
    private lateinit var gps: GPSTracker
    private var startTracker = false
    private lateinit var markerOptions: MarkerOptions
    private var mAlertDialog: AlertDialog? = null
    private var hmap = HashMap<Marker, ProvidersResponseItem>()
    var mLang = ""
    var mLat = ""
    var result = ""
    private  var order_id = 0
    private  var reason=""
    private lateinit var provider:ProvidersResponseItem

    override fun afterCreateView() {
        order_id = requireArguments().getInt("order_id")
        reason = requireArguments().getString("reason")!!



    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        try {
            MapsInitializer.initialize(requireContext())
        } catch (e: Exception) {
            e.printStackTrace()
        }
        navigate()
        providersObserver()
        transferOrderObserver()

    }
    private fun navigate(){
        binding.back.setOnClickListener { onBack() }
        binding.title.text = getString(R.string.transfer_order_to_another_provider)
        binding.map.setOnClickListener {
            binding.providerLay.visibility = View.GONE
        }
        binding.request.setOnClickListener {
             viewModel.transferOrder(order_id,provider.id!!,reason,"Bearer"+preferenceModule.userData?.token,preferenceModule.language)
        }
    }

    override fun getFragmentView(): Int = R.layout.fragment_providers

    override fun onMapReady(p0: GoogleMap) {
        this.googleMap = p0!!
        googleMap!!.setOnMarkerClickListener(this)
        getLocationWithPermission( null)
        putMapMarker(gps.latitude,gps.longitude)
        googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(gps.latitude, gps.longitude), 9f))
        googleMap!!.setOnCameraIdleListener {
            Log.e("jjjj", "uuuuuuu")
            viewModel.providers(googleMap.cameraPosition.target.latitude.toString(),googleMap.cameraPosition.target.longitude.toString(),preferenceModule.language)
           // getData(googleMap!!.cameraPosition.target.latitude.toString(), googleMap!!.cameraPosition.target.longitude.toString(), type, null)
        }

    }

    override fun onResume() {
        super.onResume()
        getLocationWithPermission( null)
    }
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }


   private fun getLocationWithPermission( category: Int?) {
        gps = GPSTracker(requireContext()!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(requireActivity(),
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation( category)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation( category)
        }

    }

    private fun getCurrentLocation( category: Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = Utils.showAlertDialog(requireActivity(),
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                viewModel.providers(gps.latitude.toString(),gps.longitude.toString(),preferenceModule.language)
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(requireContext(), Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            requireContext(),
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address", result)
                    }
                } catch (e: IOException) {
                }
                // googleMap.clear()
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
            }
        }
    }
    override fun onMarkerClick(p0: Marker): Boolean {
        if (p0.getTitle() != "موقعى") {
            if (hmap.containsKey(p0)) {
                showprovider(hmap[p0]!!)
            }
        }
        return false
    }
    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.loc))
        )!!
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }
    private fun providersObserver(){
        lifecycleScope.launchWhenStarted { viewModel.providersResponse.emit(DataState.Idle)
            viewModel.providersResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            for (i in 0..it.data.data?.size!!-1){
                                addShop(it.data.data?.get(i)!!)
                            }

                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }

    private fun transferOrderObserver(){
        lifecycleScope.launchWhenStarted { viewModel.transferOrderResponse.emit(DataState.Idle)
            viewModel.transferOrderResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            findNavController().navigate(ProvidersFragmentDirections.actionProvidersFragmentToAddDoneFragment())

                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }

    @SuppressLint("ResourceAsColor")
    private fun showprovider(providersResponseItem: ProvidersResponseItem){
        binding.providerLay.visibility = View.VISIBLE
        provider = providersResponseItem
        Glide.with(requireContext()).asBitmap().load(providersResponseItem.image).into(binding.image)
        binding.name.text = providersResponseItem.name
        binding.nationality.text = providersResponseItem.nationality
        binding.address.text = providersResponseItem.address
        binding.rating.rating = providersResponseItem.rate!!.toFloat()
        if (providersResponseItem.active==1){
            binding.avialable.text = getString(R.string.available)
            binding.avialable.setTextColor(R.color.green_4FD477)
        }else{
            binding.avialable.text = getString(R.string.unavailable)
            binding.avialable.setTextColor(R.color.red)
        }
    }

    internal fun addShop(shopModel: ProvidersResponseItem) {
        Log.e("estate", Gson().toJson(shopModel))
        markerOptions = MarkerOptions().position(
            LatLng(Float.parseFloat(shopModel.lat!!).toDouble(), Float.parseFloat(shopModel.lng!!).toDouble())
        )
            .title("")
            .icon(BitmapDescriptorFactory.fromBitmap(createStoreMarker(shopModel.image!!)!!))
        myMarker = googleMap.addMarker(markerOptions)!!

//        googleMap!!.animateCamera(
//                CameraUpdateFactory.newLatLngZoom(LatLng(java.lang.Double.parseDouble(shopModel.lat!!), java.lang.Double.parseDouble(shopModel.lng!!)), 10f)
//        )
        hmap[myMarker] = shopModel

    }
    private fun createStoreMarker(text: String): Bitmap? {
        val markerLayout: View = layoutInflater.inflate(R.layout.custom_marker, null)
        val markerImage = markerLayout.findViewById<View>(R.id.marker_image) as CircleImageView
        Glide.with(requireContext()).load(text).into(markerImage)

        markerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
        markerLayout.layout(0, 0, markerLayout.measuredWidth, markerLayout.measuredHeight)
        val bitmap: Bitmap = Bitmap.createBitmap(markerLayout.measuredWidth, markerLayout.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        markerLayout.draw(canvas)
        return bitmap
    }



}