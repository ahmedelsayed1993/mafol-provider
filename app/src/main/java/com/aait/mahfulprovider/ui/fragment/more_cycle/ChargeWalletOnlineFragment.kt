package com.aait.mahfulprovider.ui.fragment.more_cycle

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentPayOnlineBinding
import com.aait.mahfulprovider.ui.activity.MainActivity
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.common.getErrorMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class ChargeWalletOnlineFragment :BaseFragment<FragmentPayOnlineBinding>(){

    private var type = ""
    private var price = ""
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        type = requireArguments().getString("type")!!
        price = requireArguments().getString("price")!!

        binding.web!!.settings.javaScriptEnabled = true
        Log.e(
            "link",
            "https://mahfol.4hoste.com/charge-wallet?user="+preferenceModule.userData?.user+"&methods="+type+"&price="+price
        )
        binding.web!!.loadUrl("https://mahfol.4hoste.com/charge-wallet?user="+preferenceModule.userData?.user+"&methods="+type+"&price="+price)
        Log.e("url", binding.web!!.url!!)
        binding.web!!.webViewClient = MyWebVew()




    }
    override fun afterCreateView() {

    }

    override fun getFragmentView(): Int = R.layout.fragment_pay_online

    inner class MyWebVew : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, request: String): Boolean {
            Log.e("link",request)
            if (request.contains("https://mahfol.4hoste.com/payment-fail")) {
                view.loadUrl(request)
                Handler().postDelayed({
                    Handler().postDelayed({

                        //onBackPressed()
                        requireActivity().onBackPressed()

                        //  CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
                    }, 3000)
                }, 3000)

            } else if (request.contains("https://mahfol.4hoste.com/payment-success")) {
                requireActivity().finishAffinity()
                startActivity(Intent(requireActivity(),MainActivity::class.java))


            }else{
//                view.loadUrl(request)
//                Handler().postDelayed({
//                    Handler().postDelayed({
//
//                        //onBackPressed()
//                        requireActivity().finishAffinity()
//                        startActivity(Intent(requireActivity(),MainActivity::class.java))
//
//                        //CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
//                    }, 3000)
//                }, 3000)

                requireActivity().onBackPressed()
            }
            return true
        }
    }

}