package com.aait.mahfulprovider.ui.fragment.home_page_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.NotificationResponseItem
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class NotificationViewModel @Inject constructor(
    private val authRepo : AuthRepository
) :ViewModel(){
    private val _notificationResponse =
        MutableStateFlow<DataState<BaseResponse<List<NotificationResponseItem>>>>(DataState.Idle)

    val notificationResponse: MutableStateFlow<DataState<BaseResponse<List<NotificationResponseItem>>>>
        get() = _notificationResponse



    fun myChats(auth:String,lang:String){
        viewModelScope.launch {
            authRepo.notification(auth,lang).onEach {
                _notificationResponse.value = it
            }.launchIn(viewModelScope)
        }
    }

    private val _deleteNotificationResponse =
        MutableStateFlow<DataState<BaseResponse<String>>>(DataState.Idle)

    val deleteNotificationResponse: MutableStateFlow<DataState<BaseResponse<String>>>
        get() = _deleteNotificationResponse

    fun deleteNotification(notification_id:Int,auth:String,lang:String){
        viewModelScope.launch {
            authRepo.deleteNotification(notification_id,auth,lang).onEach {
                _deleteNotificationResponse.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun deleteAllNotification(auth:String,lang:String){
        viewModelScope.launch {
            authRepo.deleteAllNotification(auth,lang).onEach {
                _deleteNotificationResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}