package com.aait.mahfulprovider.ui.fragment.home_page_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class MoreViewModel @Inject constructor(
    private val authRepo:AuthRepository
) :ViewModel(){
    private val _logoutResponse =
        MutableStateFlow<DataState<BaseResponse<String>>>(DataState.Idle)

    val logoutResponse: MutableStateFlow<DataState<BaseResponse<String>>>
        get() = _logoutResponse

    fun logOut(device_id:String,device_type:String,mac_address:String,auth:String,lang:String){
        viewModelScope.launch {
            authRepo.logOut(device_id,device_type,mac_address,auth,lang).onEach {
                _logoutResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}