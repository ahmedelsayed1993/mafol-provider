package com.aait.mahfulprovider.ui.fragment.home_page_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.HomeResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class HomeViewModel @Inject constructor(
    private val authRepo:AuthRepository
) :ViewModel(){
    private val _homeResponse =
        MutableStateFlow<DataState<BaseResponse<HomeResponse>>>(DataState.Idle)

    val homeResponse: MutableStateFlow<DataState<BaseResponse<HomeResponse>>>
        get() = _homeResponse

    fun home(auth:String,lang:String){
        viewModelScope.launch {
            authRepo.home(auth,lang).onEach {
                _homeResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}