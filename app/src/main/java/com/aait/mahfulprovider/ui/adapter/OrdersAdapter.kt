package com.aait.mahfulprovider.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.domain.model.auth.MyCarsResponseItem
import com.aait.mahfulprovider.business.domain.model.auth.OrdersItem
import com.aait.mahfulprovider.ui.base.ParentRecyclerAdapter
import com.aait.mahfulprovider.ui.base.ParentRecyclerViewHolder
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class OrdersAdapter (context: Context, data: MutableList<OrdersItem>, layoutId: Int) :
    ParentRecyclerAdapter<OrdersItem>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)

        viewHolder.name.text = listModel.name
        viewHolder.distance.text = mcontext.getString(R.string.distance_between)+listModel.distance+"KM"
        viewHolder.date.text = listModel.created
        viewHolder.num.text = listModel.id.toString()
        if (listModel.status.equals("pending")){
            viewHolder.status.text = mcontext.getString(R.string.New)
            viewHolder.status.setTextColor(mcontext.resources.getColor(R.color.red))
        }else if (listModel.status.equals("pending_payment")||listModel.status.equals("accepted")){
            viewHolder.status.text = mcontext.getString(R.string.pending_payment)
            viewHolder.status.setTextColor(mcontext.resources.getColor(R.color.colorPrimary))
        }else if (listModel.status.equals("paid")){
            viewHolder.status.text = mcontext.getString(R.string.payed)
            viewHolder.status.setTextColor(mcontext.resources.getColor(R.color.green_4FD477))
        }else if (listModel.status.equals("processing")){
            viewHolder.status.text = mcontext.getString(R.string.UnderWay)
            viewHolder.status.setTextColor(mcontext.resources.getColor(R.color.green_4FD477))
        }else if (listModel.status.equals("completed")){
            viewHolder.status.text = mcontext.getString(R.string.finished)
            viewHolder.status.setTextColor(mcontext.resources.getColor(R.color.colorPrimary))
        }

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })


    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var distance = itemView.findViewById<TextView>(R.id.distance)
        internal var date = itemView.findViewById<TextView>(R.id.date)
        internal var num = itemView.findViewById<TextView>(R.id.num)
        internal var status = itemView.findViewById<TextView>(R.id.status)


    }
}