package com.aait.mahfulprovider.ui.fragment.more_cycle

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentContactUsBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.common.getErrorMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class ContactUsFragment :BaseFragment<FragmentContactUsBinding>() {
    private val viewModel by viewModels<ContactUsViewModel>()
    private var insta = ""
    private var face = ""
    private var twitte = ""
    private var snap = ""
    override fun afterCreateView() {}
    override fun getFragmentView(): Int = R.layout.fragment_contact_us
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        contactObserver()
    }

    private fun navigate(){
        viewModel.contact(preferenceModule.language)
        binding.titleLay.title.text = getString(R.string.contact_us)
        binding.titleLay.back.setOnClickListener {
            onBack()
        }
        binding.call.setOnClickListener {
           Utils.openWhats(requireContext(),binding.whats.text.toString())
        }
        binding.facebook.setOnClickListener {
            Utils.openBrowser(requireContext(),face)
        }
        binding.instagram.setOnClickListener {
            Utils.openBrowser(requireContext(),insta)
        }
        binding.twitter.setOnClickListener {
            Utils.openBrowser(requireContext(),twitte)
        }
        binding.snapchat.setOnClickListener {
            Utils.openBrowser(requireContext(),snap)
        }
    }

    private fun contactObserver(){
        lifecycleScope.launchWhenStarted { viewModel.contactResponse.emit(DataState.Idle)
            viewModel.contactResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {

                            binding.whats.text = it.data?.data?.whatsapp
                            for ( i in 0..it.data?.data?.socials!!.size-1) {
                                if (it.data?.data?.socials[i]!!.name.equals("facebook")){
                                    face = it.data?.data?.socials[i]!!.link!!
                                }
                                else if (it.data?.data?.socials[i]!!.name.equals("instagram")){
                                    insta = it.data?.data?.socials[i]!!.link!!
                                }
                                else if (it.data?.data?.socials[i]!!.name.equals("twitter")){
                                    twitte = it.data?.data?.socials[i]!!.link!!
                                }else if (it.data?.data?.socials[i]!!.name.equals("snapchat")){
                                    snap = it.data?.data?.socials[i]!!.link!!
                                }
                            }

                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }
}