package com.aait.mahfulprovider.ui.base

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import com.aait.mahfulprovider.di.PreferenceModule
import com.aait.mahfulprovider.util.ProgressUtil
import com.aait.mahfulprovider.util.ToastType
import com.aait.mahfulprovider.util.common.NetworkExtensionsActions
import com.aait.mahfulprovider.util.showToast
import javax.inject.Inject

abstract class BaseDialogFragment<T : ViewDataBinding> : DialogFragment(),
    NetworkExtensionsActions {

    private var isInitialized: Boolean = false

    @Inject
    lateinit var progressUtil: ProgressUtil

    @Inject
    lateinit var preferenceModule: PreferenceModule

    lateinit var binding: T

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(getFragmentView(), container, false)
//        val displayRectangle = Rect()
//        val window = requireActivity().window
//        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
//        view.minimumWidth = ((displayRectangle.width() * 0.9f).toInt())
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        if (!::binding.isInitialized) {
            binding = DataBindingUtil.bind(view)!!
            isInitialized = false
        } else {
            isInitialized = true
        }

        return binding.root
    }

    abstract fun getFragmentView(): Int

    override fun notAuth() {
        preferenceModule.token = null
    }

    override fun onLoad(showLoading: Boolean) {
        if (showLoading) {
            progressUtil.showProgress()
        } else {
            progressUtil.hideProgress()
        }
    }

    override fun onError(exceptionMsgId: Int?) {
        exceptionMsgId?.let {
            requireContext().showToast(getString(it), ToastType.ERROR)
        }
    }

    override fun onFail(message: String) {
        if (message.isNotEmpty()) {
            requireContext().showToast(message, ToastType.ERROR)

        }
    }
}