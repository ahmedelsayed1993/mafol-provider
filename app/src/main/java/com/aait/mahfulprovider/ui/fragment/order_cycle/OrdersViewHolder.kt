package com.aait.mahfulprovider.ui.fragment.order_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.OrdersItem
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class OrdersViewHolder @Inject constructor(
    private val authRepo : AuthRepository
) : ViewModel(){
    private val _ordersResponse =
        MutableStateFlow<DataState<BaseResponse<List<OrdersItem>>>>(DataState.Idle)

    val ordersResponse: MutableStateFlow<DataState<BaseResponse<List<OrdersItem>>>>
        get() = _ordersResponse

    fun orders(status:String,auth : String ,lang:String){
        viewModelScope.launch {
            authRepo.orders(status,auth,lang).onEach {
                _ordersResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}