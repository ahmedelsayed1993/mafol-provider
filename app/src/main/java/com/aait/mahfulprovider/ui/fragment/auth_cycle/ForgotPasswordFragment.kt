package com.aait.mahfulprovider.ui.fragment.auth_cycle

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentForgotPasswordBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.common.getErrorMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class ForgotPasswordFragment : BaseFragment<FragmentForgotPasswordBinding>() {

    private val viewModel by viewModels<ForgotPasswordViewModel>()
    override fun afterCreateView() {

        navigate()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        loginObserver()
    }

    private fun navigate(){
        binding.send.setOnClickListener {
            if (Utils.checkEditError(binding.phone,getString(R.string.phone_number))){

            }else{
                forgotPass()
            }
        }
    }


    override fun getFragmentView(): Int = R.layout.fragment_forgot_password

    private fun forgotPass(){
        viewModel.forgot(binding.phone.text.toString(),preferenceModule.language)
    }

    private fun loginObserver() {
        lifecycleScope.launchWhenStarted { viewModel.forgotResponse.emit(DataState.Idle) }

        viewModel.forgotResponse.onEach {

            when (it) {
                is DataState.Loading -> {
                    progressUtil.showProgress()
                }
                is DataState.Success -> {
                    progressUtil.hideProgress()

                    if (it.data.value == "1") {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
                        findNavController().navigate(ForgotPasswordFragmentDirections.actionForgotPasswordFragmentToNewPasswordFragment(it.data.data?.token!!))

                    }
                    else {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
                    }
                }
                is DataState.Error -> {
                    progressUtil.hideProgress()
                    Toast.makeText(
                        requireContext(),
                        it.exception.getErrorMessage(),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                is DataState.Idle -> {

                }
            }
        }.launchWhenStarted(viewLifecycleOwner)
    }
}