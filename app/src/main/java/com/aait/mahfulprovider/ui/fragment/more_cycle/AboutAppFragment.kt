package com.aait.mahfulprovider.ui.fragment.more_cycle

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentAboutAppBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.ui.fragment.auth_cycle.RegisterFragmentDirections
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.common.getErrorMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class AboutAppFragment :BaseFragment<FragmentAboutAppBinding>() {
    private val viewModel by viewModels<AboutViewModel>()
    override fun afterCreateView() {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        aboutObserver()
    }
    private fun navigate(){
        viewModel.About(preferenceModule.language)
        binding.titleLay.back.setOnClickListener {
            onBack()
        }
        binding.titleLay.title.text = getString(R.string.about_app)
    }

    override fun getFragmentView(): Int = R.layout.fragment_about_app

    private fun aboutObserver(){
        lifecycleScope.launchWhenStarted { viewModel.aboutResponse.emit(DataState.Idle)
            viewModel.aboutResponse.onEach {

                    when (it) {
                        is DataState.Loading -> {
                            progressUtil.showProgress()
                        }
                        is DataState.Success -> {
                            progressUtil.hideProgress()
                            if (it.data.value == "1") {

                               binding.about.text = it.data?.data

                            } else {
                                Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                        is DataState.Error -> {
                            progressUtil.hideProgress()
                            Toast.makeText(
                                requireContext(),
                                it.exception.getErrorMessage(),
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                        is DataState.Idle -> {
                        }
                    }
                }.launchWhenStarted(viewLifecycleOwner)


        }
    }
}