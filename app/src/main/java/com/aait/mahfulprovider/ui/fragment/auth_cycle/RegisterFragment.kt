package com.aait.mahfulprovider.ui.fragment.auth_cycle

import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.auth.NationalityResponseItem
import com.aait.mahfulprovider.databinding.FragmentRegisterBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.*
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson

import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class RegisterFragment :BaseFragment<FragmentRegisterBinding>() ,OnItemClickListener{
    private val viewModel by viewModels<RegisterViewModel>()
    private var ID = ""
    private var deviceID = ""
    private lateinit var nationalityDialog: NationalityDialog
    private var nationalities = ArrayList<NationalityResponseItem>()
    private lateinit var nationalityResponseItem: NationalityResponseItem
    override fun afterCreateView() {
       navigate()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ID = Settings.Secure.getString(activity?.contentResolver, Settings.Secure.ANDROID_ID)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device", deviceID)
            // Log and toast

        })
        navigate()
        userSignUpObserver()
        nationalityObserver()
    }
    private fun navigate(){
        binding.login.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }
        binding.nationality.setOnClickListener {
            viewModel.nationality(preferenceModule.language)
        }
        binding.register.setOnClickListener {
           if (Utils.checkEditError(binding.name, getString(R.string.name))||
                   Utils.checkEditError(binding.phone,getString(R.string.phone_number))||
                   Utils.checkTextError(binding.nationality,getString(R.string.nationality))||
                   Utils.checkLength(binding.phone,getString(R.string.phone_length),9)||

                   Utils.checkEditError(binding.password,getString(R.string.password))||
                   Utils.checkLength(binding.password,getString(R.string.password_length),6)||
                   Utils.checkEditError(binding.confirmPassword,getString(R.string.confirm_password))){

           }else{
               if (!binding.password.text.toString().equals(binding.confirmPassword.text.toString())){
                   binding.confirmPassword.error = getString(R.string.password_not_match)
               }else{
                   if (!binding.check.isChecked){
                       Toast.makeText(requireContext(),getString(R.string.agree_on_terms),Toast.LENGTH_LONG).show()
                   }else{
                       viewModel.register(binding.name.text.toString(),binding.phone.text.toString(),nationalityResponseItem.id!!
                           ,binding.code.text.toString(),binding.password.text.toString(),deviceID,
                       ID,preferenceModule.language)
                   }
               }
           }
        }

        binding.terms.setOnClickListener {
            findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToTermsFragment())
        }
    }

    override fun getFragmentView(): Int  = R.layout.fragment_register

    private fun userSignUpObserver() {
        lifecycleScope.launchWhenStarted { viewModel.registerResponse.emit(DataState.Idle) }

        viewModel.registerResponse.onEach {
            when (it) {
                is DataState.Loading -> {
                    progressUtil.showProgress()
                }
                is DataState.Success -> {
                    progressUtil.hideProgress()
                    if (it.data.value == "1") {
                        Constants.TOKEN = it.data.data?.token!!
                        findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToActivateCodeFragment(it.data.data?.token!!))
                    } else {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
                    }
                }
                is DataState.Error -> {
                    progressUtil.hideProgress()
                    Toast.makeText(
                        requireContext(),
                        it.exception.getErrorMessage(),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                is DataState.Idle -> {
                }
            }
        }.launchWhenStarted(viewLifecycleOwner)
    }

    private fun nationalityObserver(){
        lifecycleScope.launchWhenStarted {
            viewModel.nationalityResponse.emit(DataState.Idle)
        }
        viewModel.nationalityResponse.onEach {
            when (it) {
                is DataState.Loading -> {
                    progressUtil.showProgress()
                }
                is DataState.Success -> {
                    progressUtil.hideProgress()
                    if (it.data.value == "1") {
                        nationalities = it.data.data as ArrayList<NationalityResponseItem>
                        nationalityDialog = NationalityDialog(requireContext(),this,nationalities,getString(R.string.nationality))
                        nationalityDialog.show()
                    } else {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
                    }
                }
                is DataState.Error -> {
                    progressUtil.hideProgress()
                    Toast.makeText(
                        requireContext(),
                        it.exception.getErrorMessage(),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                is DataState.Idle -> {
                }
            }
        }.launchWhenStarted(viewLifecycleOwner)
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            nationalityDialog.dismiss()
            nationalityResponseItem = nationalities.get(position)
            binding.nationality.text = nationalityResponseItem.nationality
        }
    }
}