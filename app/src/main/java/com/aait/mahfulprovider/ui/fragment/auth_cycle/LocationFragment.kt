package com.aait.mahfulprovider.ui.fragment.auth_cycle

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentLocationBinding
import com.aait.mahfulprovider.ui.activity.MainActivity
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.PermissionUtils
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.aait.mahfulprovider.util.map.GPSTracker
import com.aait.mahfulprovider.util.map.GpsTrakerListener
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach
import java.io.IOException
import java.util.*

@AndroidEntryPoint
class LocationFragment : BaseFragment<FragmentLocationBinding>() , OnMapReadyCallback,
    GoogleMap.OnMapClickListener,
    GpsTrakerListener {
    private val viewModel by viewModels<LocationViewModel>()
    private lateinit var googleMap: GoogleMap
    private lateinit var myMarker: Marker
    private lateinit var geocoder: Geocoder
    private lateinit var gps: GPSTracker
    private var startTracker = false
    private lateinit var markerOptions: MarkerOptions
    private var mAlertDialog: AlertDialog? = null
    var mLang = ""
    var mLat = ""
    var result = ""
    private var token = ""
    private var type = ""
    override fun afterCreateView() {
        token = requireArguments().getString("token")!!
        type = requireArguments().getString("type")!!
    }
    override fun getFragmentView(): Int = R.layout.fragment_location

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        try {
            MapsInitializer.initialize(requireContext())
        } catch (e: Exception) {
            e.printStackTrace()
        }
        navigate()
        locationObserver()
    }


    private fun navigate(){
        binding.back.setOnClickListener { onBack() }
        binding.confirm.setOnClickListener {
            viewModel.location(result,mLat,mLang,"Bearer"+token,preferenceModule.language)
        }

    }

    override fun onMapReady(p0: GoogleMap) {
        this.googleMap = p0!!
        googleMap!!.setOnMapClickListener(this)
        getLocationWithPermission()
        putMapMarker(gps.getLatitude(), gps.getLongitude())

    }

    override fun onResume() {
        super.onResume()
        getLocationWithPermission( )
    }
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }


    private fun getLocationWithPermission() {
        gps = GPSTracker(requireContext()!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(requireActivity(),
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation( )
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation( )
        }

    }

    private fun getCurrentLocation( ) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = Utils.showAlertDialog(requireActivity(),
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(requireContext(), Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            requireContext(),
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        binding.address.text = result
                        Log.e("address", result)
                    }
                } catch (e: IOException) {
                }
                // googleMap.clear()

            }
        }
    }

    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.placeholder))
        )!!
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f))
        // kkjgj

    }
    override fun onMapClick(p0: LatLng) {
        Log.e("LatLng", p0.toString())
        mLang =  java.lang.Double.toString(p0?.latitude!!)
        mLat =  java.lang.Double.toString(p0?.longitude!!)
        if (myMarker != null) {
            myMarker.remove()
            putMapMarker(p0?.latitude, p0?.longitude)
        } else {
            putMapMarker(p0?.latitude, p0?.longitude)
        }

        if (p0?.latitude != 0.0 && p0?.longitude != 0.0) {
            putMapMarker(p0?.latitude, p0?.longitude)
            mLat = p0?.latitude.toString()
            mLang = p0?.longitude.toString()
            val addresses: List<Address>

            geocoder = Geocoder(requireContext(), Locale.getDefault())

            try {
                addresses = geocoder.getFromLocation(
                    java.lang.Double.parseDouble(mLat),
                    java.lang.Double.parseDouble(mLang),
                    1
                )


                if (addresses.isEmpty()) {
                    Toast.makeText(
                        requireContext(),
                        resources.getString(R.string.detect_location),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    result = addresses[0].getAddressLine(0)
                    binding.address.text = result
                    Log.e("address",result)

                }


            } catch (e: IOException) {
            }

            googleMap.clear()
            putMapMarker(p0?.latitude, p0?.longitude)
        }

    }

    private fun locationObserver(){
        lifecycleScope.launchWhenStarted { viewModel.locationResponse.emit(DataState.Idle) }

        viewModel.locationResponse.onEach {

            when (it) {
                is DataState.Loading -> {
                    progressUtil.showProgress()
                }
                is DataState.Success -> {
                    progressUtil.hideProgress()

                    if (it.data.value == "1") {
                        Toast.makeText(requireContext(), it.data.data, Toast.LENGTH_SHORT).show()
                        if (type=="auth"){
                            findNavController().navigate(LocationFragmentDirections.actionLocationFragment2ToLoginFragment())
                        }else {
                            startActivity(Intent(activity, MainActivity::class.java))
                        }
                    }
                    else {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
                    }
                }
                is DataState.Error -> {
                    progressUtil.hideProgress()
                    Toast.makeText(
                        requireContext(),
                        it.exception.getErrorMessage(),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                is DataState.Idle -> {

                }
            }
        }.launchWhenStarted(viewLifecycleOwner)
    }

}