package com.aait.mahfulprovider.ui.base

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.aait.mahfulprovider.di.PreferenceModule
import com.aait.mahfulprovider.util.ProgressUtil
import com.aait.mahfulprovider.util.ToastType
import com.aait.mahfulprovider.util.common.NetworkExtensionsActions
import com.aait.mahfulprovider.util.hideKeyboard
import com.aait.mahfulprovider.util.showToast
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import javax.inject.Inject

abstract class BaseBottomSheetFragment<T : ViewDataBinding> : BottomSheetDialogFragment(),
    NetworkExtensionsActions {

    private var isInitialized: Boolean = false

    @Inject
    lateinit var progressUtil: ProgressUtil

    @Inject
    lateinit var preferenceModule: PreferenceModule

    lateinit var binding: T

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!::binding.isInitialized) {
            binding = DataBindingUtil.inflate(
                layoutInflater,
                getFragmentView(),
                container,
                false
            )

            isInitialized = false
        } else {
            isInitialized = true
        }

        backDefaultKey()

        return binding.root
    }

    abstract fun getFragmentView(): Int

    private fun backDefaultKey() {
        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()

        requireView().setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        onBack()
                        return true
                    }
                }
                return false
            }
        })
    }

    fun onBack() {
        requireView().hideKeyboard()
    }

    override fun notAuth() {
        preferenceModule.token = null
    }

    override fun onLoad(showLoading: Boolean) {
        if (showLoading) {
            progressUtil.showProgress()
        } else {
            progressUtil.hideProgress()
        }
    }

    override fun onError(exceptionMsgId: Int?) {
        exceptionMsgId?.let {
            requireContext().showToast(getString(it), ToastType.ERROR)
        }
    }

    override fun onFail(message: String) {
        if (message.isNotEmpty()) {
            requireContext().showToast(message, ToastType.ERROR)

        }
    }
}