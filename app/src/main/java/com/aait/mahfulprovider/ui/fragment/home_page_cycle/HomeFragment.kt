package com.aait.mahfulprovider.ui.fragment.home_page_cycle

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.data.soket.SocketIoHelper
import com.aait.mahfulprovider.business.data.soket.SocketIoManger

import com.aait.mahfulprovider.business.domain.model.auth.OrdersItem
import com.aait.mahfulprovider.business.domain.model.auth.SliderItem
import com.aait.mahfulprovider.databinding.FragmentHomeBinding
import com.aait.mahfulprovider.ui.activity.MainActivity
import com.aait.mahfulprovider.ui.adapter.OrdersAdapter
import com.aait.mahfulprovider.ui.adapter.SlidersAdapter

import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.ui.fragment.main_cycle.HomeContainerFragmentDirections
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.OnItemClickListener
import com.aait.mahfulprovider.util.PermissionUtils
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.aait.mahfulprovider.util.map.GPSTracker
import com.aait.mahfulprovider.util.map.GpsTrakerListener
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.gson.Gson
import com.zhpan.indicator.enums.IndicatorStyle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach
import org.json.JSONObject
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment @Inject constructor() : BaseFragment<FragmentHomeBinding>(), OnItemClickListener,
    SlidersAdapter.SliderViewClickListener, SocketIoHelper, GpsTrakerListener {
    private val viewModel by viewModels<HomeViewModel>()
    private lateinit var homeAdapter: OrdersAdapter
    private var categories = ArrayList<OrdersItem>()
    private lateinit var slidersAdapter: SlidersAdapter
    private lateinit var sliderImage: ArrayList<SliderItem>
    private lateinit var gps: GPSTracker

    @Inject
    lateinit var socketIoManager: SocketIoManger
    override fun afterCreateView() {
        binding.titleLay.title.text = getString(R.string.main)
        homeAdapter = OrdersAdapter(requireContext(), categories, R.layout.item_home)
        homeAdapter.setOnItemClickListener(this)
        if (checkLocationPermission()) {
            gps = GPSTracker(requireContext(), this)
            gps.getLocation()
            MainActivity().sendLocationToSocket(
                requireActivity(),
                preferenceModule.userData?.user!!,
                socketIoManager,
                this
            )
        }else{
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                ), 800
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        homeObserver()
    }

    private fun navigate() {
        binding.indicator.setIndicatorStyle(IndicatorStyle.DASH)
        binding.orders.rvRecycle.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.orders.rvRecycle.adapter = homeAdapter
        binding.orders.swipeRefresh.setOnRefreshListener {
            viewModel.home("Bearer" + preferenceModule.userData?.token, preferenceModule.language)
        }
        viewModel.home("Bearer" + preferenceModule.userData?.token, preferenceModule.language)
    }

    override fun getFragmentView(): Int = R.layout.fragment_home
    override fun onItemClick(view: View, position: Int) {
        findNavController().navigate(
            HomeContainerFragmentDirections.actionHomeContainerFragmentToOrderDetailsFragment(
                categories[position].id!!
            )
        )
    }

    private fun initSliderAds(list: ArrayList<SliderItem>) {
        if (list.isEmpty()) {
            binding.viewPager.visibility = View.GONE
            binding.indicator.visibility = View.GONE
        } else {
            binding.viewPager.visibility = View.VISIBLE
            binding.indicator.visibility = View.VISIBLE
            slidersAdapter = SlidersAdapter(requireContext(), list)
            slidersAdapter.setOnItemClickListener(this)
            binding.viewPager.adapter = slidersAdapter
            binding.indicator.setupWithViewPager(binding.viewPager)
        }
    }

    private fun homeObserver() {
        lifecycleScope.launchWhenStarted {
            viewModel.homeResponse.emit(DataState.Idle)
            viewModel.homeResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                        binding.orders.layNoInternet!!.visibility = View.GONE
                        binding.orders.layNoItem!!.visibility = View.GONE
                        binding.orders.swipeRefresh.isRefreshing = true
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        binding.orders.swipeRefresh.isRefreshing = false
                        if (it.data.value == "1") {
                            sliderImage = it.data.data?.sliders!! as ArrayList<SliderItem>
                            initSliderAds(it.data.data?.sliders!! as ArrayList<SliderItem>)
                            if (it.data.data.orders!!.size == 0) {
                                binding.orders.layNoItem!!.visibility = View.VISIBLE
                                binding.orders.layNoInternet!!.visibility = View.GONE
                            } else {
                                homeAdapter.updateAll(it.data.data.orders!! as List<OrdersItem>)
                            }
                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        binding.orders.layNoInternet!!.visibility = View.VISIBLE
                        binding.orders.layNoItem!!.visibility = View.GONE
                        binding.orders.swipeRefresh!!.isRefreshing = false
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }

    fun checkLocationPermission(): Boolean {

        return ((ContextCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED))
    }

    override fun recyclerViewListClicked(v: View?, position: Int) {
        findNavController().navigate(
            HomeContainerFragmentDirections.actionHomeContainerFragmentToImageFragment(
                sliderImage.get(position).image!!,
                sliderImage.get(position).description!!
            )
        )

    }

    override fun startSocketEvent() {

    }

    override fun onChannelReceivedData(channel: String, messageJson: JSONObject) {

    }

    override fun closeSocketEvent() {

    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 800 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            gps = GPSTracker(requireContext()!!, this)
            gps.getLocation()
            MainActivity().sendLocationToSocket(
                requireActivity(),
                preferenceModule.userData?.user!!,
                socketIoManager,
                this
            )
        }
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {

    }

    override fun onStartTracker() {

    }

}