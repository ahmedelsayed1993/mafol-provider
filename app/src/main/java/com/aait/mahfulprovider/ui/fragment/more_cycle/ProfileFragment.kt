package com.aait.mahfulprovider.ui.fragment.more_cycle

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentProfileBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.Constants
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.PermissionUtils
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

@AndroidEntryPoint
class ProfileFragment : BaseFragment<FragmentProfileBinding>() {
    private val viewModel by viewModels<ProfileViewModel>()

    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options : Options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        // .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
        .setVideoDurationLimitinSeconds(30)
        .setMode(Options.Mode.All)
    private var imageBasePath = ""
    override fun afterCreateView() {

    }

    override fun getFragmentView(): Int = R.layout.fragment_profile

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e("token",Gson().toJson(preferenceModule.userData))
        navigate()
        profileObserver()
        avatarObserver()
        availableObserver()
    }

    private fun navigate(){
        binding.titleLay.provider.setImageResource(R.mipmap.edit)
        binding.titleLay.title.text = getString(R.string.profile)
        binding.titleLay.back.setOnClickListener { onBack() }
        binding.changePass.setOnClickListener {
            findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToChangePasswordFragment())
        }
        viewModel.profile(preferenceModule.userData?.mac_address!!,"Bearer"+preferenceModule.userData?.token,preferenceModule.language)
        binding.image.setOnClickListener { pickImage() }
        binding.titleLay.provider.setOnClickListener {
            findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToUpdateProfileFragment())
        }
//        binding.swAvailable.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) {
//                viewModel.availability(preferenceModule.userData?.mac_address!!,1)
//            } else {
//                viewModel.availability(preferenceModule.userData?.mac_address!!,0)
//            }
//        })
        binding.swAvailable.setOnClickListener {
            if (preferenceModule.userData?.available==1){
                viewModel.availability(preferenceModule.userData?.mac_address!!,0)
            }else{
                viewModel.availability(preferenceModule.userData?.mac_address!!,1)
            }
        }
    }

    private fun pickImage(){
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(
                    requireContext(),
                    android.Manifest.permission.CAMERA
                )&& PermissionUtils.hasPermissions(
                    requireContext(),
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                )&& PermissionUtils.hasPermissions(
                    requireContext(),
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                        )) {
                Log.e("tag","Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.IMAGE_PERMISSIONS,
                        400
                    )
                }
            } else {
                Pix.start(this, options)
                Log.e("tag","Permission is granted before")
            }
        } else {
            Log.e("tag","SDK minimum than 23")
            Pix.start(this, options)
        }
    }

    private fun update(path:String){
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        viewModel.updateAvatar(preferenceModule.userData?.mac_address!!,"Bearer"+preferenceModule.userData?.token,preferenceModule.language,filePart)

    }

    @SuppressLint("ResourceAsColor")
    private fun profileObserver(){
        lifecycleScope.launchWhenStarted { viewModel.profileResponse.emit(DataState.Idle)
            viewModel.profileResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            Constants.TOKEN = it.data.data?.token!!
                            preferenceModule.userData = it.data.data
                            binding.name.text = it.data.data?.name
                            Glide.with(requireContext()).asBitmap().load(it.data.data?.avatar).into(binding.image)
                            binding.phone.text = it.data.data?.phone
                            binding.address.text = it.data.data?.address
                            if (it.data.data?.available==1){
                                binding.swAvailable.isChecked = true

                                binding.availableText.text = getString(R.string.available)
                            }else{
                                binding.swAvailable.isChecked = false

                                binding.availableText.text = getString(R.string.unavailable)
                            }

                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }
    @SuppressLint("ResourceAsColor")
    private fun availableObserver(){
        lifecycleScope.launchWhenStarted { viewModel.availableResponse.emit(DataState.Idle)
            viewModel.availableResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            Log.e("user",Gson().toJson(it.data.data))
                            Constants.TOKEN = it.data.data?.token!!
                            preferenceModule.userData = it.data.data
                            binding.name.text = it.data.data?.name
                            Glide.with(requireContext()).asBitmap().load(it.data.data?.avatar).into(binding.image)
                            binding.phone.text = it.data.data?.phone
                            binding.address.text = it.data.data?.address
                            if (it.data.data?.available==1){
                                binding.swAvailable.isChecked = true

                                binding.availableText.text = getString(R.string.available)
                            }else{
                                binding.swAvailable.isChecked = false

                                binding.availableText.text = getString(R.string.unavailable)
                            }

                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }
    @SuppressLint("ResourceAsColor")
    private fun avatarObserver(){
        lifecycleScope.launchWhenStarted { viewModel.updateAvatarResponse.emit(DataState.Idle)
            viewModel.updateAvatarResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            Toast.makeText(requireContext(), getString(R.string.data_updated), Toast.LENGTH_SHORT)
                                .show()
                            preferenceModule.userData = it.data.data
                            Constants.TOKEN = it.data.data?.token!!
                            binding.name.text = it.data.data?.name
                            Glide.with(requireContext()).asBitmap().load(it.data.data?.avatar).into(binding.image)
                            binding.phone.text = it.data.data?.phone
                            binding.address.text = it.data.data?.address
                            if (it.data.data?.available==1){
                                binding.swAvailable.isChecked = true

                                binding.availableText.text = getString(R.string.available)
                            }else{
                                binding.swAvailable.isChecked = false

                                binding.availableText.text = getString(R.string.unavailable)
                            }
                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue?.clear()
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)
                imageBasePath = returnValue?.get(0) !!
                Glide.with(requireContext()).asBitmap().load(imageBasePath).into(binding.image)
                Log.e("path", imageBasePath)
                update(imageBasePath)
            }
        }
    }
}