package com.aait.mahfulprovider.ui.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Log
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.di.PreferenceModule
import com.aait.mahfulprovider.ui.base.BaseActivity
import com.aait.mahfulprovider.util.Constants
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashActivity : BaseActivity() {
    @Inject
    lateinit var preferenceModule: PreferenceModule
    var ID = ""
    var deviceID = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setConfig(preferenceModule.language,this)
        ID = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device", deviceID)
            // Log and toast

        })

        preferenceModule.firebaseToken = deviceID
        preferenceModule.id = ID
        Handler(Looper.getMainLooper()).postDelayed({
            Log.e("token",preferenceModule.token!!)
            if (preferenceModule.token.isNullOrEmpty()) {
                openAuth()
            }else{
                Constants.TOKEN = preferenceModule.token.toString()
                openHome()
            }
        }, 3000)
    }

    override fun onStart() {
        super.onStart()

    }
    private fun openAuth(){
        val mainIntent = Intent(this, AuthActivity::class.java)
        startActivity(mainIntent)
        finish()
    }
    private fun openHome(){
        val mainIntent = Intent(this, MainActivity::class.java)
        startActivity(mainIntent)
        finish()
    }
    private  fun setConfig(language: String, context: Context) {
        val locale = Locale(language)
        Locale.setDefault(locale)
        preferenceModule.language = language
        val config = Configuration()
        config.locale = locale
        context.resources.updateConfiguration(
            config,
            context.resources.displayMetrics
        )
    }
}