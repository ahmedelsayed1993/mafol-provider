package com.aait.mahfulprovider.ui.fragment.more_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.UserResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class AboutViewModel @Inject constructor(
    private val authRepo:AuthRepository
) :ViewModel(){
    private val _aboutResponse =
        MutableStateFlow<DataState<BaseResponse<String>>>(DataState.Idle)

    val aboutResponse: MutableStateFlow<DataState<BaseResponse<String>>>
        get() = _aboutResponse

    fun About(lang:String){
        viewModelScope.launch {
            authRepo.about(lang).onEach {
                _aboutResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
    fun terms(lang:String){
        viewModelScope.launch {
            authRepo.terms(lang).onEach {
                _aboutResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}