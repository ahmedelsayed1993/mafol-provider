package com.aait.mahfulprovider.ui.fragment.main_cycle

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.data.soket.SocketIoHelper
import com.aait.mahfulprovider.business.data.soket.SocketIoManger
import com.aait.mahfulprovider.business.domain.model.auth.ConversationResponseItem
import com.aait.mahfulprovider.databinding.FragmentChatBinding
import com.aait.mahfulprovider.databinding.RecordBottomSheetBinding
import com.aait.mahfulprovider.ui.activity.MainActivity
import com.aait.mahfulprovider.ui.adapter.ChatAdapter
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.OnItemClickListener
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.audio.VoiceRecord
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.devlomi.record_view.OnBasketAnimationEnd
import dagger.hilt.android.AndroidEntryPoint
import com.devlomi.record_view.OnRecordListener
import com.devlomi.record_view.RecordPermissionHandler
import com.fxn.pix.Pix
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import kotlinx.coroutines.flow.onEach
import okhttp3.MultipartBody
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class ChatFragment :BaseFragment<FragmentChatBinding>() ,SocketIoHelper,OnItemClickListener{
    private val viewModel by viewModels<ChatViewModel>()
    private lateinit var voiceRecord: VoiceRecord
    private var recordFile: File? = null
    private var reciever_id = 0
    private var conversation_id = 0
    private var user_id = 0
    private var lastPage = 0
    private var loading = true
    var name = ""
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var chatAdapter: ChatAdapter
    private var chats = ArrayList<ConversationResponseItem>()
    private val baseAttachmentUrl = "https://mahfol.4hoste.com/assets/uploads/messages/"
    @Inject
    lateinit var socketIoManager: SocketIoManger
    override fun afterCreateView() {
        reciever_id = requireArguments().getInt("sender_id")
        conversation_id = requireArguments().getInt("conversation_id")
        lastPage = requireArguments().getInt("last_page")
        user_id = requireArguments().getInt("user_id")
        name = requireArguments().getString("name")!!
        chatAdapter = ChatAdapter(requireContext(),chats,user_id,this)

        voiceRecord = VoiceRecord(requireContext())
        navigate()
        setupPagination()
        conversationObserver()
        sendAttachMessageObserver()

        socketIoManager.connectToSocket(this)
    }
    override fun getFragmentView(): Int = R.layout.fragment_chat
    private fun navigate(){
        binding.titleLay.back.setOnClickListener {
            startActivity(Intent(requireActivity(),MainActivity::class.java))
            requireActivity().finish()
        }
        binding.titleLay.title.text = name
        conversation(conversation_id.toString(),lastPage.toString())
        layoutManager = LinearLayoutManager(requireContext(),LinearLayoutManager.VERTICAL,false)
        binding.chat.layoutManager = layoutManager
        binding.chat.adapter = chatAdapter
        binding.recordButton.setRecordView(binding.recordView)
        binding.image.setOnClickListener {
            Pix.start(
                this,
                Utils.pixOptions.setRequestCode(40)
            )
        }

        binding.send.setOnClickListener {
            sendMessage(
                user_id.toString(),
                reciever_id.toString(),
                conversation_id.toString(),
                "text",
                binding.message.text.toString(),
                preferenceModule.userData?.avatar
            )
        }
        binding.recordButton.setOnRecordClickListener {

            Toast.makeText(requireContext(), "RECORD BUTTON CLICKED", Toast.LENGTH_SHORT)
                .show()

            Log.d("RecordButton", "RECORD BUTTON CLICKED")
        }
        binding.recordView.setCancelBounds(8f)
        binding.recordView.setSmallMicColor(Color.parseColor("#c2185b"))
        binding.recordView.setLessThanSecondAllowed(false)
        binding.recordView.setSlideToCancelText("Slide To Cancel")
        binding.recordView.setCustomSounds(R.raw.record_start, R.raw.record_finished, 0)
        binding.recordView.setOnRecordListener(object : OnRecordListener {
            override fun onStart() {
                recordFile = File(requireContext().getExternalFilesDir(Environment.DIRECTORY_MUSIC)?.absolutePath + "/recording.3gp")
                try {
                    voiceRecord.startRecord(recordFile!!.path)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                Log.d("RecordView", "onStart")
            }
            override fun onCancel() {
                voiceRecord.stopRecording()
                Log.d("RecordView", "onCancel")
            }
            override fun onFinish(recordTime: Long, limitReached: Boolean) {
                voiceRecord.stopRecording()
                Log.e("path",recordFile!!.path)
                val image =
                    Utils.convertToMultiPart(recordFile!!.path, "file")
                viewModel.sendAttachmentMessage(image,"Bearer"+preferenceModule.userData?.token,preferenceModule.language)
//                sendAttachMessage(
//                    reciever_id.toString(),
//                    conversation_id.toString(),
//                    "sound",
//                    image
//                )
                val time: String = getHumanTimeText(recordTime).toString()
                Log.d("RecordView", "onFinish")
                Log.d("RecordTime", time)
            }
            override fun onLessThanSecond() {
                Log.d("RecordView", "onLessThanSecond")
            }
        })
        binding.recordView.setOnBasketAnimationEndListener(OnBasketAnimationEnd {
            Log.d("RecordView", "Basket Animation Finished")
        })
        binding.recordView.setRecordPermissionHandler(RecordPermissionHandler {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                return@RecordPermissionHandler true
            }
            val recordPermissionAvailable = ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.RECORD_AUDIO
            ) == PermissionChecker.PERMISSION_GRANTED
            if (recordPermissionAvailable) {
                return@RecordPermissionHandler true
            }
            ActivityCompat.requestPermissions(
                requireActivity(), arrayOf(Manifest.permission.RECORD_AUDIO),
                0
            )
            false
        })
    }
    private fun conversation(conversationId: String, page: String?) {
        viewModel.conversation("Bearer"+preferenceModule.userData?.token,preferenceModule.language,conversationId, page)
    }
    private fun conversationObserver() {
        lifecycleScope.launchWhenStarted { viewModel.conversationResponse.emit(DataState.Idle) }

        viewModel.conversationResponse.onEach {
            when (it) {
                is DataState.Loading -> {
                    progressUtil.showProgress()
                }
                is DataState.Success -> {
                    progressUtil.hideProgress()

                    if (it.data.value.equals("1")) {
                        chats.addAll(0,it.data.data as ArrayList<ConversationResponseItem>)
                        chatAdapter.notifyDataSetChanged()
                        binding.chat.scrollToPosition(chatAdapter.itemCount-1)
                        lastPage--
                        loading = true
                    }else{
                        Toast.makeText(
                            requireContext(),
                            it.data.msg,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
                is DataState.Error -> {
                    progressUtil.hideProgress()
                    Toast.makeText(
                        requireContext(),
                        it.exception.getErrorMessage(),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                is DataState.Idle -> {
                }
            }
        }.launchWhenStarted(viewLifecycleOwner)
    }

    private fun getHumanTimeText(milliseconds: Long): String? {
        return String.format(
            "%02d:%02d",
            TimeUnit.MILLISECONDS.toMinutes(milliseconds),
            TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds))
        )
    }
    private fun setupPagination() {
        layoutManager.stackFromEnd = true
        binding.chat.layoutManager = layoutManager
        binding.chat.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val itemno: Int = layoutManager.findFirstVisibleItemPosition()
                if (itemno == 0) {
                    if (lastPage != 0) {
                        conversation(conversation_id.toString(), lastPage.toString())
                    }
                }
            }
        })
    }

    override fun startSocketEvent() {
        val data = JSONObject().put("client", reciever_id).put("conversation", conversation_id)
        socketIoManager.setEmit("adduser", data, 3, this)

        socketIoManager.openChannelListening(requireActivity(), "message", 3, this)
    }

    override fun onChannelReceivedData(channel: String, messageJson: JSONObject) {
        try {
            val msgBody = messageJson["message"] as String
            val msgType = messageJson["type"] as String
            val receiverId = messageJson["receiver_id"] as String
            val avatar = messageJson["avatar"] as String
            Log.e("message",Gson().toJson(messageJson))
            var message: ConversationResponseItem
            message = ConversationResponseItem(
                "",
                0,
                0,
                "",
                0,
                avatar,
                ""
            )
            if (msgType == "sound" || msgType == "image") {
                message = ConversationResponseItem(
                    msgType,
                    conversation_id,
                    receiverId.toInt(),
                    getString(R.string.now),
                    null,
                    avatar,
                    baseAttachmentUrl + msgBody
                )
            } else {
                message = ConversationResponseItem(
                    msgType,
                    conversation_id,
                    receiverId.toInt(),
                    getString(R.string.now),
                    null,
                    avatar,
                    msgBody
                )
            }
            chats.add(message)
//            chatAdapter = ChatAdapter(requireContext(),chats,user_id,this)
//            binding.chat.adapter = chatAdapter
            chatAdapter.notifyItemChanged(chats.lastIndex)
            binding.chat.scrollToPosition(chatAdapter.itemCount - 1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun closeSocketEvent() {

    }



    private fun sendMessage(
        receiverId: String,
        senderId: String,
        conversationId: String,
        type: String,
        message: String?,
        avatar:String?,
        ) {
        if (message?.trim()?.isNotEmpty()!!) {

            val data =
                JSONObject()
                    .put("sender_id", senderId)
                    .put("receiver_id", receiverId)
                    .put("conversation_id", conversationId)
                    .put("message", message)
                    .put("type", type)
                    .put("avatar",avatar)
            socketIoManager.setEmit("sendMessage", data, 3, this)

            Log.e("data",Gson().toJson(data))

//
            binding.message.text.clear()
            binding.chat.scrollToPosition(chatAdapter.itemCount - 1)
        }
    }


    private fun sendAttachMessageObserver() {
        lifecycleScope.launchWhenStarted { viewModel.sendAttachmentMessageResponse.emit(DataState.Idle) }

        viewModel.sendAttachmentMessageResponse.onEach {
            when (it) {
                is DataState.Loading -> {
                    progressUtil.showProgress()
                }
                is DataState.Success -> {
                    progressUtil.hideProgress()
                    if (it.data.value.equals("1")) {
                        var type = ""
                        if (it.data.data?.file_name?.endsWith(".jpg")!!||it.data.data?.file_name?.endsWith(".png")!!) {
                            type = "image"
                        }else {
                            type = "sound"
                        }
                        sendMessage(
                            user_id.toString(),
                            reciever_id.toString(),
                            conversation_id.toString(),
                            type,
                            it.data.data?.file_name.toString(),
                            preferenceModule.userData?.avatar
                        )
                    }else{
                        Toast.makeText(
                            requireContext(),
                            it.data.msg,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
                is DataState.Error -> {
                    progressUtil.hideProgress()
                    Toast.makeText(
                        requireContext(),
                        it.exception.getErrorMessage(),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                is DataState.Idle -> {
                }
            }
        }.launchWhenStarted(viewLifecycleOwner)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val returnValue = data?.getStringArrayListExtra(Pix.IMAGE_RESULTS)
            when (requestCode) {
                40 -> {
                    val image =
                        Utils.convertToMultiPart(returnValue!![0], "file")
                    viewModel.sendAttachmentMessage(image,"Bearer"+preferenceModule.userData?.token,preferenceModule.language)
                }
            }
        }
    }

    override fun onItemClick(view: View, position: Int) {
        if (chats.get(position).messageType.equals("sound")){
            val mediaPlayer = MediaPlayer.create(requireContext(), Uri.parse(chats.get(position).message))
            mediaPlayerSheet(mediaPlayer, chats.get(position).message.toString())
        }else if (chats.get(position).messageType.equals("image")){
            findNavController().navigate(ChatFragmentDirections.actionChatFragmentToImagesFragment(chats.get(position).message!!))
        }
    }

    private fun mediaPlayerSheet(mediaPlayer: MediaPlayer, message: String) {
        val bottomSheetDialog =
            BottomSheetDialog(requireContext(), R.style.TransparentDialog)
        val binding = RecordBottomSheetBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(binding.root)
        bottomSheetDialog.setCanceledOnTouchOutside(true)
        binding.recordProgress.visibility = View.VISIBLE
        binding.btnSendRecordVoice.text = getString(R.string.play)
        binding.btnCancelRecordVoice.text = getString(R.string.pause)
        binding.btnSendRecordVoice.setOnClickListener {
            mediaPlayer.start()
        }
        binding.btnCancelRecordVoice.setOnClickListener {
            mediaPlayer.pause()
            bottomSheetDialog.dismiss()
        }
        bottomSheetDialog.setOnShowListener {
            mediaPlayer.start()
        }
        bottomSheetDialog.setOnDismissListener {
            stopMediaPlayer(mediaPlayer)
        }
        bottomSheetDialog.show()
    }

    private fun stopMediaPlayer(mediaPlayer: MediaPlayer) {
        try {
            if (mediaPlayer.isPlaying)
                mediaPlayer.stop()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        socketIoManager.setEmit("exitchat", null, 3, this)
        socketIoManager.disconnectSocket(this)
    }
}