package com.aait.mahfulprovider.ui.fragment.auth_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.IntroResponseItem
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class ScreensViewModel @Inject constructor(
    private val authRepo: AuthRepository
) : ViewModel(){
    private val _aboutResponse =
        MutableStateFlow<DataState<BaseResponse<List<IntroResponseItem>>>>(DataState.Idle)

    val aboutResponse: MutableStateFlow<DataState<BaseResponse<List<IntroResponseItem>>>>
        get() = _aboutResponse

    fun about(){
        viewModelScope.launch {
            authRepo.screens().onEach {
                _aboutResponse.value = it
            }.launchIn(viewModelScope)
        }
    }



}