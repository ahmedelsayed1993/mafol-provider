package com.aait.mahfulprovider.ui.fragment.auth_cycle

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import com.aait.mahfulprovider.databinding.FragmentLoginBinding
import com.aait.mahfulprovider.ui.activity.MainActivity
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.Constants
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    private val viewModel by viewModels<LoginViewModel>()

    var deviceID = ""
    var ID = ""
    override fun afterCreateView() {
        ID = Settings.Secure.getString(activity?.contentResolver, Settings.Secure.ANDROID_ID)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device", deviceID)
            // Log and toast

        })
        Log.e("ID", ID)
        navigate()


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginObserver()
    }


    override fun getFragmentView(): Int = R.layout.fragment_login

    private fun navigate() {
        binding.register.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_introOneFragment)
        }
        binding.forgotPass.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_forgotPasswordFragment)
        }
        binding.login.setOnClickListener {
            if (Utils.checkEditError(binding.phone, getString(R.string.phone_number)) ||
                Utils.checkEditError(binding.password, getString(R.string.password))
            ) {
            } else {
                login()
            }
        }


    }

    fun login() {
        viewModel.login(
            binding.phone.text.toString(),
            binding.password.text.toString(),
            deviceID,
            ID,
            preferenceModule.language
        )
    }

    private fun loginObserver() {
        lifecycleScope.launchWhenStarted { viewModel.loginResponse.emit(DataState.Idle) }

        viewModel.loginResponse.onEach {

            when (it) {
                is DataState.Loading -> {
                    progressUtil.showProgress()
                }
                is DataState.Success -> {
                    progressUtil.hideProgress()

                    if (it.data.value == "1") {
                        if (it.data.data?.user_type.equals("provider")) {
                            preferenceModule.token = it.data.data?.token
                            Constants.TOKEN = it.data.data?.token!!
                            //preferenceModule.userType = it.data.data?.userType
                            preferenceModule.userData = it.data.data
                            startActivity(Intent(requireContext(), MainActivity::class.java))
                            activity?.finish()
                        }else{
                            Toast.makeText(requireContext(),getString(R.string.you_not_provider), Toast.LENGTH_SHORT).show()
                        }

                    }else if (it.data.value == "2") {
                        Constants.TOKEN = it.data.data?.token!!
                        findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToActivateCodeFragment(it.data.data?.token!!))
                    }
                    else {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
                    }
                }
                is DataState.Error -> {
                    progressUtil.hideProgress()
                    Toast.makeText(
                        requireContext(),
                        it.exception.getErrorMessage(),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                is DataState.Idle -> {

                }
            }
        }.launchWhenStarted(viewLifecycleOwner)
    }
}