package com.aait.mahfulprovider.ui.fragment.more_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.ContactUsResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class ComplainViewModel @Inject constructor(
    private val authRepo:AuthRepository
) :ViewModel(){
    private val _complaintResponse =
        MutableStateFlow<DataState<BaseResponse<String>>>(DataState.Idle)

    val complaintResponse: MutableStateFlow<DataState<BaseResponse<String>>>
        get() = _complaintResponse

    fun complaint(phone:String,name:String,subject:String,message:String,lang:String){
        viewModelScope.launch {
            authRepo.complaint(phone,name,subject,message,lang).onEach {
                _complaintResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}