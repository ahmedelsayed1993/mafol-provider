package com.aait.mahfulprovider.ui.fragment.more_cycle

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentComplaintsBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.common.getErrorMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class ComplainFragment :BaseFragment<FragmentComplaintsBinding>() {
    private val viewModel by viewModels<ComplainViewModel>()
    override fun afterCreateView() {}

    override fun getFragmentView(): Int = R.layout.fragment_complaints

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        aboutObserver()
    }
    private fun navigate(){
        binding.titleLay.back.setOnClickListener {
            onBack()
        }
        binding.titleLay.title.text = getString(R.string.complaint_suggestions)
        binding.send.setOnClickListener {
            if (Utils.checkEditError(binding.name,getString(R.string.name))||
                Utils.checkEditError(binding.phone,getString(R.string.phone_number))||
                Utils.checkLength(binding.phone,getString(R.string.phone_length),9)||
                    Utils.checkEditError(binding.topic,getString(R.string.complain_title))||
                    Utils.checkEditError(binding.text,getString(R.string.complain_text))){

            }else{
                viewModel.complaint(binding.phone.text.toString(),
                binding.name.text.toString(),
                binding.topic.text.toString(),
                binding.text.text.toString(),
                preferenceModule.language)
            }
        }
    }
    private fun aboutObserver(){
        lifecycleScope.launchWhenStarted { viewModel.complaintResponse.emit(DataState.Idle)
            viewModel.complaintResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {

                            Toast.makeText(requireContext(), it.data.data, Toast.LENGTH_SHORT)
                                .show()
                            onBack()

                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }
}