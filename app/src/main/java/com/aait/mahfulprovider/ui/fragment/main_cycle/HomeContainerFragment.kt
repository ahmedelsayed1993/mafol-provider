package com.aait.mahfulprovider.ui.fragment.main_cycle

import androidx.fragment.app.Fragment
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.databinding.FragmentHomeContainerBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.ui.fragment.home_page_cycle.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeContainerFragment :BaseFragment<FragmentHomeContainerBinding>() {
    override fun afterCreateView() {
        binding.home.setImageResource(R.mipmap.home_active)
        binding.chat.setImageResource(R.mipmap.chat)
        binding.orders.setImageResource(R.mipmap.order)
        binding.notification.setImageResource(R.mipmap.notification)
        binding.more.setImageResource(R.mipmap.more)
        setCurrentFragment(HomeFragment())
        navigate()
    }

    override fun getFragmentView(): Int = R.layout.fragment_home_container

    private fun navigate(){
        binding.home.setOnClickListener {
            binding.home.setImageResource(R.mipmap.home_active)
            binding.chat.setImageResource(R.mipmap.chat)
            binding.orders.setImageResource(R.mipmap.order)
            binding.notification.setImageResource(R.mipmap.notification)
            binding.more.setImageResource(R.mipmap.more)
            setCurrentFragment(HomeFragment())
        }
        binding.orders.setOnClickListener {
            binding.home.setImageResource(R.mipmap.home)
            binding.chat.setImageResource(R.mipmap.chat)
            binding.orders.setImageResource(R.mipmap.order_active)
            binding.notification.setImageResource(R.mipmap.notification)
            binding.more.setImageResource(R.mipmap.more)
            setCurrentFragment(OrdersFragment())
        }
        binding.chat.setOnClickListener {
            binding.home.setImageResource(R.mipmap.home)
            binding.chat.setImageResource(R.mipmap.chat_active)
            binding.orders.setImageResource(R.mipmap.order)
            binding.notification.setImageResource(R.mipmap.notification)
            binding.more.setImageResource(R.mipmap.more)
            setCurrentFragment(ChatsFragment())
        }
        binding.notification.setOnClickListener {
            binding.home.setImageResource(R.mipmap.home)
            binding.chat.setImageResource(R.mipmap.chat)
            binding.orders.setImageResource(R.mipmap.order)
            binding.notification.setImageResource(R.mipmap.notification_active)
            binding.more.setImageResource(R.mipmap.more)
            setCurrentFragment(NotificationsFragment())
        }
        binding.more.setOnClickListener {
            binding.home.setImageResource(R.mipmap.home)
            binding.chat.setImageResource(R.mipmap.chat)
            binding.orders.setImageResource(R.mipmap.order)
            binding.notification.setImageResource(R.mipmap.notification)
            binding.more.setImageResource(R.mipmap.more_active)
            setCurrentFragment(MoreFragment())
        }

    }

    private fun setCurrentFragment(fragment: Fragment) =
        childFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            .replace(binding.homeContainer.id, fragment)
            .commit()
}