package com.aait.mahfulprovider.ui.fragment.home_page_cycle

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentMoreBinding
import com.aait.mahfulprovider.ui.activity.MainActivity
import com.aait.mahfulprovider.ui.activity.SplashActivity
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.ui.fragment.main_cycle.HomeContainerFragmentDirections
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.common.getErrorMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class MoreFragment  @Inject constructor():BaseFragment<FragmentMoreBinding>() {
    private val viewModel by viewModels<MoreViewModel>()
    override fun afterCreateView() {
        binding.titleLay.title.text = getString(R.string.more)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        logoutObserver()
    }

    override fun getFragmentView(): Int = R.layout.fragment_more

    private fun navigate(){

        binding.profile.setOnClickListener {
            findNavController().navigate(HomeContainerFragmentDirections.actionHomeContainerFragmentToProfileFragment())
        }
        binding.lang.setOnClickListener {
            findNavController().navigate(HomeContainerFragmentDirections.actionHomeContainerFragmentToLanguageFragment())
        }
        binding.wallet.setOnClickListener {
            findNavController().navigate(HomeContainerFragmentDirections.actionHomeContainerFragmentToWalletFragment())
        }

        binding.aboutApp.setOnClickListener {
            findNavController().navigate(HomeContainerFragmentDirections.actionHomeContainerFragmentToAboutAppFragment())
        }
        binding.complains.setOnClickListener {
            findNavController().navigate(HomeContainerFragmentDirections.actionHomeContainerFragmentToComplainFragment())
        }
        binding.contactUs.setOnClickListener {
            findNavController().navigate(HomeContainerFragmentDirections.actionHomeContainerFragmentToContactUsFragment())
        }
        binding.logout.setOnClickListener {
           viewModel.logOut(preferenceModule.userData?.device_id!!,preferenceModule.userData?.device_type!!,
           preferenceModule.userData?.mac_address!!,"Bearer"+preferenceModule.userData?.token,preferenceModule.language)
        }
    }

    private fun logoutObserver(){
        lifecycleScope.launchWhenStarted { viewModel.logoutResponse.emit(DataState.Idle)
            viewModel.logoutResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            preferenceModule.token = null
                            Toast.makeText(requireContext(), it.data.data, Toast.LENGTH_SHORT)
                                .show()
                            activity?.finishAffinity()
                            MainActivity().stopLocationToSocket(requireActivity())
                            startActivity(Intent(activity,SplashActivity::class.java))


                        } else {
                            preferenceModule.token = null
                            activity?.finishAffinity()
                            startActivity(Intent(activity,SplashActivity::class.java))

                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }
}