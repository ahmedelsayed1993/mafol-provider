package com.aait.mahfulprovider.ui.fragment.home_page_cycle

import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.databinding.FragmentMyordersBinding
import com.aait.mahfulprovider.ui.adapter.OrderTapAdapter
//import com.aait.mahfulprovider.ui.adapter.user.OrderTapAdapter
import com.aait.mahfulprovider.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class OrdersFragment  @Inject constructor() :BaseFragment<FragmentMyordersBinding> (){
    private var mAdapter: OrderTapAdapter? = null
    override fun afterCreateView() {
        binding.titleLay.title.text = getString(R.string.orders)
        mAdapter = OrderTapAdapter(requireContext(),childFragmentManager)

        binding.ordersViewPager.adapter= mAdapter
        binding.orders.setupWithViewPager(binding.ordersViewPager)

    }

    override fun getFragmentView(): Int = R.layout.fragment_myorders
}