package com.aait.mahfulprovider.ui.fragment.home_page_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.HomeResponse
import com.aait.mahfulprovider.business.domain.model.auth.MyConversationsResponseItem
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class ChatsViewModel @Inject constructor(
    private val authRepo: AuthRepository
) : ViewModel(){
    private val _chatsResponse =
        MutableStateFlow<DataState<BaseResponse<List<MyConversationsResponseItem>>>>(DataState.Idle)

    val chatsResponse: MutableStateFlow<DataState<BaseResponse<List<MyConversationsResponseItem>>>>
        get() = _chatsResponse

    fun myChats(auth:String,lang:String){
        viewModelScope.launch {
            authRepo.myConversations(auth,lang).onEach {
                _chatsResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}