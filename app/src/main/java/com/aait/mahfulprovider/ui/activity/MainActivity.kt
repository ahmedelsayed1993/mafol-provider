package com.aait.mahfulprovider.ui.activity

import android.app.Activity
import android.os.Bundle
import android.util.Log
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.soket.SocketIoHelper
import com.aait.mahfulprovider.business.data.soket.SocketIoManger
import com.aait.mahfulprovider.ui.base.BaseActivity
import com.aait.mahfulprovider.util.Constants.TAG
import dagger.hilt.android.AndroidEntryPoint
import io.nlopez.smartlocation.SmartLocation
import org.json.JSONObject

@AndroidEntryPoint
class MainActivity :BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun sendLocationToSocket(
        activity: Activity,
        id: Int,
        socketIoManager: SocketIoManger,
        socketIoHelper: SocketIoHelper
    ) {
        Log.e(TAG, "sendLocationToSocket:")
        SmartLocation.with(activity.applicationContext).location()
            .start {
                Log.e(TAG, "sendLocationToSocket: $it")
                val data = JSONObject()
                    .put("user_id", id)
                    .put("lat", it.latitude)
                    .put("lng", it.longitude)
                socketIoManager.setEmit("updateLocation", data, 3, socketIoHelper)
            }
    }

    fun stopLocationToSocket(activity: Activity) {
        SmartLocation.with(activity.applicationContext).location().stop()
    }
}