package com.aait.mahfulprovider.ui.fragment.order_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.ConversationIdResponse
import com.aait.mahfulprovider.business.domain.model.auth.OrderDetailsResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class OrderDetailsViewHolder @Inject constructor(
    private val authRepo:AuthRepository
) :ViewModel(){

    private val _orderDetailsResponse =
        MutableStateFlow<DataState<BaseResponse<OrderDetailsResponse>>>(DataState.Idle)

    val orderDetailsResponse: MutableStateFlow<DataState<BaseResponse<OrderDetailsResponse>>>
        get() = _orderDetailsResponse

    private val _conversationIdResponse =
        MutableStateFlow<DataState<BaseResponse<ConversationIdResponse>>>(DataState.Idle)

    val conversationIdResponse: MutableStateFlow<DataState<BaseResponse<ConversationIdResponse>>>
        get() = _conversationIdResponse

    fun orderDetails(order_id:Int,action:String?,reason:String?,auth : String ,lang:String){
        viewModelScope.launch {
            authRepo.orderDetails(order_id,action,reason,auth,lang).onEach {
                _orderDetailsResponse.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun conversationId(receiver_id:Int,auth:String,lang:String){
        viewModelScope.launch {
            authRepo.newConversation(receiver_id,auth,lang).onEach {
                _conversationIdResponse.value = it
            }.launchIn(viewModelScope)
        }
    }

    private val _deleteOrderResponse =
        MutableStateFlow<DataState<BaseResponse<OrderDetailsResponse>>>(DataState.Idle)

    val deleteOrderResponse: MutableStateFlow<DataState<BaseResponse<OrderDetailsResponse>>>
        get() = _deleteOrderResponse

    fun deleteOrder(order_id:Int,action:String?,reason:String?,auth : String ,lang:String){
        viewModelScope.launch {
            authRepo.orderDetails(order_id,action,reason,auth,lang).onEach {
                _deleteOrderResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
    private val _acceptOrderResponse =
        MutableStateFlow<DataState<BaseResponse<OrderDetailsResponse>>>(DataState.Idle)

    val acceptOrderResponse: MutableStateFlow<DataState<BaseResponse<OrderDetailsResponse>>>
        get() = _acceptOrderResponse

    fun acceptOrder(order_id:Int,action:String?,auth : String ,lang:String){
        viewModelScope.launch {
            authRepo.orderDetails(order_id,action,null,auth,lang).onEach {
                _acceptOrderResponse.value = it
            }.launchIn(viewModelScope)
        }
    }

}