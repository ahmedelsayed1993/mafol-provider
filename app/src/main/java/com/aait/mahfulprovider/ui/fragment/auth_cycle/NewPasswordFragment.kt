package com.aait.mahfulprovider.ui.fragment.auth_cycle

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentNewPasswordBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.common.getErrorMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class NewPasswordFragment :BaseFragment<FragmentNewPasswordBinding>() {

    private val viewModel by viewModels<NewPasswordViewModel>()
    private var user = ""
    override fun afterCreateView() {
        user = requireArguments().getString("user")!!
        Log.e("user",user)

    }


    override fun getFragmentView(): Int = R.layout.fragment_new_password
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        loginObserver()
    }
    private fun navigate(){
        binding.confirm.setOnClickListener {
            if (Utils.checkEditError(binding.code,getString(R.string.verification_code))||
                    Utils.checkEditError(binding.password,getString(R.string.new_password))||
                    Utils.checkLength(binding.password,getString(R.string.password_length),6)||
                    Utils.checkEditError(binding.confirmPassword,getString(R.string.confirm_new_password))){
            }else{
                if (binding.password.text.toString().equals(binding.confirmPassword.text.toString())){
                    viewModel.forgot(binding.password.text.toString(),binding.code.text.toString(),"Bearer"+user
                    ,preferenceModule.language)
                }else{
                    binding.confirmPassword.error = getString(R.string.password_not_match)
                }
            }
        }
    }

    private fun loginObserver() {
        lifecycleScope.launchWhenStarted { viewModel.forgotResponse.emit(DataState.Idle) }

        viewModel.forgotResponse.onEach {

            when (it) {
                is DataState.Loading -> {
                    progressUtil.showProgress()
                }
                is DataState.Success -> {
                    progressUtil.hideProgress()

                    if (it.data.value == "1") {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
//                        preferenceModule.token = it.data.data?.token
//                        //preferenceModule.userType = it.data.data?.userType
//                        preferenceModule.userData = it.data.data
                        findNavController().navigate(R.id.action_newPasswordFragment_to_loginFragment)


                    } else {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
                    }
                }
                is DataState.Error -> {
                    progressUtil.hideProgress()
                    Toast.makeText(
                        requireContext(),
                        it.exception.getErrorMessage(),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                is DataState.Idle -> {

                }
            }
        }.launchWhenStarted(viewLifecycleOwner)
    }
}