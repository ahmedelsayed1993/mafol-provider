package com.aait.mahfulprovider.ui.fragment.more_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class WalletViewModel @Inject constructor(
    private val authRepo:AuthRepository
):ViewModel() {
    private val _walletResponse =
    MutableStateFlow<DataState<BaseResponse<Int>>>(DataState.Idle)

    val walletResponse: MutableStateFlow<DataState<BaseResponse<Int>>>
        get() = _walletResponse

    private val _pullResponse =
        MutableStateFlow<DataState<BaseResponse<String>>>(DataState.Idle)

    val pullResponse: MutableStateFlow<DataState<BaseResponse<String>>>
        get() = _pullResponse


    fun wallet(auth : String ,lang:String){
        viewModelScope.launch {
            authRepo.wallet(auth,lang).onEach {
                _walletResponse.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun pullBalance(bank_name:String,
                    account_name:String,
                    account_number:String,
                    iban_number:String){
        viewModelScope.launch {
            authRepo.balanceRequest(bank_name, account_name, account_number, iban_number).onEach {
                _pullResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}