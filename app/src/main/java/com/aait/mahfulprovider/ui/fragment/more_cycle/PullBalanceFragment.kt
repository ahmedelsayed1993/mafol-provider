package com.aait.mahfulprovider.ui.fragment.more_cycle

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentPullBalanceBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.common.getErrorMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class PullBalanceFragment :BaseFragment<FragmentPullBalanceBinding>(){
    private val viewModel by viewModels<WalletViewModel>()
    override fun afterCreateView() {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        pullObserver()
    }

    override fun getFragmentView(): Int = R.layout.fragment_pull_balance

    private fun navigate(){
        binding.titleLay.title.text = getString(R.string.Balance_withdrawal)
        binding.titleLay.back.setOnClickListener { onBack() }
        binding.send.setOnClickListener {
            if (Utils.checkEditError(binding.bankName,getString(R.string.bank_name))||
                    Utils.checkEditError(binding.accountOwnerNumber,getString(R.string.account_owner_number))||
                    Utils.checkEditError(binding.accountNumber,getString(R.string.account_number))||
                    Utils.checkEditError(binding.ibanNumber,getString(R.string.iban_number))){

            }else{
                viewModel.pullBalance(binding.bankName.text.toString(),binding.accountOwnerNumber.text.toString()
                    ,binding.accountNumber.text.toString(),binding.ibanNumber.text.toString())
            }
        }
    }

    private fun pullObserver(){
        lifecycleScope.launchWhenStarted { viewModel.pullResponse.emit(DataState.Idle)
            viewModel.pullResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {

                            Toast.makeText(requireContext(), it.data.data, Toast.LENGTH_SHORT)
                                .show()
                            onBack()
                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }
}