package com.aait.mahfulprovider.ui.fragment.auth_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.UserResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class LocationViewModel @Inject constructor(
    private val authRepo:AuthRepository
) : ViewModel(){
    private val _locationResponse =
        MutableStateFlow<DataState<BaseResponse<String>>>(DataState.Idle)

    val locationResponse: MutableStateFlow<DataState<BaseResponse<String>>>
        get() = _locationResponse


    fun location(
        address: String,
        lat: String,
        lng:String,
        auth:String,
        lang:String
    ) {
        viewModelScope.launch {
            authRepo.updateLocation(
                address,lat,lng,auth,lang
            ).onEach {
                _locationResponse.value = it
            }.launchIn(viewModelScope)
        }
    }

}