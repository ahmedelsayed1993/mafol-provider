package com.aait.mahfulprovider.ui.fragment.auth_cycle

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.auth.UserResponse
import com.aait.mahfulprovider.databinding.FragmentActiviateAccountBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.Constants
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.Utils
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class ActivateCodeFragment :BaseFragment<FragmentActiviateAccountBinding>() {
    private val viewModel by viewModels<ActivationCodeViewModel>()
    private var user = ""
    override fun afterCreateView() {
        user = requireArguments().getString("user")!!
        Log.e("user",user)

    }

    override fun getFragmentView(): Int = R.layout.fragment_activiate_account
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        loginObserver()
    }

    private fun navigate(){
        binding.confirm.setOnClickListener {
            if (Utils.checkEditError(binding.code,getString(R.string.verification_code))){

            }else{
                activate()
            }
        }
        binding.resend.setOnClickListener {
            resend()
        }
    }
    private fun activate() {
        Log.e("token",Constants.BEARER+user)
        viewModel.activate(
            binding.code.text.toString(),
            "Bearer"+user,
            preferenceModule.language
        )
    }
    private fun resend(){
        viewModel.resend("Bearer"+user,
        preferenceModule.language)
    }

    private fun loginObserver() {
        lifecycleScope.launchWhenStarted { viewModel.activateResponse.emit(DataState.Idle) }

        viewModel.activateResponse.onEach {

            when (it) {
                is DataState.Loading -> {
                    progressUtil.showProgress()
                }
                is DataState.Success -> {
                    progressUtil.hideProgress()

                    if (it.data.value == "1") {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
                        preferenceModule.token = it.data.data?.token
                        Constants.TOKEN = it.data.data?.token!!
                        //preferenceModule.userType = it.data.data?.userType
                        preferenceModule.userData = it.data.data
                        findNavController().navigate(ActivateCodeFragmentDirections.actionActivateCodeFragmentToLocationFragment2(it.data.data?.token!!,"auth"))


                    } else {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
                    }
                }
                is DataState.Error -> {
                    progressUtil.hideProgress()
                    Toast.makeText(
                        requireContext(),
                        it.exception.getErrorMessage(),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                is DataState.Idle -> {

                }
            }
        }.launchWhenStarted(viewLifecycleOwner)

        lifecycleScope.launchWhenStarted { viewModel.resendResponse.emit(DataState.Idle) }

        viewModel.resendResponse.onEach {

            when (it) {
                is DataState.Loading -> {
                    progressUtil.showProgress()
                }
                is DataState.Success -> {
                    progressUtil.hideProgress()

                    if (it.data.value == "1") {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
//                        preferenceModule.token = it.data.data?.token
//                        //preferenceModule.userType = it.data.data?.userType
//                        preferenceModule.userData = it.data.data


                    } else {
                        Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT).show()
                    }
                }
                is DataState.Error -> {
                    progressUtil.hideProgress()
                    Toast.makeText(
                        requireContext(),
                        it.exception.getErrorMessage(),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                is DataState.Idle -> {

                }
            }
        }.launchWhenStarted(viewLifecycleOwner)
    }
}