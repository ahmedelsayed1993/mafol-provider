package com.aait.mahfulprovider.ui.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.ui.fragment.order_cycle.CurrentFragment
import com.aait.mahfulprovider.ui.fragment.order_cycle.FinishedFragment
import com.aait.mahfulprovider.ui.fragment.order_cycle.PendingFragment
import com.aait.mahfulprovider.ui.fragment.order_cycle.PendingPaymentFragment


class OrderTapAdapter (
        private val context: Context,
        fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            PendingFragment()
        } else if (position == 1){
            PendingPaymentFragment()
        }else if (position == 2){
            CurrentFragment()
        }
        else {
            FinishedFragment()
        }

    }

    override fun getCount(): Int {
        return 4
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.pending_payment)
        }else if (position == 1){
            context.getString(R.string.payed)
        }else if (position == 2){
            context.getString(R.string.UnderWay)
        }
        else {
            context.getString(R.string.finished)
        }

    }
}
