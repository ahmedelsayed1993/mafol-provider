package com.aait.mahfulprovider.ui.base

import android.content.Context
import android.webkit.WebView
import com.aait.mahfulprovider.util.ThemeHelper
import com.akexorcist.localizationactivity.ui.LocalizationApplication
import dagger.hilt.android.HiltAndroidApp
import java.util.*


@HiltAndroidApp
class BaseApplication : LocalizationApplication() {

    override fun getDefaultLanguage(base: Context): Locale {
        return Locale("ar")
    }

    override fun onCreate() {
        super.onCreate()
        ThemeHelper.applyTheme("light")
        WebView(this).destroy()
    }
}