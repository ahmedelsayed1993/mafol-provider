package com.aait.mahfulprovider.ui.activity

import android.os.Bundle
import android.os.PersistableBundle
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AuthActivity:BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
    }
}