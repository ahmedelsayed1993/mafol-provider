package com.aait.mahfulprovider.ui.fragment.auth_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.UserResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NewPasswordViewModel @Inject constructor(
    private val authRepo:AuthRepository
) : ViewModel(){
    private val _forgotResponse =
        MutableStateFlow<DataState<BaseResponse<UserResponse>>>(DataState.Idle)

    val forgotResponse: MutableStateFlow<DataState<BaseResponse<UserResponse>>>
        get() = _forgotResponse


    fun forgot(phone:String,code:String,auth:String,lang:String){
        viewModelScope.launch {
            authRepo.updatePass(phone,code,auth,lang)
                .onEach {
                    _forgotResponse.value = it
                }.launchIn(viewModelScope)
        }
    }
}