package com.aait.mahfulprovider.ui.fragment.order_cycle

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.databinding.FragmentOrderDetailsBinding
import com.aait.mahfulprovider.ui.adapter.ImagesAdapter
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.common.getErrorMessage
import com.bumptech.glide.Glide
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class OrderDetailsFragment : BaseFragment<FragmentOrderDetailsBinding>() {
    private val viewModel by viewModels<OrderDetailsViewHolder>()
    private var order_id = 0
    private lateinit var imagesAdapter: ImagesAdapter

    private var receiver = 0
    private var lat = ""
    private var lng = ""
    private var type = ""
    private var price = ""
    var name = ""
    override fun afterCreateView() {
        order_id = requireArguments().getInt("order_id")
        imagesAdapter = ImagesAdapter(requireContext(),ArrayList<String>(),R.layout.recycle_order_images)
    }

    override fun getFragmentView(): Int = R.layout.fragment_order_details
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        orderDetailsObserver()
        acceptOrderObserver()
        deleteOrderObserver()
        conversationIdObserver()
    }


    private fun navigate(){
        binding.titleLay.back.setOnClickListener {
            onBack()
        }
        binding.titleLay.title.text = getString(R.string.order_details)
        binding.location.setOnClickListener {
            startActivity(
                Intent(Intent.ACTION_VIEW,Uri.parse("http://maps.google.com/maps?"+"&daddr="+lat+","+lng)))
        }
        binding.images.layoutManager  = LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)
        binding.images.adapter = imagesAdapter
        viewModel.orderDetails(order_id,null,null,"Bearer"+preferenceModule.userData?.token,preferenceModule.language)
        //pending  قيد الموافقة
        //pending payment قيد الدفع
        //paid
        //processing
        //completed
        binding.accept.setOnClickListener {
            viewModel.acceptOrder(order_id,"accepted","Bearer"+preferenceModule.userData?.token,preferenceModule.language)

        }
        binding.refuse.setOnClickListener {
            findNavController().navigate(OrderDetailsFragmentDirections.actionOrderDetailsFragmentToCancelOrderFragment(order_id))
        }
        binding.cancel.setOnClickListener {
            findNavController().navigate(OrderDetailsFragmentDirections.actionOrderDetailsFragmentToRefuseOrderFragment(order_id))
        }
        binding.chat.setOnClickListener {
            Log.e("rrrrr","yyyyyy")
            viewModel.conversationId(receiver,"Bearer"+preferenceModule.userData?.token,preferenceModule.language)
        }
        binding.moveTo.setOnClickListener {
            viewModel.acceptOrder(order_id,"processing","Bearer"+preferenceModule.userData?.token,preferenceModule.language)

        }
        binding.process.setOnClickListener {
            viewModel.acceptOrder(order_id,"completed","Bearer"+preferenceModule.userData?.token,preferenceModule.language)

        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun orderDetailsObserver(){
        lifecycleScope.launchWhenStarted {
            viewModel.orderDetailsResponse.onEach {
                when(it){
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            Log.e("response", Gson().toJson(it.data.data))
                            binding.location.text = it.data.data?.address

                            binding.name.text = it.data.data?.name
                            binding.type.text = it.data.data?.category
                            binding.details.text = it.data.data?.notes
                            binding.num.text = it.data.data?.id.toString()
                            binding.price.text = it.data.data?.price+getString(R.string.rs)
                            binding.date.text = it.data.data?.created
                            imagesAdapter.updateAll(it.data.data?.images!! as ArrayList<String>)
                            binding.distance.text = getString(R.string.distance_between)+it.data.data.distance+"KM"
                            lat = it.data.data.lat!!
                            lng = it.data.data.lng!!
                            type = it.data.data.category!!
                            price = it.data.data.price!!
                            name = it.data.data.name!!
                            receiver = it.data.data.receiver_id!!
                            binding.reason.text = it.data.data?.reason
                            if (it.data.data?.reason.equals("")){
                                binding.reasonLay.visibility = View.GONE
                            }else{
                                binding.reasonLay.visibility = View.VISIBLE
                            }
                            if (it.data.data.rate==0){
                                binding.rate.visibility = View.GONE
                            }else{
                                binding.rate.visibility = View.VISIBLE
                            }
                            binding.rate.rating = it.data.data.rate!!.toFloat()
                            if (it.data.data.status.equals("pending")){
                                binding.refuse.visibility = View.VISIBLE
                                binding.accept.visibility = View.VISIBLE
                                binding.cancel.visibility = View.GONE
                                binding.chat.visibility = View.GONE
                                binding.moveTo.visibility = View.GONE
                                binding.process.visibility = View.GONE

                                binding.status.setTextColor(requireContext().getColor(R.color.red))
                                binding.status.text = getString(R.string.New)
                            }else if (it.data.data.status.equals("payment_pending")){
                                binding.refuse.visibility = View.GONE
                                binding.accept.visibility = View.GONE
                                binding.cancel.visibility = View.VISIBLE
                                binding.chat.visibility = View.GONE
                                binding.moveTo.visibility = View.GONE
                                binding.process.visibility = View.GONE
                                binding.rate.visibility = View.GONE
                                binding.status.setTextColor(requireContext().getColor(R.color.colorPrimary))
                                binding.status.text = getString(R.string.pending_payment)
                            }else if (it.data.data.status.equals("paid")){
                                binding.refuse.visibility = View.GONE
                                binding.accept.visibility = View.GONE
                                binding.cancel.visibility = View.GONE
                                binding.chat.visibility = View.VISIBLE
                                binding.moveTo.visibility = View.VISIBLE
                                binding.process.visibility = View.GONE

                                binding.status.setTextColor(requireContext().getColor(R.color.green_4FD477))
                                binding.status.text = getString(R.string.payed)
                            }
                            else if (it.data.data.status.equals("accepted")){
                                if (it.data.data.payment.equals("")) {
                                    binding.refuse.visibility = View.GONE
                                    binding.accept.visibility = View.GONE
                                    binding.cancel.visibility = View.VISIBLE
                                    binding.chat.visibility = View.VISIBLE
                                    binding.moveTo.visibility = View.GONE
                                    binding.process.visibility = View.GONE

                                    binding.status.setTextColor(requireContext().getColor(R.color.colorPrimary))
                                    binding.status.text = getString(R.string.pending_payment)
                                }else{
                                    binding.refuse.visibility = View.GONE
                                    binding.accept.visibility = View.GONE
                                    binding.cancel.visibility = View.GONE
                                    binding.chat.visibility = View.VISIBLE
                                    binding.moveTo.visibility = View.VISIBLE
                                    binding.process.visibility = View.GONE

                                    binding.status.setTextColor(requireContext().getColor(R.color.green_4FD477))
                                    binding.status.text = getString(R.string.payed)
                                }
                            }else if (it.data.data.status.equals("processing")){
                                binding.refuse.visibility = View.GONE
                                binding.accept.visibility = View.GONE
                                binding.cancel.visibility = View.GONE
                                binding.chat.visibility = View.VISIBLE
                                binding.moveTo.visibility = View.GONE
                                binding.process.visibility = View.VISIBLE

                                binding.status.setTextColor(requireContext().getColor(R.color.green_4FD477))
                                binding.status.text = getString(R.string.UnderWay)
                            }else if (it.data.data.status.equals("completed")){
                                binding.refuse.visibility = View.GONE
                                binding.accept.visibility = View.GONE
                                binding.cancel.visibility = View.GONE
                                binding.chat.visibility = View.GONE
                                binding.moveTo.visibility = View.GONE
                                binding.process.visibility = View.GONE
                                binding.status.setTextColor(requireContext().getColor(R.color.colorPrimary))
                                binding.status.text = getString(R.string.finished)
                            }
                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                         progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)
        }
    }

    private fun deleteOrderObserver(){
        lifecycleScope.launchWhenStarted {
            viewModel.deleteOrderResponse.onEach {
                when(it){
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {

                            onBack()
                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)
        }
    }

    private fun acceptOrderObserver(){
        lifecycleScope.launchWhenStarted {
            viewModel.acceptOrderResponse.onEach {
                when(it){
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            viewModel.orderDetails(order_id,null,null,"Bearer"+preferenceModule.userData?.token,preferenceModule.language)

                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)
        }
    }

    private fun conversationIdObserver(){
        lifecycleScope.launchWhenStarted {
            viewModel.conversationIdResponse.onEach {
                when(it){
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            Log.e("response",Gson().toJson(it.data.data))
                            findNavController().navigate(OrderDetailsFragmentDirections.actionOrderDetailsFragmentToChatFragment(it.data.data?.sender_id!!,it.data.data.conversation_id!!,it.data.data.lastPage!!,it.data.data.receiver_id!!,name))
                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)
        }
    }
}