package com.aait.mahfulprovider.ui.fragment.home_page_cycle

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.auth.ConversationResponseItem
import com.aait.mahfulprovider.business.domain.model.auth.MyConversationsResponseItem
import com.aait.mahfulprovider.business.domain.model.auth.OrdersItem
import com.aait.mahfulprovider.databinding.FragmentChatsBinding
import com.aait.mahfulprovider.ui.adapter.ConversationsAdapter
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.ui.fragment.main_cycle.HomeContainerFragmentDirections
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.OnItemClickListener
import com.aait.mahfulprovider.util.common.getErrorMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class ChatsFragment  @Inject constructor() :BaseFragment<FragmentChatsBinding>() ,OnItemClickListener{
    private val viewModel by viewModels<ChatsViewModel>()
    private lateinit var conversationsAdapter: ConversationsAdapter
    private var chats = ArrayList<MyConversationsResponseItem>()

    override fun afterCreateView() {
        binding.titleLay.title.text = getString(R.string.conversations)
        conversationsAdapter = ConversationsAdapter(requireContext(),chats,R.layout.item_conversation)
        conversationsAdapter.setOnItemClickListener(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        homeObserver()
    }

    override fun getFragmentView(): Int = R.layout.fragment_chats

    private fun navigate(){
        binding.chats.rvRecycle.layoutManager = LinearLayoutManager(requireContext(),LinearLayoutManager.VERTICAL,false)
        binding.chats.rvRecycle.adapter = conversationsAdapter
        binding.chats.swipeRefresh.setOnRefreshListener {
            viewModel.myChats("Bearer"+preferenceModule.userData?.token,preferenceModule.language)
        }
        viewModel.myChats("Bearer"+preferenceModule.userData?.token,preferenceModule.language)
    }
    override fun onItemClick(view: View, position: Int) {
        findNavController().navigate(HomeContainerFragmentDirections.actionHomeContainerFragmentToChatFragment(chats.get(position).sender_id!!
        ,chats.get(position).conversation_id!!,chats.get(position).lastPage!!,chats.get(position).user_id!!,chats.get(position).username!!))

    }
    private fun homeObserver(){
        lifecycleScope.launchWhenStarted { viewModel.chatsResponse.emit(DataState.Idle)
            viewModel.chatsResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                        binding.chats.layNoInternet.visibility = View.GONE
                        binding.chats.layNoItem.visibility = View.GONE
                        binding.chats.swipeRefresh.isRefreshing = true
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        binding.chats.swipeRefresh.isRefreshing = false
                        if (it.data.value == "1") {

                            if (it.data.data!!.size==0){
                                binding.chats.layNoItem.visibility = View.VISIBLE
                                binding.chats.layNoInternet.visibility = View.GONE
                            }else{
                                conversationsAdapter.updateAll(it.data.data as List<MyConversationsResponseItem>)
                            }
                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        binding.chats.layNoInternet.visibility = View.VISIBLE
                        binding.chats.layNoItem.visibility = View.GONE
                        binding.chats.swipeRefresh.isRefreshing = false
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }
}