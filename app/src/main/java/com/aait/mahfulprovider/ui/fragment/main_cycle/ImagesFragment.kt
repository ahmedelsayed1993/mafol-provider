package com.aait.mahfulprovider.ui.fragment.main_cycle

import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.databinding.FragmentImageBinding
import com.aait.mahfulprovider.databinding.FragmentShowImageBinding
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImagesFragment : BaseFragment<FragmentImageBinding>(){
    private var path = ""
    private var desc = ""
    override fun afterCreateView() {
        path = requireArguments().getString("path")!!

        Glide.with(requireContext()).load(path).into(binding.image)


    }

    override fun getFragmentView(): Int = R.layout.fragment_image
}