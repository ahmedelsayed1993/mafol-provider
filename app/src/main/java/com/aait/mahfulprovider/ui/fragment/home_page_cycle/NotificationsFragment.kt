package com.aait.mahfulprovider.ui.fragment.home_page_cycle

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.aait.mahfulprovider.R
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.auth.NotificationResponseItem
import com.aait.mahfulprovider.databinding.FragmentNotificationsBinding
import com.aait.mahfulprovider.ui.adapter.NotificationAdapter
import com.aait.mahfulprovider.ui.base.BaseFragment
import com.aait.mahfulprovider.ui.fragment.main_cycle.HomeContainerFragmentDirections
import com.aait.mahfulprovider.util.FlowUtil.launchWhenStarted
import com.aait.mahfulprovider.util.OnItemClickListener
import com.aait.mahfulprovider.util.common.getErrorMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class NotificationsFragment  @Inject constructor() :BaseFragment<FragmentNotificationsBinding>() ,OnItemClickListener {
    private val viewModel by viewModels<NotificationViewModel>()
    private lateinit var notificationAdapter: NotificationAdapter
    private var notifications = ArrayList<NotificationResponseItem>()
    override fun afterCreateView() {
        binding.titleLay.title.text = getString(R.string.notifications)
        notificationAdapter =
            NotificationAdapter(requireContext(), notifications, R.layout.item_notification)
        notificationAdapter.setOnItemClickListener(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigate()
        homeObserver()
        deleteNotificationObserver()
    }

    override fun getFragmentView(): Int = R.layout.fragment_notifications

    private fun navigate() {
        binding.notification.rvRecycle.layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.VERTICAL, false
        )
        binding.notification.rvRecycle.adapter = notificationAdapter
        binding.notification.swipeRefresh.setOnRefreshListener {
            viewModel.myChats(
                "Bearer" + preferenceModule.userData?.token,
                preferenceModule.language
            )
        }
        viewModel.myChats("Bearer" + preferenceModule.userData?.token, preferenceModule.language)
        binding.deleteAll.setOnClickListener {
            viewModel.deleteAllNotification(
                "Bearer" + preferenceModule.userData?.token,
                preferenceModule.language
            )
        }

    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.lay) {
            if (notifications.get(position).order_id==0){

            }else {
                findNavController().navigate(
                    HomeContainerFragmentDirections.actionHomeContainerFragmentToOrderDetailsFragment(
                        notifications.get(position).order_id!!
                    )
                )
            }
        } else if (view.id == R.id.delete) {
            viewModel.deleteNotification(
                notifications.get(position).id!!,
                "Bearer" + preferenceModule.userData?.token,
                preferenceModule.language
            )
        }
    }

    private fun homeObserver() {
        lifecycleScope.launchWhenStarted {
            viewModel.notificationResponse.emit(DataState.Idle)
            viewModel.notificationResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                        binding.notification.layNoInternet.visibility = View.GONE
                        binding.notification.layNoItem.visibility = View.GONE
                        binding.notification.swipeRefresh.isRefreshing = true
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        binding.notification.swipeRefresh.isRefreshing = false
                        if (it.data.value == "1") {

                            if (it.data.data!!.size == 0) {
                                binding.notification.layNoItem.visibility = View.VISIBLE
                                binding.notification.layNoInternet.visibility = View.GONE
                            } else {
                                notificationAdapter.updateAll(it.data.data)
                            }
                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        binding.notification.layNoInternet.visibility = View.VISIBLE
                        binding.notification.layNoItem.visibility = View.GONE
                        binding.notification.swipeRefresh.isRefreshing = false
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)


        }
    }

    private fun deleteNotificationObserver() {
        lifecycleScope.launchWhenStarted {
            viewModel.deleteNotificationResponse.emit(DataState.Idle)
            viewModel.deleteNotificationResponse.onEach {

                when (it) {
                    is DataState.Loading -> {
                        progressUtil.showProgress()
                    }
                    is DataState.Success -> {
                        progressUtil.hideProgress()
                        if (it.data.value == "1") {
                            Toast.makeText(requireContext(), it.data.data, Toast.LENGTH_SHORT)
                                .show()
                            viewModel.myChats(
                                "Bearer" + preferenceModule.userData?.token,
                                preferenceModule.language
                            )
                        } else {
                            Toast.makeText(requireContext(), it.data.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    is DataState.Error -> {
                        progressUtil.hideProgress()
                        Toast.makeText(
                            requireContext(),
                            it.exception.getErrorMessage(),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    is DataState.Idle -> {
                    }
                }
            }.launchWhenStarted(viewLifecycleOwner)
        }
    }
}