package com.aait.mahfulprovider.ui.fragment.more_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.UserResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class ChangePasswordViewModel @Inject constructor(
    private val authRepo : AuthRepository
)  : ViewModel (){
    private val _changePassResponse =
        MutableStateFlow<DataState<BaseResponse<UserResponse>>>(DataState.Idle)

    val changePassResponse: MutableStateFlow<DataState<BaseResponse<UserResponse>>>
        get() = _changePassResponse


    fun changePass(
        current_pass:String,
        password:String,
        auth:String,
        lang:String
    ){
        viewModelScope.launch {
            authRepo.changePassword(current_pass,password,auth,lang).onEach {
                _changePassResponse.value = it
            }.launchIn(viewModelScope)
        }
    }
}