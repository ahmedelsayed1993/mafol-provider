package com.aait.mahfulprovider.ui.fragment.main_cycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aait.mahfulprovider.business.data.network.utils.DataState
import com.aait.mahfulprovider.business.domain.model.BaseResponse
import com.aait.mahfulprovider.business.domain.model.auth.ConversationResponseItem
import com.aait.mahfulprovider.business.domain.model.auth.SendFileResponse
import com.aait.mahfulprovider.business.domain.repo.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import javax.inject.Inject
@HiltViewModel
class ChatViewModel @Inject constructor(
    private val authRepo:AuthRepository
) :ViewModel(){

    private val _conversationResponse =
        MutableStateFlow<DataState<BaseResponse<List<ConversationResponseItem>>>>(DataState.Idle)

    val conversationResponse: MutableStateFlow<DataState<BaseResponse<List<ConversationResponseItem>>>>
        get() = _conversationResponse

    private val _sendMessageResponse =
        MutableStateFlow<DataState<BaseResponse<ConversationResponseItem>>>(DataState.Idle)

    val sendMessageResponse: MutableStateFlow<DataState<BaseResponse<ConversationResponseItem>>>
        get() = _sendMessageResponse


    private val _sendImageMessageResponse =
        MutableStateFlow<DataState<BaseResponse<ConversationResponseItem>>>(DataState.Idle)

    val sendImageMessageResponse: MutableStateFlow<DataState<BaseResponse<ConversationResponseItem>>>
        get() = _sendImageMessageResponse

    private val _sendAttachmentMessageResponse =
        MutableStateFlow<DataState<BaseResponse<SendFileResponse>>>(DataState.Idle)

    val sendAttachmentMessageResponse: MutableStateFlow<DataState<BaseResponse<SendFileResponse>>>
        get() = _sendAttachmentMessageResponse
    fun conversation(auth:String,lang:String,conversationId: String, page: String?) {

        viewModelScope.launch {
            authRepo.conversation(
                conversationId,
                auth,
                lang,
                page,"android"
            )
                .onEach {
                    _conversationResponse.value = it
                }.launchIn(viewModelScope)
        }
    }

    fun sendImageMessage(
        receiverId: String,
        conversationId: String,
        type: String,
        message: MultipartBody.Part,auth: String,lang: String

    ) {

        viewModelScope.launch {
            authRepo.sendImageMessage(
                receiverId,
                conversationId,
                type,
                message,
                auth,
                lang
            )
                .onEach {
                    _sendImageMessageResponse.value = it
                }.launchIn(viewModelScope)
        }
    }

    fun sendAttachmentMessage(
        message: MultipartBody.Part,auth: String,lang: String

    ) {

        viewModelScope.launch {
            authRepo.sendFile(

                message,
                auth,
                lang
            )
                .onEach {
                    _sendAttachmentMessageResponse.value = it
                }.launchIn(viewModelScope)
        }
    }
}